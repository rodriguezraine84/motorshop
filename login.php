<?php
include('connection.php');
session_start();

if(isset($_POST['login'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];

    // Using prepared statements to prevent SQL injection
    $user_query = mysqli_prepare($conn, "SELECT * FROM users WHERE email = ? AND BINARY password = ?");
    mysqli_stmt_bind_param($user_query, "ss", $email, $password);
    mysqli_stmt_execute($user_query);
    $user_result = mysqli_stmt_get_result($user_query);

    $user_branch_query = mysqli_prepare($conn, "SELECT * FROM users_branch WHERE email = ? AND BINARY password = ?");
    mysqli_stmt_bind_param($user_branch_query, "ss", $email, $password);
    mysqli_stmt_execute($user_branch_query);
    $user_branch_result = mysqli_stmt_get_result($user_branch_query);

    $user_dealer_query = mysqli_prepare($conn, "SELECT * FROM users_dealer WHERE email = ? AND BINARY password = ?");
    mysqli_stmt_bind_param($user_dealer_query, "ss", $email, $password);
    mysqli_stmt_execute($user_dealer_query);
    $user_dealer_result = mysqli_stmt_get_result($user_dealer_query);

    if(mysqli_num_rows($user_result) > 0) {
        $row = mysqli_fetch_assoc($user_result);
        $code = $row['code'];
        $usertype = $row['usertype'];

        $_SESSION['code'] = $code;
        $_SESSION['email'] = $email;
        $_SESSION['password'] = $password;

        if($usertype == 'super admin' || $usertype == 'Super Admin') {
            $url = '/admin/super-admin-dashboard.php';
            header('Location: ' . $url);
            exit();
        }
    } else if(mysqli_num_rows($user_branch_result) > 0) {
        $row = mysqli_fetch_assoc($user_branch_result);
        $code = $row['code'];
        $usertype = $row['usertype'];

        $_SESSION['code'] = $code;
        $_SESSION['email'] = $email;
        $_SESSION['password'] = $password;

        if($usertype == 'branch manager' || $usertype == 'Branch Manager') {
            $url = '/branch/branch-dashboard.php';
            header('Location: ' . $url);
            exit();
        }
    } else if(mysqli_num_rows($user_dealer_result) > 0) {
        $row = mysqli_fetch_assoc($user_dealer_result);
        $code = $row['code'];
        $usertype = $row['usertype'];

        $_SESSION['code'] = $code;
        $_SESSION['email'] = $email;
        $_SESSION['password'] = $password;

        if($usertype == 'dealer' || $usertype == 'Dealer') {
            $url = '/dealer/dealer-dashboard.php';
            header('Location: ' . $url);
            exit();
        }
    }
    else {
        echo "<script>
                alert('Invalid username or password');
                window.location.href='index.php';
              </script>";
        exit();
    }

}
?>
