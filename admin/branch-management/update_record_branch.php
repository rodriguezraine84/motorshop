<?php
include('../../connection.php');
session_start();

if(isset($_POST['updaterecord'])) {   
    $code = $_POST['edit_code'];
    $branch_name = $_POST['edit_branchname'];
    $branch_address = $_POST['edit_address'];
    $contact_person = $_POST['edit_person'];
    $contact_number = $_POST['edit_number'];
    $email = $_POST['edit_email'];

    // Prepare and execute statement to update branch record
    $query = mysqli_prepare($conn, "UPDATE branch_record SET 
        branch_name=?, branch_address=?, contact_person=?, contact_number=?, email=?
        WHERE code=?");
    mysqli_stmt_bind_param($query, "ssssss", $branch_name, $branch_address, $contact_person, $contact_number, $email, $code);
    $query_run = mysqli_stmt_execute($query);

    if($query_run) {
        $_SESSION['message'] = "Successfully Updated Branch";
        $_SESSION['message_type'] = "success";  
        header("Location: ../branch-management/records.php");
        exit();
    } else {
        $_SESSION['message'] = "Failed to Update Branch";
        $_SESSION['message_type'] = "danger";           
        header("Location: ../branch-management/records.php");
        exit();
    }
}
?>
