<?php 
include('../../connection.php');
session_start();

if(isset($_POST['recorddelete']))
{
    $code = $_POST['delete_code'];

    // Prepare and execute statement to delete branch record
    $query_delete = mysqli_prepare($conn, "DELETE FROM branch_record WHERE code=?");
    mysqli_stmt_bind_param($query_delete, "s", $code);
    $query_run_delete = mysqli_stmt_execute($query_delete);

    if($query_run_delete)
    {
        $_SESSION['message'] = "Successfully Deleted Branch";
        $_SESSION['message_type'] = "success";
        header("Location: ../branch-management/records.php");
        exit();
    }
    else
    {
        $_SESSION['message'] = "Failed to Delete Service";
        $_SESSION['message_type'] = "danger";   
        header("Location: ../branch-management/records.php");
        exit();
    }
}
?>
