<?php
include('../../connection.php');
session_start();

if (isset($_POST['updaterecord'])) {   
    $code = $_POST['edit_code'];
    $user_name = $_POST['edit_fullname'];
    $user_email = $_POST['edit_email'];
    $contact_number = $_POST['edit_number'];
    $user_password = $_POST['edit_password'];
    $edit_usertype = $_POST['edit_usertype'];
    $usertype_branch = "Branch Manager";
    $usertype_admin = "Super Admin";
    // Determine which table to update based on usertype
    if ($edit_usertype == "Super Admin") {
        $query = "UPDATE users SET 
            firstname=?, email=?, password=?, contact=?, usertype= 'Super Admin'
            WHERE userID=?";
    } else if($edit_usertype == "Branch Manager"){
        $query = "UPDATE users_branch SET 
            firstname=?, email=?, password=?, contact=?, usertype= 'Branch Manager'
            WHERE userID=?";
    }else{
        $query = "UPDATE users_dealer SET 
            firstname=?, email=?, password=?, contact=?, usertype= 'Dealer'
            WHERE userID=?";
    }

    $stmt = mysqli_prepare($conn, $query);

    if ($stmt) {
        mysqli_stmt_bind_param($stmt, "sssss", $user_name, $user_email, $user_password, $contact_number, $code);
        $query_run = mysqli_stmt_execute($stmt);

        if ($query_run) {
            $_SESSION['message'] = "Successfully Updated Account";
            $_SESSION['message_type'] = "success";
            header("Location: ../user-management/accounts.php");
            exit();
        } else {
            $_SESSION['message'] = "Failed to Update Account";
            $_SESSION['message_type'] = "danger";   
            header("Location: ../user-management/accounts.php");
            exit();
        }
    } else {
        $_SESSION['message'] = "Prepared statement error";
        $_SESSION['message_type'] = "danger";   
        header("Location: ../user-management/accounts.php");
        exit();
    }
}
?>
