<!DOCTYPE html>
<?php
include('../../connection.php');
session_start();

if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo "Record not Found!";  
    exit;
}

// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype FROM users WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
}


if (isset($_POST['addrecord'])) {
    $full_name = $_POST['fullname'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $contact = $_POST['contact'];
    $usertype = $_POST['usertype'];
    $branch = $_POST['branch'];

    // Check if the selected branch exists in branch_record
    $branch_check_query = "SELECT * FROM branch_record WHERE code = ?";
    $stmt_branch_check = mysqli_prepare($conn, $branch_check_query);
    mysqli_stmt_bind_param($stmt_branch_check, "s", $branch);
    mysqli_stmt_execute($stmt_branch_check);
    $result_branch_check = mysqli_stmt_get_result($stmt_branch_check);

    if (mysqli_num_rows($result_branch_check) > 0) {
        $insert_user = "INSERT INTO users_branch (`firstname`, `email`, `password`, `contact`, `usertype`, `branch_code`) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt_insert_user = mysqli_prepare($conn, $insert_user);
        mysqli_stmt_bind_param($stmt_insert_user, "ssssss", $full_name, $email, $password, $contact, $usertype, $branch);
        $insert_user_query = mysqli_stmt_execute($stmt_insert_user);

        if ($insert_user_query) {
            $_SESSION['message'] = "Successfully Added Branch";
            $_SESSION['message_type'] = "success";
            header("Location: ../user-management/accounts.php");
            exit();
        } else {
            $_SESSION['message'] = "Failed to Add Branch";
            $_SESSION['message_type'] = "danger";
            header("Location: ../user-management/accounts.php");
            exit();
        }
    } else {
        $_SESSION['message'] = "Selected branch does not exist";
        $_SESSION['message_type'] = "danger";
        header("Location: ../user-management/accounts.php");
        exit();
    }
}


if(isset($_POST['addrecordAdmin'])) {
    $full_name = $_POST['fullname'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $contact = $_POST['contact'];
    $usertype = $_POST['usertype'];

    $insert_user = "INSERT INTO users (`firstname`, `email`, `password`, `contact`, `usertype`) VALUES (?, ?, ?, ?, ?)";
    $stmt_insert_user = mysqli_prepare($conn, $insert_user);
    mysqli_stmt_bind_param($stmt_insert_user, "sssss", $full_name, $email, $password, $contact, $usertype);
    $insert_user_query = mysqli_stmt_execute($stmt_insert_user);

    if($insert_user_query) {
        $_SESSION['message'] = "Successfully Added Admin";
        $_SESSION['message_type'] = "success";
        header("Location: ../user-management/accounts.php");
        exit();
    } else {
        $_SESSION['message'] = "Failed to Add Admin";
        $_SESSION['message_type'] = "danger";    
        header("Location: ../user-management/accounts.php");
        exit();
    }
}

if (isset($_POST['addrecordDealer'])) {
    $full_name = $_POST['fullname'];
    $email = $_POST['email'];
    $password =$_POST['password'];
    $contact = $_POST['contact'];
    $usertype = $_POST['usertype'];
    $branch = $_POST['dealer'];

    // Check if the selected branch exists in branch_record
    $branch_check_query = "SELECT * FROM dealer_record WHERE code = ?";
    $stmt_branch_check = mysqli_prepare($conn, $branch_check_query);
    mysqli_stmt_bind_param($stmt_branch_check, "s", $branch);
    mysqli_stmt_execute($stmt_branch_check);
    $result_branch_check = mysqli_stmt_get_result($stmt_branch_check);

    if (mysqli_num_rows($result_branch_check) > 0) {
        $insert_user = "INSERT INTO users_dealer (`firstname`, `email`, `password`, `contact`, `usertype`, `dealer_code`) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt_insert_user = mysqli_prepare($conn, $insert_user);
        mysqli_stmt_bind_param($stmt_insert_user, "ssssss", $full_name, $email, $password, $contact, $usertype, $branch);
        $insert_user_query = mysqli_stmt_execute($stmt_insert_user);

        if ($insert_user_query) {
            $_SESSION['message'] = "Successfully Added Dealer";
            $_SESSION['message_type'] = "success";
            header("Location: ../user-management/accounts.php");
            exit();
        } else {
            $_SESSION['message'] = "Failed to Add Dealer";
            $_SESSION['message_type'] = "danger";
            header("Location: ../user-management/accounts.php");
            exit();
        }
    } else {
        $_SESSION['message'] = "Selected dealer does not exist";
        $_SESSION['message_type'] = "danger";
        header("Location: ../user-management/accounts.php");
        exit();
    }
}
// Select queries (not directly related to user creation)

$users = "SELECT * FROM users_branch";
$users_result = mysqli_query($conn, $users);

$admin = "SELECT * FROM users";
$admin_result = mysqli_query($conn, $admin);

$dealer = "SELECT * FROM users_dealer";
$dealer_result = mysqli_query($conn, $dealer);
?>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <script src="https://cdn.rawgit.com/serratus/quaggaJS/0.12.1/dist/quagga.min.js"></script>
    <title>Herb and Angel | User Management</title>
</head>
<style>
        .page-title {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .page-title h4 {
            margin: 0;
        }

        .btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }

        .horizontal-line {
            display: block;
            width: 100%;
            border-bottom: 1px solid grey; /* Adjust color here */
        }

    </style>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
                <a href="./super-admin-dashboard.php" class="logo">
                    <img src="../../assets/img/logo (1).png" alt="">
                </a>

                <a href="super-admin-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
                </a>

                <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>
            

           <!--Underline Nav-->
           <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset" style="width: 120%; height: 10vh;">
                                <div class="profilesets" style="width: 120%; height: 20vh;">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <span class="horizontal-line"></span>
                            <a class="dropdown-item settingsBtn">
                                <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                                Settings
                            </a>
                            <a class="dropdown-item logout pb-0" href="../../index.php">
                                <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                                Logout
                            </a>
                        </div>
                    </div>
                </li>
                </ul>

                <div class="dropdown mobile-user-menu">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <div class="profilesets">
                                <h5><?php echo $fullname?></h5>
                                <h6><?php echo $usertype?></h6>
                                </div>
                            </div>
                            <span class="horizontal-line"></span>
                            <a class="dropdown-item settingsBtn">
                                <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                                Settings
                            </a>
                            <a class="dropdown-item logout pb-0" href="../index.php">
                                <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                                Logout
                            </a>
                        </div>
                    </div>
                </div>
        </div>

            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../super-admin-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-store-alt.svg" alt="img"><span> Dealer Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../dealer-management/records.php">Dealer Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-package.svg" alt="img"><span> Supplier Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../supplier-management/records.php">Supplier Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-git-branch.svg" alt="img"><span> Branch Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../branch-management/records.php">Branch Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-cog.svg" alt="img"><span> Services Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../services-management/services.php">Services</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../..//assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/product-list.php">Add Product</a></li>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/returned.php">Returned</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                            </ul>
                                </li>
                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bxs-truck.svg" alt="img"><span> Logistics Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../logistics/records.php">Logistics Records</a></li>
                                    </ul>
                                </li>

                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wallet.svg" alt="img"><span> Expenses Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../expenses/records.php">Expenses Records</a></li>
                                    </ul>
                                </li>
                                
                                
                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> User Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../user-management/accounts.php">Accounts</a></li>
                                            </ul>
                                </li>   

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../report-generation/profit.php">Product Profit Report</a></li>
                                        <li><a href="../report-generation/mechanic.php">Mechanic Profit Report</a></li>
                                        <li><a href="../report-generation/logistics.php">Logistics Report</a></li>
                                        <li><a href="../report-generation/expenses.php">Expenses Report</a></li>
                                        <li><a href="../report-generation/productlist.php">Product List Report</a></li>
                                        <li><a href="../report-generation/report_stockinflow.php">Stock Inflow Report</a></li>
                                        <li><a href="../report-generation/report_stockoutflow.php">Stock Outflow Report</a></li>
                                    </ul>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

        <!--Under Main Content-->
        <div class="page-wrapper">
            <div class="content">
                <div class="col-lg-6 col-12" style="width: 100%;">
                    <?php
                        if (isset($_SESSION['message'])) {
                            $alertClass = ($_SESSION['message_type'] == 'success') ? 'alert-success' : 'alert-danger';
                            echo "<div class='alert $alertClass' role='alert'>" . $_SESSION['message'] . "</div>";
                            unset($_SESSION['message']); // Clear the session variable after displaying the message
                        }
                    ?>
                </div>
                <div class="page-title">
                    <h2 style="font-weight: bold;">ACCOUNTS</h2>
                    <div class="btn-container">
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addrecordAdmin" onclick="openPopupAdmin()">Add Account for Super Admin</button>
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addrecord" onclick="openPopup()">Add Account for Branch</button>
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addrecordDealer" onclick="openPopupDealer()">Add Account for Dealer</button>
                    </div>
                </div>
                
                <table class="table table-striped mt-3">
                    <thead>
                        <tr>
                            <th scope="col">User ID</th>
                            <th scope="col">Full Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Contact Number</th>
                            <th scope="col">Usertype</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        while ($product_row = mysqli_fetch_assoc($admin_result)) {
                            echo '<tr>';
                            echo '<td>' . $product_row['userID'] . '</td>';
                            echo '<td>' . $product_row['firstname'] . '</td>';
                            echo '<td>' . $product_row['email'] . '</td>';
                            echo '<td>' . $product_row['contact'] . '</td>';
                            echo '<td>' . $product_row['usertype'] . '</td>';
                            echo '<td>';
                            echo '<input type="hidden" class="form-control" value="' . $product_row["code"] . '" name="code" id="code">';
                            echo '<button type="button" class="btn btn-success editBtn" style="margin-right: 5px;" title="Edit"> <i class="fas fa-edit"></i> </button>';
                            echo '<button type="button" class="btn btn-danger deleteBtn"><i class="fas fa-trash-alt" title="Delete"></i> </button>';
                            echo '</td>';
                            echo '</tr>';
                        }
                    ?>
                    <?php
                        while ($product_row = mysqli_fetch_assoc($dealer_result)) {
                            echo '<tr>';
                            echo '<td>' . $product_row['userID'] . '</td>';
                            echo '<td>' . $product_row['firstname'] . '</td>';
                            echo '<td>' . $product_row['email'] . '</td>';
                            echo '<td>' . $product_row['contact'] . '</td>';
                            echo '<td>' . $product_row['usertype'] . '</td>';
                            echo '<td>';
                            echo '<input type="hidden" class="form-control" value="' . $product_row["code"] . '" name="code" id="code">';
                            echo '<button type="button" class="btn btn-success editBtn" style="margin-right: 5px;" title="Edit"> <i class="fas fa-edit"></i> </button>';
                            echo '<button type="button" class="btn btn-danger deleteBtn"><i class="fas fa-trash-alt" title="Delete"></i> </button>';
                            echo '</td>';
                            echo '</tr>';
                        }
                    
                    ?>
                    <?php
                        while ($product_row = mysqli_fetch_assoc($users_result)) {
                            echo '<tr>';
                            echo '<td>' . $product_row['userID'] . '</td>';
                            echo '<td>' . $product_row['firstname'] . '</td>';
                            echo '<td>' . $product_row['email'] . '</td>';
                            echo '<td>' . $product_row['contact'] . '</td>';
                            echo '<td>' . $product_row['usertype'] . '</td>';
                            echo '<td>';
                            echo '<input type="hidden" class="form-control" value="' . $product_row["code"] . '" name="code" id="code">';
                            echo '<button type="button" class="btn btn-success editBtn" style="margin-right: 5px;" title="Edit"> <i class="fas fa-edit"></i> </button>';
                            echo '<button type="button" class="btn btn-danger deleteBtn"><i class="fas fa-trash-alt" title="Delete"></i> </button>';
                            echo '</td>';
                            echo '</tr>';
                        }
                    
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="popup" id="addrecord-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addrecord" tabindex="-1" role="dialog" aria-labelledby="addrecordModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addrecordModalLabel">Add Accounts for Branch</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="fullname" id="fullname" value="" placeholder="Enter Full Name" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="password" id="password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="contact" id="contact" placeholder="Enter Contact" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="usertype">Usertype</label>
                                                    <select id="usertype" name="usertype" class="form-control" required>
                                                        <option value="">Select Usertype</option>
                                                        <option value="Branch Manager">Branch Manager</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Branch</label>
                                                    <select id="branch" name="branch" class="form-control" required>
                                                    <option value="" selected>Select a Branch</option>
                                                    <?php
                                                    $branch_name = "SELECT b.* FROM branch_record b LEFT JOIN users_branch ub ON b.code = ub.branch_code WHERE ub.branch_code IS NULL";
                                                    $branch_name_query = mysqli_query($conn, $branch_name);

                                                    if(mysqli_num_rows($branch_name_query) > 0) {
                                                        while($row = mysqli_fetch_assoc($branch_name_query)) {
                                                            $branchName = $row["branch_address"];
                                                            $branchCode = $row["code"];
                                                            $branch_name = $row["branch_name"];
                                                            echo "<option value='$branchCode'>$branch_name</option>";
                                                        }
                                                    } else {
                                                        echo "<option value=''>No branches found!</option>";
                                                    }
                                                    ?>

                                                    </select>
                                                </div> 
                                            </div>

                                            <div class="col-lg-12">
                                                <button type="submit" name="addrecord" class="btn btn-submit me-2">Add Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="addrecordAdmin-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addrecordAdmin" tabindex="-1" role="dialog" aria-labelledby="addrecordAdminModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addrecordAdminModalLabel">Add Accounts for Admin</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="fullname" id="fullname" value="" placeholder="Enter Full Name" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="password" id="password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="contact" id="contact" placeholder="Enter Contact" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="usertype">Usertype</label>
                                                    <select id="usertype" name="usertype" class="form-control" required>
                                                        <option value="">Select Usertype</option>
                                                        <option value="Super Admin">Super Admin</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12">
                                                <button type="submit" name="addrecordAdmin" class="btn btn-submit me-2">Add Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="popup" id="addrecordDealer-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addrecordDealer" tabindex="-1" role="dialog" aria-labelledby="addrecordDealerModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addrecordDealerModalLabel">Add Accounts for Dealer</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="fullname" id="fullname" value="" placeholder="Enter Full Name" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="password" id="password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="contact" id="contact" placeholder="Enter Contact" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="usertype">Usertype</label>
                                                    <select id="usertype" name="usertype" class="form-control" required>
                                                        <option value="">Select Usertype</option>
                                                        <option value="Dealer">Dealer</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Dealers</label>
                                                    <select id="dealer" name="dealer" class="form-control" required>
                                                        <option value="" selected>Select a Dealer</option>
                                                        <?php
                                                        // SQL query to fetch branches not already assigned in users_branch
                                                        $branch_name = "SELECT b.* FROM dealer_record b 
                                                                        LEFT JOIN users_dealer ub ON b.code = ub.dealer_code 
                                                                        WHERE ub.dealer_code IS NULL";
                                                        $branch_name_query = mysqli_query($conn, $branch_name);

                                                        // Check if there are any results
                                                        if (mysqli_num_rows($branch_name_query) > 0) {
                                                            // Loop through the results and populate the dropdown
                                                            while ($row = mysqli_fetch_assoc($branch_name_query)) {
                                                                $branchCode = $row["code"];
                                                                $branchName = $row["dealer_name"];
                                                                echo "<option value='$branchCode'>$branchName</option>";
                                                            }
                                                        } else {
                                                            // Display a message if no branches are found
                                                            echo "<option value=''>No dealer found!</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div> 
                                            </div>

                                            <div class="col-lg-12">
                                                <button type="submit" name="addrecordDealer" class="btn btn-submit me-2">Add Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--EDIT MODAL-->
    <div class="popup" id="editModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="addrecordModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addrecordModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="update_record_user.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="edit_code" id="edit_code">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="edit_fullname" id="edit_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="edit_email" id="edit_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="edit_number" id="edit_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="updaterecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <!--DELETE MODAL-->
    <div class="popup" id="deleteModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalLabel">Delete Record</h5>
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="delete_record_user.php" method="POST">
                            
                            <div class="modal-body">   
                                <input type="hidden" class="form-control" name="delete_code" id="delete_code">
                                <input type="hidden" name="userstype" id="userstype">
                                <h4>Are you sure you want to delete this record?</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="recorddelete" class="btn btn-primary">Delete Record</button>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="../settings.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
            $('#settings_usertype').val(usertype);
        }); 
    });
</script>

<script>
    function openPopup() {
        document.getElementById("addrecord-popup").style.display = "block";
    }
</script>

<script>
    function openPopupAdmin() {
        document.getElementById("addrecordAdmin-popup").style.display = "block";
    }
</script>
<script>
    function openPopupAdmin() {
        document.getElementById("addrecordDealer-popup").style.display = "block";
    }
</script>
<script>
    function updateUserType(selectedType) {
        document.getElementById('usertypeBtn').innerHTML = selectedType;
    }
</script>

<script>
        $(document).ready(function () {

            $('.editBtn').on('click', function () {

                $('#editModal').modal('show');

                $tr = $(this).closest('tr');

                var data = $tr.children("td").map(function () {
                    return $(this).text();
                }).get();

                console.log(data);

                $('#edit_code').val(data[0]);
                $('#edit_fullname').val(data[1]);
                $('#edit_email').val(data[2]);
                $('#edit_number').val(data[3]);
                $('#edit_usertype').val(data[4]);
            }); 
        });
</script>

<script>
    $(document).ready(function () {

        $('.deleteBtn').on('click', function () {

            $('#deleteModal').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function () {
                return $(this).text();
            }).get();

            console.log(data);

            $('#delete_code').val(data[0]);
            $('#userstype').val(data[4]);
        });
    });
</script>
</body>
</html>