<?php 
include('../../connection.php');
session_start();

if(isset($_POST['recorddelete'])) {
    $code = $_POST['delete_code'];
    $usertype = $_POST['userstype'];

    if($usertype == "Branch Manager"){
        $query_delete = "DELETE FROM users_branch WHERE userID=?";
        $stmt_delete = mysqli_prepare($conn, $query_delete);
    
        if ($stmt_delete) {
            mysqli_stmt_bind_param($stmt_delete, "s", $code);
            $query_run_delete = mysqli_stmt_execute($stmt_delete);
    
            if($query_run_delete) {
                $_SESSION['message'] = "Successfully Deleted Branch Manager's Account";
                $_SESSION['message_type'] = "success";
                header("Location: ../user-management/accounts.php");
                exit();
            } else {
                $_SESSION['message'] = "Failed to Delete Branch Manager's Account";
                $_SESSION['message_type'] = "danger";
                header("Location: ../user-management/accounts.php");
                exit();
            }
        } else {
            $_SESSION['message'] = "Prepared statement error";
            $_SESSION['message_type'] = "danger";
            header("Location: ../user-management/accounts.php");
            exit();
        }
    }else if($usertype == "Super Admin"){
        $query_delete = "DELETE FROM users WHERE userID=?";
        $stmt_delete = mysqli_prepare($conn, $query_delete);
    
        if ($stmt_delete) {
            mysqli_stmt_bind_param($stmt_delete, "s", $code);
            $query_run_delete = mysqli_stmt_execute($stmt_delete);
    
            if($query_run_delete) {
                $_SESSION['message'] = "Successfully Deleted Super Admin's Account";
                $_SESSION['message_type'] = "success";
                header("Location: ../user-management/accounts.php");
                exit();
            } else {
                $_SESSION['message'] = "Failed to Delete Super Admin's Account";
                $_SESSION['message_type'] = "danger";
                header("Location: ../user-management/accounts.php");
                exit();
            }
        } else {
            $_SESSION['message'] = "Prepared statement error";
            $_SESSION['message_type'] = "danger";
            header("Location: ../user-management/accounts.php");
            exit();
        }
    }else{
        $query_delete = "DELETE FROM users_dealer WHERE userID=?";
        $stmt_delete = mysqli_prepare($conn, $query_delete);
    
        if ($stmt_delete) {
            mysqli_stmt_bind_param($stmt_delete, "s", $code);
            $query_run_delete = mysqli_stmt_execute($stmt_delete);
    
            if($query_run_delete) {
                $_SESSION['message'] = "Successfully Deleted Dealer's Account";
                $_SESSION['message_type'] = "success";
                header("Location: ../user-management/accounts.php");
                exit();
            } else {
                $_SESSION['message'] = "Failed to Delete Dealer's Account";
                $_SESSION['message_type'] = "danger";
                header("Location: ../user-management/accounts.php");
                exit();
            }
        } else {
            $_SESSION['message'] = "Prepared statement error";
            $_SESSION['message_type'] = "danger";
            header("Location: ../user-management/accounts.php");
            exit();
        }
    }
    
    
}
?>
