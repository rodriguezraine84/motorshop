<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();

if(isset($_SESSION["code"])) {
    $code = $_SESSION["code"];
} else {
    echo "Record not Found!";
    exit;
}

// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype FROM users WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">-->
    <title>Herb and Angel | Inventory Analyzer</title>
</head>
<style>
    .blue-highlight {
        background-color: #a9c9ea; /* Adjust the color as needed */
    }

    .red-highlight {
        background-color: #ffaaaa; /* Adjust the color as needed */
    }

    .white-highlight {
        background-color: #FFFFFF; /* Adjust the color as needed */
    }
</style>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
                <a href="./super-admin-dashboard.php" class="logo">
                    <img src="../../assets/img/logo (1).png" alt="">
                </a>

                <a href="super-admin-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
                </a>

                <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                                <span class="status online"></span></span>
                                <div class="profilesets">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <a class="dropdown-item settingsBtn"><img src="../../assets/img/icons/settings.svg" class="me-2" alt="img">Settings</a>
                            <a class="dropdown-item logout pb-0" href="../../index.php"><img src="../../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="../../index.php">Logout</a>
                </div>
            </div>
        </div>

        <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../super-admin-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-store-alt.svg" alt="img"><span> Dealer Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../dealer-management/records.php">Dealer Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-package.svg" alt="img"><span> Supplier Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../supplier-management/records.php">Supplier Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-git-branch.svg" alt="img"><span> Branch Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../branch-management/records.php">Branch Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-cog.svg" alt="img"><span> Services Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../services-management/services.php">Services</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../..//assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/product-list.php">Add Product</a></li>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                        <li><a href="../product-management/inflowproductlist.php">Inflow Product List</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                            </ul>
                                </li>
                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bxs-truck.svg" alt="img"><span> Logistics Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../logistics/records.php">Logistics Records</a></li>
                                    </ul>
                                </li>

                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wallet.svg" alt="img"><span> Expenses Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../expenses/records.php">Expenses Records</a></li>
                                    </ul>
                                </li>
                                
                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> User Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../user-management/accounts.php">Accounts</a></li>
                                    </ul>
                                </li>   

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../report-generation/profit.php">Product Profit Report</a></li>
                                        <li><a href="../report-generation/mechanic.php">Mechanic Profit Report</a></li>
                                        <li><a href="../report-generation/logistics.php">Logistics Report</a></li>
                                        <li><a href="../report-generation/expenses.php">Expenses Report</a></li>
                                        <li><a href="../report-generation/productlist.php">Product List Report</a></li>
                                        <li><a href="../report-generation/report_stockinflow.php">Stock Inflow Report</a></li>
                                        <li><a href="../report-generation/report_stockoutflow.php">Stock Outflow Report</a></li>
                                    </ul>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="page-title">
                        <h2 style="font-weight: bold;">INVENTORY ANALYZER</h2>
                    </div>

                <div class="modal-body d-flex justify-content-center">
                        <div class="form-group" style="width: 50%;">
                            <label>Generate Inventory From:</label>
                            <select id="inventorySource" name="inventorySource" placeholder="Nothing Selected" class="selectpicker custom-width form-control" required>
                                <option value="" selected>Select an Inventory</option>
                                <option value="branch">Branch</option>
                                <option value="dealer">Dealer</option>
                            </select>
                        </div>

                        <!-- Dealer selection, initially hidden -->
                    <div  class="modal-body d-flex justify-content-center" style="display: none;">
                        <div  id="dealerSelect" class="form-group" style="width: 50%;">
                            <label>Select Dealer:</label>
                            <select id="dealers" name="dealers" class="form-control" required>
                                <option value="" selected>Select a Dealer</option>
                                <?php 
                                    $users_branch_query = mysqli_query($conn, "SELECT * FROM users_dealer");

                                    if(mysqli_num_rows($users_branch_query) > 0) {
                                        while($user_row = mysqli_fetch_assoc($users_branch_query)) {
                                            // Fetch the branch information for each user
                                            $user_branch_code = $user_row["dealer_code"];
                                            $branch_info_query = mysqli_query($conn, "SELECT * FROM dealer_record WHERE code = '$user_branch_code'");
                                            
                                            if(mysqli_num_rows($branch_info_query) > 0) {
                                                $branch_info = mysqli_fetch_assoc($branch_info_query);
                                                $branchName = $branch_info["dealer_address"];
                                                $branchCode = $branch_info["code"];

                                                echo "<option value='$user_branch_code'>$branchName</option>";
                                            } else {
                                                echo "<option value=''>No branch found for user!</option>";
                                            }
                                        }
                                    } else {
                                        echo "<option value=''>No users found!</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>


                    <!-- Branch selection, initially hidden -->
                    <div  class="modal-body d-flex justify-content-center" style="display: none;">
                        <div  id="branchSelection" class="form-group" style="width: 60%;">
                            <label>Select Branch:</label>
                            <select id="branched" name="branched" class="form-control" required>
                                    <option value="" selected>Select a Branch</option>
                                    <?php
                                    $users_branch_query = mysqli_query($conn, "SELECT * FROM users_branch");

                                    if(mysqli_num_rows($users_branch_query) > 0) {
                                        while($user_row = mysqli_fetch_assoc($users_branch_query)) {
                                            // Fetch the branch information for each user
                                            $user_branch_code = $user_row["branch_code"];
                                            $branch_info_query = mysqli_query($conn, "SELECT * FROM branch_record WHERE code = '$user_branch_code'");
                                            
                                            if(mysqli_num_rows($branch_info_query) > 0) {
                                                $branch_info = mysqli_fetch_assoc($branch_info_query);
                                                $branchName = $branch_info["branch_address"];
                                                $branchCode = $branch_info["code"];

                                                echo "<option value='$user_branch_code'>$branchName</option>";
                                            } else {
                                                echo "<option value=''>No branch found for user!</option>";
                                            }
                                        }
                                    } else {
                                        echo "<option value=''>No users found!</option>";
                                    }
                                    ?>
                                </select>
                        </div>
                    </div>
                </div>
                    <!-- Branch table, initially hidden -->
                    <div id="branchTable" style="display: none;">
                        <table class="table" id="branchTables">
                            <thead>
                                <tr>
                                    <th scope="col">Product Name</th>
                                    <th scope="col">Stock Inflow</th>
                                    <th scope="col">Stock Outflow</th>
                                    <th scope="col">Available Stocks</th>
                                    <th scope="col">Supplier Price</th>
                                    <th scope="col">SRP</th>
                                    <th scope="col">Total Value (Supplier Price)</th>
                                    <th scope="col">Total Value (SRP)</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>

                    <div id="dealerTable" style="display: none;">
                        <table class="table" id="dealerTables">
                            <thead>
                                <tr>
                                    <th scope="col">Product Name</th>
                                    <th scope="col">Stock Inflow</th>
                                    <th scope="col">Stock Outflow</th>
                                    <th scope="col">Available Stocks</th>
                                    <th scope="col">Dealer Price</th>
                                    <th scope="col">Supplier Price</th>
                                    <th scope="col">SRP</th>
                                    <th scope="col">Total Value (Dealer Price)</th>
                                    <th scope="col">Total Value (Supplier Price)</th>
                                    <th scope="col">Total Value (SRP)</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>

    <div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="../settings.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
            $('#settings_usertype').val(usertype);
        }); 
    });
</script>

<script>
    document.addEventListener('DOMContentLoaded', function() {
    const inventorySource = document.getElementById('inventorySource');
    const dealerSelection = document.getElementById('dealerSelect');
    const tables = document.querySelector('#dealerTables tbody');
    const branchSelection = document.getElementById('branchSelection');
    const table = document.querySelector('#branchTables tbody');
    const branch = document.getElementById('branched');
    const dealer = document.getElementById('dealers');

    function updateSelectedBranchesContainer() {
        if (inventorySource.value === 'dealer') {
            dealerSelection.style.display = 'block';
            dealerTable.style.display = 'block';
            branchSelection.style.display = 'none';
            branchTable.style.display = 'none';
        } else if (inventorySource.value === 'branch') {
            branchSelection.style.display = 'block';
            branchTable.style.display = 'block';
            dealerSelection.style.display = 'none';
            dealerTable.style.display = 'none';
        } else {
            dealerSelection.style.display = 'none';
            dealerTable.style.display = 'none';
            branchSelection.style.display = 'none';
            branchTable.style.display = 'none';
        }
    }

    branch.addEventListener('change', function() {
        console.log('Branch: ', branch.value);
        fetchData();
    });
    
    dealer.addEventListener('change', function(){
        console.log('Dealer: ', dealer.value);
        fetchDataDealers();
    });
    
    inventorySource.addEventListener('change', function() {
        console.log(`Selected option: ${inventorySource.value}`);
        updateSelectedBranchesContainer();

        if(inventorySource.value === 'branch'){
            fetchData();
        }else if(inventorySource.value ==='dealer'){
            fetchDataDealers();
        }
    });

    async function fetchData() {
        const branchValue = branch.value;

        const formData = new FormData();
        formData.append('branched', branchValue);

        try {
            const response = await fetch('inventory_branch.php', {
                method: 'POST',
                body: formData
            });

            const text = await response.text();
            try {
                const data = JSON.parse(text);
                table.innerHTML = ''; // Clear the table before adding new data
                data.forEach(item => {
                    console.log(`Barcode: ${item.productname}`);

                    const row = document.createElement('tr');
                    row.innerHTML = `
                        <td>${item.productname}</td>
                        <td>${item.received}</td>
                        <td>${item.sold}</td>
                        <td>${item.availablestocks}</td>
                        <td>${item.supplierprice}</td>
                        <td>${item.srp}</td>
                        <td>${item.totalvalue}</td>
                        <td>${item.totalvalue_srp}</td>
                    `;

                    table.appendChild(row);
                });
            } catch (jsonError) {
                console.error('Error parsing JSON:', jsonError);
                console.error('Response text:', text);
            }
        } catch (error) {
            console.error('Error fetching profit data:', error);
        }
    };

    async function fetchDataDealers() {
        const dealerValue = dealer.value;

        const formData = new FormData();
        formData.append('dealers', dealerValue);
        try {
            const response = await fetch('inventory_dealer.php', {
                method: 'POST',
                body: formData
            });

            const text = await response.text();
            try {
                const data = JSON.parse(text);
                tables.innerHTML = ''; // Clear the table before adding new data
                data.forEach(item => {
                    console.log(`Barcode: ${item.productname}`);

                    const row = document.createElement('tr');
                    row.innerHTML = `
                        <td>${item.productname}</td>
                        <td>${item.received}</td>
                        <td>${item.sold}</td>
                        <td>${item.availablestocks}</td>
                        <td>${item.dealerprice}</td>
                        <td>${item.supplierprice}</td>
                        <td>${item.srp}</td>
                        <td>${item.totalvalue_dealer}</td>
                        <td>${item.totalvalue}</td>
                        <td>${item.totalvalue_srp}</td>
                    `;

                    tables.appendChild(row);
                });
            } catch (jsonError) {
                console.error('Error parsing JSON:', jsonError);
                console.error('Response text:', text);
            }
        } catch (error) {
            console.error('Error fetching profit data:', error);
        }
    };

    updateSelectedBranchesContainer();
});

</script>

</body>
</html>