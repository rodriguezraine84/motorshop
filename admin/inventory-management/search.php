<?php
session_start();
include('../../connection.php');

if(isset($_POST['searchproductname'])){
    // Escape user input to prevent SQL Injection
    $query = mysqli_real_escape_string($conn, $_POST['searchproductname']);
    $output = "";
    // Correct SQL query to fetch product names
    $sql = "SELECT productname FROM products WHERE productname LIKE '%$query%'";
    $result = mysqli_query($conn, $sql);
    
    $output = '<ul class="list-unstyled">';
    
    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)){
            $output .= '<li class="list">'.$row["productname"].'</li>';
        }
    } else {
        $output .= '<li>Product Name not Found! </li>';
    }
    
    $output .= '</ul>';
    
    echo $output;

    // Close the result set
    mysqli_free_result($result);
}

// Close the connection
mysqli_close($conn);
?>
