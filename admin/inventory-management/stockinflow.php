<!DOCTYPE html>
<?php
include('../../connection.php');
session_start();

if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo "Record not Found!";
    exit;
}

// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype FROM users WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
}

$productname = "";
$dealerprice = "";
$srp = "";
$supplierprice = "";

if (isset($_POST['barcode'])) {
    // Escape special characters in the barcode input to prevent SQL injection
    $barcode = mysqli_real_escape_string($conn, $_POST['barcode']);

    // Prepare an SQL statement to select product details based on the barcode
    $product = "SELECT productname, dealer_price, supplier_price, srp FROM products WHERE barcode= ?";
    $product_query = mysqli_prepare($conn, $product);

    // Bind the barcode parameter to the prepared statement
    mysqli_stmt_bind_param($product_query, "s", $barcode);

    // Execute the prepared statement
    mysqli_stmt_execute($product_query);

    // Store the result of the query
    mysqli_stmt_store_result($product_query);

    // Check if the query returned any rows
    if(mysqli_stmt_num_rows($product_query) > 0){
        // Bind the result variables to the prepared statement
        mysqli_stmt_bind_result($product_query, $productname, $dealerprice, $supplierprice, $srp);

        // Fetch the result into the bound variables
        mysqli_stmt_fetch($product_query);
    }
}


$barcode = ""; // Initialize $barcode variable to store the fetched barcode

if(isset($_POST['searchproductname'])) {
    $productname = mysqli_real_escape_string($conn, $_POST['searchproductname']);

    $product = "SELECT barcode FROM products WHERE productname= ?";
    $product_query = mysqli_prepare($conn, $product);
    mysqli_stmt_bind_param($product_query, "s", $productname);
    mysqli_stmt_execute($product_query);
    mysqli_stmt_store_result($product_query);

    if(mysqli_stmt_num_rows($product_query) > 0){
        mysqli_stmt_bind_result($product_query, $barcode);
        mysqli_stmt_fetch($product_query);
    }

}

$defaultDate = date('Y-m-d');
$fromDate = isset($_POST['startfrom']) ? $_POST['startfrom'] : null;
$toDate = isset($_POST['endto']) ? $_POST['endto'] : null;

date_default_timezone_set('Asia/Manila');
// $product_query = "SELECT 
//                             ib.date,
//                             ib.barcode,
//                             p.productname,
//                             ib.supplier_price,
//                             ib.units_received,
//                             ib.totalvalue,
//                             ib.totalvalue_dealer,
//                             ib.totalvalue_srp,
//                             ib.dealer_price,
//                             ib.srp,
//                             br.branch_address AS location
//                         FROM 
//                             inflow_branch ib
//                         JOIN 
//                             branch_record br ON ib.code = br.code
//                         JOIN 
//                             products p ON ib.barcode = p.barcode
//                         WHERE
//                             DATE(ib.date) = CURDATE()
//                         ORDER BY ib.date DESC";
// $product_result = mysqli_query($conn, $product_query);

// $product_query1 = "SELECT 
//                             ib.date,
//                             ib.barcode,
//                             p.productname,
//                             ib.supplier_price,
//                             ib.units_received,
//                             ib.totalvalue,
//                             ib.totalvalue_dealer,
//                             ib.totalvalue_srp,
//                             ib.dealer_price,
//                             ib.srp,
//                             ib.location
//                         FROM 
//                             inflow_admin ib
//                         JOIN 
//                             products p ON ib.barcode = p.barcode
//                         WHERE
//                             DATE(ib.date) = CURDATE()
//                         ORDER BY ib.date DESC";
// $product_result1 = mysqli_query($conn, $product_query1);

?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <script src="https://cdn.rawgit.com/serratus/quaggaJS/0.12.1/dist/quagga.min.js"></script>
    <title>Herb and Angel | Stock Inflow</title>
</head>
<style>
        .page-title {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .page-title h4 {
            margin: 0;
        }

        .btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }
        .wrap-text {
            
            word-wrap: break-word;
            max-width: 200px; /* Adjust the max-width as needed */
        }
        

        .table-responsive{
            overflow-x: auto;
        }

        .list-unstyled{
            background-color: #eee;
            cursor: pointer;
            border-radius: 5px;
        }

        .list{
            padding:12px;
        }
        
        .list:hover{
            background-color: black;
            color: white;
        }
        .horizontal-line {
            display: block;
            width: 100%;
            border-bottom: 1px solid grey; /* Adjust color here */
        }
        
    </style>
<body>
    <div id="global-loader">
        <div class="whirly-loader">
        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
                <a href="./super-admin-dashboard" class="logo">
                    <img src="../../assets/img/logo (1).png" alt="">
                </a>

                <a href="super-admin-dashboard" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
                </a>

                <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

            <!--Nav Items Dropdown-->
            <li class="nav-item dropdown has-arrow main-drop">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset" style="width: 120%; height: 10vh;">
                            <div class="profilesets" style="width: 120%; height: 20vh;">
                            <h6><?php echo $fullname?></h6>
                            <h5><?php echo $usertype?></h5>
                            </div>
                        </div>
                        <span class="horizontal-line"></span>
                        <a class="dropdown-item settingsBtn">
                            <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                            Settings
                        </a>
                        <a class="dropdown-item logout pb-0" href="../../index">
                            <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                            Logout
                        </a>
                    </div>
                </div>
            </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset">
                            <div class="profilesets">
                            <h5><?php echo $fullname?></h5>
                            <h6><?php echo $usertype?></h6>
                            </div>
                        </div>
                        <span class="horizontal-line"></span>
                        <a class="dropdown-item settingsBtn">
                            <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                            Settings
                        </a>
                        <a class="dropdown-item logout pb-0" href="../index">
                            <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                            Logout
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../super-admin-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-store-alt.svg" alt="img"><span> Dealer Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../dealer-management/records.php">Dealer Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-package.svg" alt="img"><span> Supplier Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../supplier-management/records.php">Supplier Record</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-git-branch.svg" alt="img"><span> Branch Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../branch-management/records.php">Branch Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-cog.svg" alt="img"><span> Services Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../services-management/services">Services</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/product-list.php">Add Product</a></li>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                        <li><a href="../product-management/inflowproductlist.php">Inflow Product List</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                            </ul>
                                </li>                
                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bxs-truck.svg" alt="img"><span> Logistics Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../logistics/records.php">Logistics Records</a></li>
                                    </ul>
                                </li>

                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wallet.svg" alt="img"><span> Expenses Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../expenses/records.php">Expenses Records</a></li>
                                    </ul>
                                </li>
                                
                                
                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> User Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../user-management/accounts.php">Accounts</a></li>
                                            </ul>
                                </li>   

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../report-generation/profit.php">Product Profit Report</a></li>
                                        <li><a href="../report-generation/mechanic.php">Mechanic Profit Report</a></li>
                                        <li><a href="../report-generation/logistics.php">Logistics Report</a></li>
                                        <li><a href="../report-generation/expenses.php">Expenses Report</a></li>
                                        <li><a href="../report-generation/productlist.php">Product List Report</a></li>
                                        <li><a href="../report-generation/report_stockinflow.php">Stock Inflow Report</a></li>
                                        <li><a href="../report-generation/report_stockoutflow.php">Stock Outflow Report</a></li>
                                    </ul>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

        <!--Under Main Content-->
        <div class="page-wrapper">
            <div class="content">
                <?php
                    if (isset($_SESSION['message'])) {
                        $alertClass = ($_SESSION['message_type'] == 'success') ? 'alert-success' : 'alert-danger';
                        echo "<div class='alert $alertClass' role='alert'>" . $_SESSION['message'] . "</div>";
                        unset($_SESSION['message']); // Clear the session variable after displaying the message
                    }
                ?>
                <div class="page-title">
                    <h2 style="font-weight: bold;">STOCK INFLOW</h2>
                    <div class="btn-container">
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addstocks" onclick="openPopup()">Add Stocks Dealer</button>
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#selectBranch" onclick="openSelectBranch()">Add Stocks Branch</button>
                        <button type="button" class="btn btn-dark editBtn" title="Print" onclick="generatePDF()"> <i class="fas fa-print" style="margin-right: 5px;"></i>Print Report</button>
                    </div>
                </div>
                <div>
                    <form action="" method="POST">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label for="startfrom">Start From:</label>
                                    <input type="date" class="form-control" id="startfrom" name="startfrom">
                                </div>
                                
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label for="endto">End To:</label>
                                    <input type="date" class="form-control" id="endto" name="endto">
                                </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                                <label>Branch</label>
                                <select id="branched" name="branched" class="form-control" required>
                                    <option value="" selected>Select a Branch</option>
                                    <?php
                                    $users_branch_query = mysqli_query($conn, "SELECT * FROM users_branch");

                                    if(mysqli_num_rows($users_branch_query) > 0) {
                                        while($user_row = mysqli_fetch_assoc($users_branch_query)) {
                                            // Fetch the branch information for each user
                                            $user_branch_code = $user_row["branch_code"];
                                            $branch_info_query = mysqli_query($conn, "SELECT * FROM branch_record WHERE code = '$user_branch_code'");
                                            
                                            if(mysqli_num_rows($branch_info_query) > 0) {
                                                $branch_info = mysqli_fetch_assoc($branch_info_query);
                                                $branchName = $branch_info["branch_address"];
                                                $branchCode = $branch_info["code"];

                                                echo "<option value='$user_branch_code'>$branchName</option>";
                                            } else {
                                                echo "<option value=''>No branch found for user!</option>";
                                            }
                                        }
                                    } else {
                                        echo "<option value=''>No users found!</option>";
                                    }
                                    ?>
                                </select>
                                <!-- <button type="button" class="btn btn-submit mt-5" data-bs-toggle="modal" data-bs-target="#searchproduct" onclick="openPopupSearchProduct()">Proceed</button> -->
                            </div>
                        </div>
                    </form>
                </div>
                <div class="container">
                    <div class="row"  id="reportTable">
                        <table class="table table-striped" id="productTable">
                            <thead>
                                <tr>
                                    <th scope="col" style="text-align: center;">Date</th>
                                    <th scope="col" style="text-align: center;">Barcode <br> Number</th>
                                    <th scope="col" class="wrap-text" style="text-align: center;">Product <br> Name</th>
                                    <th scope="col" style="text-align: center;">Dealer <br> Price</th>
                                    <th scope="col" style="text-align: center;">Supplier <br> Price</th>
                                    <th scope="col" style="text-align: center;">SRP</th>
                                    <th scope="col" style="text-align: center;">Units <br> Received</th>
                                    <th scope="col" class="wrap-text" style="text-align: center;">Dealer Price <br>Total Value</th>
                                    <th scope="col" class="wrap-text" style="text-align: center;">Supplier Price <br>Total Value</th>
                                    <th scope="col" class="wrap-text" style="text-align: center;">SRP <br> Total Value</th>
                                    <th scope="col">Location</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="select-branch"> 
        <div class="popupcontent">
            <div class="modal fade" id="selectBranch" tabindex="-1" role="dialog" aria-labelledby="addstocksModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header d-flex justify-content-between align-items-center">
                            <h5 class="modal-title" id="addstocksModalLabel">Select Branch</h5>
                            <div>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                            </div>
                        </div>
                        <div class="modal-body d-flex justify-content-center">
                            <div class="form-group">
                                <label>Branch</label>
                                <select id="branch" name="branch" class="form-control" required>
                                    <option value="" selected>Select a Branch</option>
                                    <?php
                                    $users_branch_query = mysqli_query($conn, "SELECT * FROM users_branch");

                                    if(mysqli_num_rows($users_branch_query) > 0) {
                                        while($user_row = mysqli_fetch_assoc($users_branch_query)) {
                                            // Fetch the branch information for each user
                                            $user_branch_code = $user_row["branch_code"];
                                            $branch_info_query = mysqli_query($conn, "SELECT * FROM branch_record WHERE code = '$user_branch_code'");
                                            
                                            if(mysqli_num_rows($branch_info_query) > 0) {
                                                $branch_info = mysqli_fetch_assoc($branch_info_query);
                                                $branchName = $branch_info["branch_address"];
                                                $branchCode = $branch_info["code"];

                                                echo "<option value='$user_branch_code'>$branchName</option>";
                                            } else {
                                                echo "<option value=''>No branch found for user!</option>";
                                            }
                                        }
                                    } else {
                                        echo "<option value=''>No users found!</option>";
                                    }
                                    ?>
                                </select>
                                <button type="button" class="btn btn-submit mt-5" data-bs-toggle="modal" data-bs-target="#searchproduct" onclick="openPopupSearchProduct()">Proceed</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="search-product"> 
        <div class="popupcontent">
            <div class="modal fade" id="searchproduct" tabindex="-1" role="dialog" aria-labelledby="addstocksModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header d-flex justify-content-between align-items-center">
                            <h5 class="modal-title" id="addstocksModalLabel">Search Product</h5>
                            <div>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                            </div>
                        </div>
                        <input type="hidden" id="branchid" name="branchid"></input>
                        <div class="modal-body d-flex justify-content-center">
                            <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#searchname" onclick="openSearchProductName()">Search Product by Name</button>
                            <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#searchbarcodee" onclick="openSearchBarcode()">Search Product by Barcode</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="searchname-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="searchname" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header d-flex justify-content-between align-items-center">
                            <h5 class="modal-title">Add Product using Product Name</h5>
                            <div style="display: flex; align-items: center;">
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                            </div>
                        </div>

                        <div class="modal-body">
                            <form action="../functions/inflow_productname.php" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Barcode</label>
                                                    <input type="text" class="form-control" name="searchbarcode" id="searchbarcode" value="<?php echo $barcode ?>" placeholder="Enter Barcode" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" class="form-control" name="searchproductname" id="searchproductname" value="<?php echo @$_POST['searchproductname'] ?>" onblur="fetchBarcodeProduct()" placeholder="Enter Product Name" autofocus>
                                                </div>
                                                <div id="productsnames"></div>
                                            </div>
                                            
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Dealer Price</label>
                                                    <input type="text" class="form-control" name="searchdealerprice" id="searchdealerprice" placeholder="Enter Supplier Price" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Supplier Price</label>
                                                    <input type="text" class="form-control" name="searchsupplierprice" id="searchsupplierprice" placeholder="Enter Supplier Price" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>SRP</label>
                                                    <input type="text" class="form-control" name="searchsrp" id="searchsrp" placeholder="Enter Supplier Price" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Units Received</label>
                                                    <input type="text" class="form-control" name="searchunits" id="searchunits" placeholder="Enter Units Received" required>
                                                </div>
                                            </div>
                                            <!--eto inadd ko-->
                                            <input type="hidden" id="branch1" name="branch1"></input>
                                            <input type="hidden" id="code1" name="code1" value="<?php echo $code?>"></input>
                                            <div class="col-lg-12">
                                                <button type="submit" name="addstocksname" class="btn btn-submit me-2">Add Stocks</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="searchbarcode-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="searchbarcodee" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header d-flex justify-content-between align-items-center">
                            <h5 class="modal-title">Add Product using Barcode</h5>
                            <div style="display: flex; align-items: center;">
                                <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#importExcel" onclick="openExcel()">Import Excel</button>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                            </div>
                        </div>

                        <div class="modal-body">
                            <form action="../functions/inflow_barcode.php" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Barcode</label>
                                                    <input type="text" class="form-control" name="barcode" id="barcode" value="<?php echo @$_POST['barcode']?>" onblur="fetchProductName()" placeholder="Enter Barcode" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" class="form-control" name="codeproductname" id="codeproductname" value="<?php echo $productname ?>" placeholder="Enter Product Name" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Dealer Price</label>
                                                    <input type="hidden" class="form-control" name="codedealerprice" id="codedealerprice" placeholder="Enter Dealer Price">
                                                    <input type="text" class="form-control" name="codedealerprice" id="codedealerprice1" value="<?php echo $dealerprice ?>" placeholder="Enter Dealer Price" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Supplier Price</label>
                                                    <input type="hidden" class="form-control" name="codesupplierprice" id="codesupplierprice" placeholder="Enter Supplier Price">
                                                    <input type="text" class="form-control" name="codesupplierprice" id="codesupplierprice1" value="<?php echo $supplierprice ?>" placeholder="Enter Supplier Price" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>SRP</label>
                                                    <input type="hidden" class="form-control" name="codesrp" id="codesrp" placeholder="Enter SRP">
                                                    <input type="text" class="form-control" name="codesrp" id="codesrp1" placeholder="Enter SRP" value="<?php echo $srp ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Units Received</label>
                                                    <input type="text" class="form-control" name="codeunits" id="codeunits" placeholder="Enter Units Received" required>
                                                </div>
                                            </div>
                                            <!--eto inadd ko-->
                                            <input type="hidden" id="branchID" name="branchID"></input>
                                            <input type="hidden" id="code_admin" name="code_admin" value="<?php echo $code;?>"></input>
                                            <div class="col-lg-12">
                                                <button type="submit" name="codeaddstocks" class="btn btn-submit me-2">Add Stocks</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="addstocks-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addstocks" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header d-flex justify-content-between align-items-center">
                            <h5 class="modal-title">Add Stocks to Dealer</h5>
                            <div style="display: flex; align-items: center;">
                                <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#importExcelAdmin" onclick="openExcelAdmin()">Import Excel</button>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                            </div>
                        </div>

                        <div class="modal-body">
                            <form action="../functions/inflow_dealer.php" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Barcode</label>
                                                    <input type="text" class="form-control" name="barcode1" id="barcode1" value="<?php echo @$_POST['barcode1']?>" onblur="fetchProductNameDealer()" placeholder="Enter Barcode" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" class="form-control" name="productname" id="productname" value="<?php echo $productname ?>" placeholder="Enter Product Name" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Dealer Price</label>
                                                    <input type="text" class="form-control" name="dealerprice" id="dealerprice" placeholder="Enter Supplier Price" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Supplier Price</label>
                                                    <input type="text" class="form-control" name="supplierprice" id="supplierprice" placeholder="Enter Supplier Price" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>SRP</label>
                                                    <input type="text" class="form-control" name="srp" id="srp" placeholder="Enter Supplier Price" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Units Received</label>
                                                    <input type="text" class="form-control" name="units" id="units" placeholder="Enter Units Received" required>
                                                </div>
                                            </div>
                                            <!--eto inadd ko-->
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Admin Location</label>
                                                    <input type="text" class="form-control" name="location_admin" id="location_admin" value="<?php echo $code?>" placeholder="Admin" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="addstocks" class="btn btn-submit me-2">Add Stocks</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="importexcel-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="importExcelLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="importExcelModalLabel">Import Excel</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="../functions/excel_branch.php" method="POST" enctype="multipart/form-data">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Upload Excel File</label>
                                                        <input type="hidden" id="excel" name="excel">
                                                        <div class="image-upload">
                                                            <input type="file" name="import_file" class="custom-file-input" id="inputGroupFile01">
                                                            <div class="image-uploads">
                                                                <img src="../../assets/img/icons/upload.svg" alt="img">
                                                                    <h4>Drag and drop a file to upload</h4>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <button type="submit" name="save_excel_data" class="btn btn-submit me-2">Add Stocks for Branch</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="importexcelAdmin-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="importExcelAdmin" tabindex="-1" role="dialog" aria-labelledby="importExcelAdminLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="importExcelAdminModalLabel">Import Excel</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="../functions/excel_dealer.php" method="POST" enctype="multipart/form-data">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <input type="hidden" class="form-control" name="location_admin" id="location_admin" value="<?php echo $code?>" placeholder="Admin" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Upload Excel File</label>
                                                        <div class="image-upload">
                                                            <input type="file" name="import_file1" class="custom-file-input" id="inputGroupFile01">
                                                            <div class="image-uploads">
                                                                <img src="../../assets/img/icons/upload.svg" alt="img">
                                                                    <h4>Drag and drop a file to upload</h4>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <button type="submit" name="save_excel_data_admin" class="btn btn-submit me-2">Add Stocks for Admin</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
</div>
<div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="../settings" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="popup" id="searchModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="searchModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Search by Barcode</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Search by Product Name</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
            $('#settings_usertype').val(usertype);
        }); 
    });
</script>

<script>
    function openPopup() {
        document.getElementById("addstocks-popup").style.display = "block";
    }

    function openPopupBranch(){
        document.getElementById("addstocksbranch-popup").style.display = "block";
    }

    function openExcel(){
        var selected = document.getElementById("branchID").value;
        var selected1 = document.getElementById("branch1").value;
        document.getElementById("excel").value = selected1;
        document.getElementById("excel").value = selected;
        document.getElementById("importexcel-popup").style.display = "block";
        console.log("Excel: ", selected);
        console.log("Excel1: ", selected1);

    }

    function openExcelAdmin(){
        document.getElementById("importexcelAdmin-popup").style.display = "block";
    }

    function openPopupSearchProduct() {
        var selectedBranch = document.getElementById("branch").value;
        document.getElementById("branchid").value = selectedBranch;
        document.getElementById("search-product").style.display = "block";
        console.log("Selected: ", selectedBranch);
    }

    function openSearchProductName() {
        var selected = document.getElementById("branchid").value;
        document.getElementById("branch1").value = selected;
        document.getElementById("searchname-popup").style.display = "block";
        console.log("Passed: ", selected);
    }

    function openSearchBarcode() {
        var selected = document.getElementById("branchid").value;
        document.getElementById("branchID").value = selected;
        document.getElementById("searchbarcode-popup").style.display = "block";
        console.log("Passed: ", selected);

    }

    function openSearchBranch() {
        document.getElementById("searchbarcode-popup").style.display = "block";
    }

    function openSelectBranch() {
        document.getElementById("select-branch").style.display = "block";
        document.getElementById("branch").addEventListener("change", function() {
            console.log(this.value);
        });
    }   
</script>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        const startFrom = document.getElementById('startfrom');
        const endTo = document.getElementById('endto');
        const branch = document.getElementById('branched');
        const table = document.querySelector('#productTable tbody');

        // Event listener for date inputs
        startFrom.addEventListener('change', function() {
            console.log('Start From: ', startFrom.value);
            fetchData();
        });

        endTo.addEventListener('change', function() {
            console.log('End To: ', endTo.value);
            fetchData();
        });

        branch.addEventListener('change', function() {
            console.log('Branch: ', branch.value);
            fetchData();
        });

        async function fetchData() {
            const startDate = startFrom.value;
            const endDate = endTo.value;
            const branchValue = branch.value;

            const formData = new FormData();
            formData.append('fromDate', startDate);
            formData.append('toDate', endDate);
            formData.append('branched', branchValue);

            try {
                const response = await fetch('fetchBranch_inflow.php', {
                    method: 'POST',
                    body: formData
                });
                const data = await response.json();
                //console.log('Data: ', data);

                table.innerHTML = ''; // Clear the table before adding new data
                data.forEach(item => {
                    console.log(`Barcode: ${item.barcode}, ProductName: ${item.product_name}, 
                                Dealer Price: ${item.dealerprice}, Supplier Price: ${item.supplierprice}, 
                                SRP: ${item.srp}`);

                    const row = document.createElement('tr');
                    row.innerHTML = `
                        <td>${item.date}</td>
                        <td>${item.barcode}</td>
                        <td>${item.product_name}</td>
                        <td>${item.dealerprice}</td>
                        <td>${item.supplierprice}</td>
                        <td>${item.srp}</td>
                        <td>${item.units}</td>
                        <td>${item.totalvalue_dealer}</td>
                        <td>${item.totalvalue}</td>
                        <td>${item.totalvalue_srp}</td>
                        <td>${item.location}</td>
                    `;

                    table.appendChild(row);
                });

            } catch (error) {
                console.error('Error fetching profit data:', error);
            }
        }
    });
</script>


</script>
<script>
    function fetchProductName() {
        var barcode = document.getElementById('barcode').value;
        if (barcode.trim() !== '') {
            $.ajax({
                type: 'POST',
                url: 'fetch_product.php',
                data: { barcode: barcode },
                success: function(response) {
                    console.log('Server response:', response); // Debugging line
                    try {
                        var data = JSON.parse(response);
                        if (data.error) {
                            alert(data.error);
                        } else {
                            console.log('Parsed data:', data); // Debugging line
                            document.getElementById('codeproductname').value = data.productname;
                            console.log(data.dealer_price);
                            document.getElementById('codedealerprice1').value = data.dealer_price;
                            document.getElementById('codesupplierprice1').value = data.supplier_price;
                            document.getElementById('codesrp1').value = data.srp;
                        }
                    } catch (e) {
                        alert('Error parsing response from server.');
                        console.error(e);
                    }
                },
                error: function(error) {
                    alert('Error fetching product name from the database.');
                    console.error(error);
                }
            });
        }
    }

    function fetchProductNameDealer() {
        var barcode = document.getElementById('barcode1').value;
        if (barcode.trim() !== '') {
            $.ajax({
                type: 'POST',
                url: 'fetch_dealer.php',
                data: { barcode: barcode },
                success: function(response) {
                    console.log('Server response:', response); // Debugging line
                    try {
                        var data = JSON.parse(response);
                        if (data.error) {
                            alert(data.error);
                        } else {
                            console.log('Parsed data:', data); // Debugging line
                            document.getElementById('productname').value = data.productname;
                            document.getElementById('dealerprice').value = data.dealer_price;
                            document.getElementById('supplierprice').value = data.supplier_price;
                            document.getElementById('srp').value = data.srp;
                        }
                    } catch (e) {
                        alert('Error parsing response from server.');
                        console.error(e);
                    }
                },
                error: function(error) {
                    alert('Error fetching product name from the database.');
                    console.error(error);
                }
            });
        }
    }

    function fetchBarcodeProduct() {
        var product_name = document.getElementById('searchproductname').value;
        if (product_name.trim() !== '') {
            $.ajax({
                type: 'POST',
                url: 'fetch_barcode.php',
                data: { searchproductname: product_name },
                success: function(response) {
                    console.log('Server response:', response); // Debugging line
                    try {
                        var data = JSON.parse(response);
                        if (data.error) {
                            alert(data.error);
                        } else {
                            console.log('Parsed data:', data); // Debugging line
                            document.getElementById('searchbarcode').value = data.barcode;
                            document.getElementById('searchdealerprice').value = data.dealer_price;
                            document.getElementById('searchsupplierprice').value = data.supplier_price;
                            document.getElementById('searchsrp').value = data.srp;
                        }
                    } catch (e) {
                        // alert('Error parsing response from server.');
                        // console.error(e);
                    }

                },
                error: function(error) {
                    alert('Error fetching barcode from the database.');
                    console.error(error);
                }
            });
        }
    }

    function fetchProductNameBranch() {
        var barcode = document.getElementById('barcodebranch').value;
        if (barcode.trim() !== '') {
            $.ajax({
                type: 'POST',
                url: 'fetch_product.php',
                data: { barcode: barcode },
                success: function(response) {
                    document.getElementById('productnamebranch').value = response;
                },
                error: function(error) {
                    alert('Error fetching product name from the database.');
                    console.error(error);
                }
            });
        }
    }

    $(document).ready(function(){
        $('#searchproductname').keyup(function(){
            var query = $(this).val();
            if(query != ''){
                $.ajax({
                    url: "search.php",
                    method: "POST",
                    data: {searchproductname: query},
                    success: function(data){
                        $('#productsnames').fadeIn();
                        $('#productsnames').html(data);
                    }
                });
            }else{
                $('#productsnames').fadeOut();
                $('#productsnames').html("");
            }
        });
        $(document).on('click', 'li', function(){
            $('#searchproductname').val($(this).text());
            $('#productsnames').fadeOut();

        });
    });
</script>
<script>
    $(document).ready(function(){
        $('#barcode').on('input', function() {
            var barcode = $(this).val();

            $.ajax({
                url: 'fetch_supplier_price.php',
                type: 'POST',
                data: {barcode: barcode},
                success: function(response) {
                    $('#supplierPrice').val(response);
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        });
    });
</script>
</body>
</html>