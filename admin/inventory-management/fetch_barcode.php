<?php
session_start();
include('../../connection.php');

// Fetch product name based on the scanned barcode
$productname = $_POST['searchproductname'];

// Prepare and execute the statement to select barcode by productname
$sql = "SELECT barcode, dealer_price, supplier_price, srp FROM products WHERE productname = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("s", $productname);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows > 0) {
    // Fetch the product details
    $row = $result->fetch_assoc();
    $response = [
        'barcode' => $row['barcode'],
        'dealer_price' => $row['dealer_price'],
        'supplier_price' => $row['supplier_price'],
        'srp' => $row['srp']
    ];
    echo json_encode($response);
}

// Close the statement and the connection
$stmt->close();
$conn->close();
?>
