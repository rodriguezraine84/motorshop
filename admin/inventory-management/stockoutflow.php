<!DOCTYPE html>
<?php
include('../../connection.php');
session_start();

if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo "Record not Found!";
    exit;
}

// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype FROM users WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
}
$productname = "";

if (isset($_POST['barcode'])) {
    $barcode = mysqli_real_escape_string($conn, $_POST['barcode']);

    $product = "SELECT products.productname FROM products 
                JOIN inflow_admin ON products.barcode = inflow_admin.barcode
                WHERE products.barcode = ?";
    $product_query = mysqli_prepare($conn, $product);
    mysqli_stmt_bind_param($product_query, "s", $barcode);
    mysqli_stmt_execute($product_query);
    mysqli_stmt_store_result($product_query);

    if(mysqli_stmt_num_rows($product_query) > 0){
        mysqli_stmt_bind_result($product_query, $productname);
        mysqli_stmt_fetch($product_query);
    }
}

if(isset($_POST['soldStocks']))
{
    $date = date("Y-m-d H:i:s");
    $barcode = $_POST['barcode'];
    $selling = $_POST['sellingprice'];
    $units = $_POST['units'];
    $discount = $_POST['discount'];
    $supplier_price = $_POST['supplierPrice'];
    
    $total_value = $selling * $units;
    $total_value_supplier = $supplier_price * $units;
    $profit =  ($selling * $units) - ($supplier_price * $units);
    $total_profit =  $profit - $discount;
    $outflow_selling = "INSERT INTO outflow_selling (`date`, `barcode`, `supplier_price`, `selling_price`, `units_sold`, `discount`, `total_value_selling`, `total_value_supplier`, `profit`, `total_profit`, `code`) VALUES (NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $onflow_query = mysqli_prepare($conn, $outflow_selling);
    mysqli_stmt_bind_param($onflow_query, "ssssssssss", $barcode, $supplier_price, $selling, $units, $discount, $total_value, $total_value_supplier, $profit, $total_profit, $code);
    mysqli_stmt_execute($onflow_query);
}

date_default_timezone_set('Asia/Manila');

$product_query = "SELECT DISTINCT
                    o.date,
                    o.barcode,
                    p.productname,
                    o.selling_price,
                    o.units_sold,
                    o.supplier_price,
                    o.total_value_selling,
                    o.total_value_supplier,
                    o.profit,
                    o.discount,
                    o.total_profit
                    FROM
                    outflow_selling o
                    JOIN
                    inflow_admin i ON o.barcode = i.barcode
                    JOIN
                    products p ON o.barcode = p.barcode
                    WHERE DATE(o.date) = CURDATE()";
$product_result = mysqli_query($conn, $product_query);

$product_query_branch = "SELECT DISTINCT
                    osb.date,
                    osb.barcode,
                    p.productname,
                    osb.selling_price,
                    osb.units_sold,
                    osb.supplier_price,
                    osb.total_value_selling,
                    osb.total_value_supplier,
                    osb.profit,
                    osb.discount,
                    osb.total_profit
                    FROM
                    outflow_selling_branch osb
                    JOIN
                    inflow_branch i ON osb.barcode = i.barcode
                    JOIN
                    products p ON osb.barcode = p.barcode
                    WHERE DATE(osb.date) = CURDATE()";
$product_result_branch = mysqli_query($conn, $product_query_branch);

?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href=".../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    -->
    <title>Herb and Angel | Stock Outflow</title>
</head>

<body>
<style>
        .page-title {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .page-title h4 {
            margin: 0;
        }

        .btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }
        .horizontal-line {
            display: block;
            width: 100%;
            border-bottom: 1px solid grey; /* Adjust color here */
        }


    </style>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
                <a href="./super-admin-dashboard.php" class="logo">
                    <img src="../../assets/img/logo (1).png" alt="">
                </a>

                <a href="super-admin-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
                </a>

                <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset" style="width: 120%; height: 10vh;">
                                <div class="profilesets" style="width: 120%; height: 20vh;">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <span class="horizontal-line"></span>
                            <a class="dropdown-item settingsBtn">
                                <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                                Settings
                            </a>
                            <a class="dropdown-item logout pb-0" href="../../index.php">
                                <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                                Logout
                            </a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <div class="profilesets">
                                <h5><?php echo $fullname?></h5>
                                <h6><?php echo $usertype?></h6>
                                </div>
                            </div>
                            <span class="horizontal-line"></span>
                            <a class="dropdown-item settingsBtn">
                                <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                                Settings
                            </a>
                            <a class="dropdown-item logout pb-0" href="../index.php">
                                <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                                Logout
                            </a>
                        </div>
                    </div>
            </div>
        </div>

        <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../super-admin-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-store-alt.svg" alt="img"><span> Dealer Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../dealer-management/records.php">Dealer Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-package.svg" alt="img"><span> Supplier Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../supplier-management/records.php">Supplier Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-git-branch.svg" alt="img"><span> Branch Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../branch-management/records.php">Branch Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-cog.svg" alt="img"><span> Services Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../services-management/services.php">Services</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../..//assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/product-list.php">Add Product</a></li>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>\
                                        <li><a href="../product-management/inflowproductlist.php">Inflow Product List</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                            </ul>
                                </li>
                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bxs-truck.svg" alt="img"><span> Logistics Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../logistics/records.php">Logistics Records</a></li>
                                    </ul>
                                </li>

                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wallet.svg" alt="img"><span> Expenses Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../expenses/records.php">Expenses Records</a></li>
                                    </ul>
                                </li>
                                
                                
                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> User Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../user-management/accounts.php">Accounts</a></li>
                                            </ul>
                                </li>   

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../report-generation/profit.php">Product Profit Report</a></li>
                                        <li><a href="../report-generation/mechanic.php">Mechanic Profit Report</a></li>
                                        <li><a href="../report-generation/logistics.php">Logistics Report</a></li>
                                        <li><a href="../report-generation/expenses.php">Expenses Report</a></li>
                                        <li><a href="../report-generation/productlist.php">Product List Report</a></li>
                                        <li><a href="../report-generation/report_stockinflow.php">Stock Inflow Report</a></li>
                                        <li><a href="../report-generation/report_stockoutflow.php">Stock Outflow Report</a></li>
                                    </ul>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="page-title">
                        <h2 style="font-weight: bold;">STOCK OUTFLOW</h2>
                        <div class="btn-container">
                            <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#soldStocks" onclick="openPopup()">Sold Stock</button>
                            <button type="button" class="btn btn-dark editBtn" title="Print" onclick="generatePDF()"> <i class="fas fa-print" style="margin-right: 5px;"></i>Print Report</button>
                        </div>
                    </div>
                    
                    <div style="overflow-x: auto;" id="reportTable">
                        <table class="table table-striped">
                            <thead>
                                <tr style="text-align: center;">
                                    <th scope="col">Date</th>
                                    <th scope="col">Product Barcode <br> Number</th>
                                    <th scope="col">Product <br>  Name</th>
                                    <th scope="col">Dealer Price</th>
                                    <th scope="col">Selling Price</th>
                                    <th scope="col">Units Sold</th>
                                    <th scope="col">Total value of <br>Dealer Price</th>
                                    <th scope="col">Total value of <br>Selling Price</th>
                                    <th scope="col">Supplier Price</th>
                                    <th scope="col">Total Value of <br>Supplier Price</th>
                                    <th scope="col">Profit</th>
                                    <th scope="col">Discount</th>
                                    <th scope="col">Total Profit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    // Loop through the product results and display them in the table
                                    while ($product_row = mysqli_fetch_assoc($product_result)) {
                                        echo '<tr>';
                                        echo '<td>' . $product_row['date'] . '</td>';
                                        echo '<td>' . $product_row['barcode'] . '</td>';
                                        echo '<td>' . $product_row['productname'] . '</td>';
                                        echo '<td>' . $product_row['selling_price'] . '</td>';
                                        echo '<td>' . $product_row['units_sold'] . '</td>';
                                        echo '<td>' . $product_row['total_value_selling'] . '</td>';
                                        echo '<td>' . $product_row['supplier_price'] . '</td>';
                                        echo '<td>' . $product_row['total_value_supplier'] . '</td>';
                                        echo '<td>' . $product_row['profit'] . '</td>';
                                        echo '<td>' . $product_row['discount'] . '</td>';
                                        echo '<td>' . $product_row['total_profit'] . '</td>';
                                        echo '</tr>';
                                    }
                                ?>
                            </tbody>
                            <tbody>
                                <?php
                                    // Loop through the product results and display them in the table
                                    while ($product_rows = mysqli_fetch_assoc($product_result_branch)) {
                                        echo '<tr>';
                                        echo '<td>' . $product_rows['date'] . '</td>';
                                        echo '<td>' . $product_rows['barcode'] . '</td>';
                                        echo '<td>' . $product_rows['productname'] . '</td>';
                                        echo '<td>' . $product_rows['selling_price'] . '</td>';
                                        echo '<td>' . $product_rows['units_sold'] . '</td>';
                                        echo '<td>' . $product_rows['total_value_selling'] . '</td>';
                                        echo '<td>' . $product_rows['supplier_price'] . '</td>';
                                        echo '<td>' . $product_rows['total_value_supplier'] . '</td>';
                                        echo '<td>' . $product_rows['profit'] . '</td>';
                                        echo '<td>' . $product_rows['discount'] . '</td>';
                                        echo '<td>' . $product_rows['total_profit'] . '</td>';
                                        echo '</tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>

    <!--Modal for Sold Stocks-->
    <div class="popup" id="soldStocks-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="soldStocks" tabindex="-1" role="dialog" aria-labelledby="soldStocksModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="soldStocksModalLabel">Sold Product</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST" id="soldStocksForm"> <!-- Added id to the form for easier selection -->
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Barcode</label>
                                                    <input type="text" class="form-control" name="barcode" id="barcode" value="<?php echo @$_POST['barcode']?>" onblur="fetchProductName()" placeholder="Enter Barcode" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" class="form-control" name="productname" id="productname" value="<?php echo $productname ?>" placeholder="Enter Product Name" readonly>
                                                </div>
                                            </div>
                                            <div class="alert alert-danger" role="alert" id="error-message" style="display:<?php echo empty($error_message) ? 'none' : 'block'; ?>">
                                                <?php echo $error_message; ?>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    
                                                    <label>Supplier Price</label>
                                                    <input type="text" class="form-control" id="supplierPrice" name="supplierPrice" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Selling Price</label>
                                                    <input type="text" class="form-control" name="sellingprice" id="sellingprice" value="<?php echo @$_POST['sellingprice']?>" placeholder="Enter Selling Price" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Units Sold</label>
                                                    <input type="text" class="form-control" name="units" id="units" placeholder="Enter Units Received" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Customer Discount</label>
                                                    <input type="text" class="form-control" name="discount" id="discount" placeholder="Enter Customer Discount">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="soldStocks" class="btn btn-submit me-2">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="../settings.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
            $('#settings_usertype').val(usertype);
        }); 
    });
</script>

<script>
    function openPopup() {
        document.getElementById("soldStocks-popup").style.display = "block";
    }
</script>

<script>
    function fetchProductName() {
        var barcode = document.getElementById('barcode').value;
        if (barcode.trim() !== '') {
            $.ajax({
                type: 'POST',
                url: 'fetch_name.php',
                data: { barcode: barcode },
                success: function(response) {
                    document.getElementById('productname').value = response;
                },
                error: function(error) {
                    alert('Error fetching product name from the database.');
                    console.error(error);
                }
            });
        }
    }
</script>

<script>
    function validateSellingPrice() {
        var sellingPrice = parseFloat($('#sellingprice').val());
        var errorMessage = document.getElementById("error-message");

        if (!isNaN(sellingPrice) && !isNaN(<?php echo $supplier_price; ?>) && sellingPrice <= <?php echo $supplier_price; ?>) {
            errorMessage.style.display = "block";
        } else {
            errorMessage.style.display = "none";
        }
    }

    $(document).ready(function() {
        // Trigger form validation when selling price is changed
        $('#sellingprice').on('input', function() {
            validateSellingPrice();
        });

        // Display error message if it exists
        <?php echo empty($error_message) ? '' : 'validateSellingPrice();'; ?>
    });
</script>

<script>
    function generatePDF() {
        // Function to print the table content
        var printContents = document.getElementById("reportTable").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    window.onafterprint = function(event) {
        setTimeout(function() {
            // Redirect back to product-management/barcode.php
            window.location.href = 'stockoutflow.php';
        }, 100);
    };
</script>

<script>
    $(document).ready(function(){
        $('#barcode').on('input', function() {
            var barcode = $(this).val();

            $.ajax({
                url: 'fetch_supplier_price.php',
                type: 'POST',
                data: {barcode: barcode},
                success: function(response) {
                    $('#supplierPrice').val(response);
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        });
    });
</script>
</body>
</html>