<?php
include('../../connection.php');
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $fromDate = isset($_POST['fromDate']) ? $_POST['fromDate'] : NULL;
    $toDate = isset($_POST['toDate']) ? $_POST['toDate'] : NULL;
    $branch = isset($_POST['branched']) ? $_POST['branched'] : NULL;

    if ($fromDate == NULL && $toDate == NULL) {
        // No dates provided, fetch today's data
        $product_query = "SELECT 
                                date,
                                barcode,
                                productname,
                                dealer_price,
                                supplier_price,
                                srp,
                                units_received,
                                totalvalue_dealer,
                                totalvalue,
                                totalvalue_srp,
                                location
                            FROM 
                                (
                                    SELECT 
                                        i.date,
                                        i.barcode,
                                        p.productname,
                                        i.dealer_price,
                                        i.supplier_price,
                                        i.srp,
                                        i.units_received,
                                        i.totalvalue_dealer,
                                        i.totalvalue,
                                        i.totalvalue_srp,
                                        i.location
                                    FROM 
                                        inflow_admin i
                                    JOIN 
                                        products p ON i.barcode = p.barcode
                                    WHERE 
                                        DATE(i.date) = CURDATE()
                                    UNION ALL

                                    SELECT 
                                        ib.date,
                                        ib.barcode,
                                        p.productname,
                                        ib.dealer_price,
                                        ib.supplier_price,
                                        ib.srp,
                                        ib.units_received,
                                        ib.totalvalue_dealer,
                                        ib.totalvalue,
                                        ib.totalvalue_srp,
                                        br.branch_address AS location
                                    FROM 
                                        inflow_branch ib
                                    JOIN 
                                        branch_record br ON ib.code = br.code
                                    JOIN 
                                        products p ON ib.barcode = p.barcode
                                    WHERE 
                                        DATE(ib.date) = CURDATE()
                                        AND ib.code = ?
                                ) AS combined_data
                            ORDER BY 
                                date DESC;";
        $product_stmt = mysqli_prepare($conn, $product_query);
        mysqli_stmt_bind_param($product_stmt, "s", $branch);
        mysqli_stmt_execute($product_stmt);
        $product_result = mysqli_stmt_get_result($product_stmt);
    } else {
        // Dates provided, fetch data between those dates
        $product_query = "SELECT 
                            i.date,
                            i.barcode,
                            p.productname,
                            i.dealer_price,
                            i.supplier_price,
                            i.srp,
                            i.units_received,
                            i.totalvalue_dealer,
                            i.totalvalue,
                            i.totalvalue_srp,
                            i.location
                        FROM 
                            inflow_admin i
                        JOIN 
                            products p ON i.barcode = p.barcode
                        WHERE
                            i.date BETWEEN ? AND ?
                        UNION
                        SELECT 
                            ib.date,
                            ib.barcode,
                            p.productname,
                            ib.dealer_price,
                            ib.supplier_price,
                            ib.srp,
                            ib.units_received,
                            ib.totalvalue_dealer,
                            ib.totalvalue,
                            ib.totalvalue_srp,
                            br.branch_address AS location
                        FROM 
                            inflow_branch ib
                        JOIN 
                            branch_record br ON ib.code = br.code
                        JOIN 
                            products p ON ib.barcode = p.barcode
                        WHERE
                            ib.date BETWEEN ? AND ? 
                            AND ib.code = ?
                        ORDER BY 
                            date DESC";
        $product_stmt = mysqli_prepare($conn, $product_query);
        mysqli_stmt_bind_param($product_stmt, "sssss", $fromDate, $toDate, $fromDate, $toDate, $branch);
        mysqli_stmt_execute($product_stmt);
        $product_result = mysqli_stmt_get_result($product_stmt);
    }

    $products = []; // Initialize the products array

    while ($product_row = mysqli_fetch_assoc($product_result)) {
        $date = date('m-d-Y', strtotime($product_row['date']));
        $barcode = $product_row['barcode'];
        $productname = $product_row['productname'];
        $dealerprice = $product_row['dealer_price'];
        $supplierprice = $product_row['supplier_price'];
        $srp = $product_row['srp'];
        $units_received = $product_row['units_received'];
        $totalvalue_dealer = $product_row['totalvalue_dealer'];
        $totalvalue = $product_row['totalvalue'];
        $totalvalue_srp = $product_row['totalvalue_srp'];
        $location = $product_row['location'];

        $products[] = [
            'date' => $date,
            'barcode' => $barcode,
            'product_name' => $productname,
            'dealerprice' => $dealerprice,
            'supplierprice' => $supplierprice,
            'srp' => $srp,
            'units' => $units_received,
            'totalvalue_dealer' => $totalvalue_dealer,
            'totalvalue' => $totalvalue,
            'totalvalue_srp' => $totalvalue_srp,
            'location' => $location
        ];
    }

    header('Content-Type: application/json');
    echo json_encode($products); // Correctly return the products array
    
} else {
    // Handle invalid request method
    header('HTTP/1.1 405 Method Not Allowed');
    echo json_encode(['error' => 'Invalid request method']);
}
?>
