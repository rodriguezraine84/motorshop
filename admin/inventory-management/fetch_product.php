<?php
session_start();
include('../../connection.php');

// Fetch product name based on the scanned barcode
$barcode = $_POST['barcode'];

// Prepare and execute the statement to select product details by barcode
$sql = "SELECT productname, dealer_price, supplier_price, srp FROM products WHERE barcode = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("s", $barcode);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows > 0) {
    // Fetch the product details
    $row = $result->fetch_assoc();
    $response = [
        'productname' => $row['productname'],
        'dealer_price' => $row['dealer_price'],
        'supplier_price' => $row['supplier_price'],
        'srp' => $row['srp']
    ];
    echo json_encode($response);
} else {
    echo json_encode(['error' => 'Product not found']);
}

// Close the statement and the connection
$stmt->close();
$conn->close();
?>
