<?php 
session_start();
include('../../connection.php');
error_reporting(E_ALL);
ini_set('display_errors', 1);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $branch = $_POST['branched'];

    $product_query = "SELECT
                            p.productname,
                            i.units_received,
                            COALESCE(o.units_sold, 0) AS units_sold,
                            COALESCE(r.returned_units, 0) AS returned_units,
                            (i.units_received - COALESCE(o.units_sold, 0) + COALESCE(r.returned_units, 0)) AS available_stocks,
                            o.selling_price,
                            o.supplier_price
                        FROM
                            products p
                        JOIN (
                            SELECT
                                barcode,
                                COALESCE(SUM(units_received), 0) AS units_received,
                                code
                            FROM
                                inflow_branch
                            GROUP BY
                                barcode, code
                        ) i ON p.barcode = i.barcode
                        LEFT JOIN (
                            SELECT
                                barcode,
                                supplier_price,
                                selling_price,
                                COALESCE(SUM(units_sold), 0) AS units_sold
                            FROM
                                outflow_selling_branch
                            GROUP BY
                                barcode
                        ) o ON p.barcode = o.barcode
                        LEFT JOIN (
                            SELECT
                                barcode,
                                COALESCE(SUM(returned_units), 0) AS returned_units
                            FROM
                                returned_product
                            GROUP BY
                                barcode
                        ) r ON p.barcode = r.barcode
                        JOIN users_branch ub ON i.code = ub.branch_code
                        WHERE ub.code = ?";

    $product_stmt = mysqli_prepare($conn, $product_query);
    mysqli_stmt_bind_param($product_stmt, "s", $branch);
    mysqli_stmt_execute($product_stmt);
    $product_result = mysqli_stmt_get_result($product_stmt);
    $products = []; // Initialize the products array

    while ($product_row = mysqli_fetch_assoc($product_result)) {
        
        $productname = $product_row['productname'];
        $inflow = $product_row['units_received'];
        $outflow = $product_row['units_sold'];
        $available = $product_row['available_stocks'];
        $supplier = $product_row['supplier_price'];
        $srp = $product_row['selling_price'];
        $totalvalue_supplier = $supplier * $inflow;
        $totalvalue_srp = $srp * $inflow;

        $products[] = [
            'productname' => $productname,
            'received' => $inflow,
            'sold' => $outflow,
            'availablestocks' => $available,
            'supplierprice' => $supplier,
            'srp' => $srp,
            'totalvalue' => $totalvalue_supplier,
            'totalvalue_srp' => $totalvalue_srp
        ];
    }

    header('Content-Type: application/json');
    echo json_encode($products); // Correctly return the products array
    
} else {
    // Handle invalid request method
    header('HTTP/1.1 405 Method Not Allowed');
    echo json_encode(['error' => 'Invalid request method']);
}
?>