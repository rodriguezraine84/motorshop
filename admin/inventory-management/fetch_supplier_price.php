<?php
session_start();
include('../../connection.php');

if(isset($_POST['barcode'])) {
    // Sanitize the barcode input
    $barcode = mysqli_real_escape_string($conn, $_POST['barcode']);

    // Prepare and execute the statement to fetch the supplier price for the specified barcode
    $supplier_price_query = "SELECT supplier_price, dealer_price, srp
                                FROM products AS p
                                JOIN (
                                    SELECT barcode, MAX(date) AS max_date
                                    FROM products
                                    GROUP BY barcode
                                ) AS latest_dates
                                    ON p.barcode = latest_dates.barcode AND p.date = latest_dates.max_date
                                    WHERE p.barcode = ?";
    
    $stmt = mysqli_prepare($conn, $supplier_price_query);
    mysqli_stmt_bind_param($stmt, "s", $barcode);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);

    // Check if the query was successful
    if ($result) {
        // Check if any rows were returned
        if (mysqli_num_rows($result) > 0) {
            // Fetch the supplier price from the result
            $row = mysqli_fetch_assoc($result);
            $supplier_price = $row['supplier_price'];
            $dealer_price = $row['dealer_price'];
            $srp = $row['srp'];
            // Return the supplier price as response
            echo $supplier_price;
            echo $dealer_price;
            echo $srp;
        } else {
            // If no rows were returned, return a message indicating supplier price not found
            echo "Prices not found for the specified product.";
        }
    } else {
        // If there was an error executing the query, return the error message
        echo "Error: " . mysqli_error($conn);
    }

    // Close the statement and the database connection
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
} else {
    // If barcode is not set in the POST request, return an error message
    echo "Barcode not provided.";
}
?>
