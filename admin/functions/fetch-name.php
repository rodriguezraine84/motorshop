<?php
include('../../connection.php');

$barcode = $_POST['barcode'];

// Prepare and execute the statement to select productname by barcode
$sql = "SELECT productname FROM products WHERE barcode = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("s", $barcode);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows > 0) {
    // Return the product name
    $row = $result->fetch_assoc();
    echo $row['productname'];
} else {
    echo "Product not found";
}

// Close the statement and the connection
$stmt->close();
$conn->close();
?>
