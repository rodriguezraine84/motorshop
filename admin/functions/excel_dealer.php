<?php

session_start();
include('../../connection.php');

require '../../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if(isset($_POST['save_excel_data_admin']))
{
    $fileName = $_FILES['import_file1']['name'];
    $file_ext = pathinfo($fileName, PATHINFO_EXTENSION);

    $allowed_ext = ['xls','csv','xlsx'];

    if(in_array($file_ext, $allowed_ext))
    {
        $inputFileNamePath = $_FILES['import_file1']['tmp_name'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileNamePath);
        $data = $spreadsheet->getActiveSheet()->toArray();

        $count = 0;
        foreach($data as $row)
        {
            if($count > 0)
            {
                $date = $row['1'];
                $productname = $row['2'];
                $dealerprice = $row['3']; 
                $supplier_price = str_replace(',', '', $row['4']);
                $srp = $row['5'];
                $units = $row['6'];
                $unixTimestamp = strtotime(str_replace('-', '/', $date));
                $formatted_date = date('Y-m-d', $unixTimestamp);

                $sql = "SELECT dealer_price, supplier_price, srp FROM products WHERE productname = '$productname'";
                $result1 = mysqli_query($conn, $sql);
                $row1 = mysqli_fetch_assoc($result1);
                $old_dealer_price = $row1['dealer_price'];
                $old_supplier_price = $row1['supplier_price'];
                $old_srp = $row1['srp'];

                $totalvalueDealer = $old_dealer_price * $units;
                $totalvalueSupplier = $old_supplier_price * $units;
                $totalvalueSrp = $old_srp * $units;

                // Prepare the first query
                $productQuery = "INSERT INTO excel_inflow (date, productname, dealerprice, supplierprice, srp, units, dealer, supplier, ssrp) VALUES (?, ?, '$old_dealer_price', '$old_supplier_price', '$old_srp', ?, '$totalvalueDealer', '$totalvalueSupplier', '$totalvalueSrp')";
                $stmt = mysqli_prepare($conn, $productQuery);

                // Bind parameters
                mysqli_stmt_bind_param($stmt, "ssi", $formatted_date, $productname, $units);

                // Execute the statement
                $result = mysqli_stmt_execute($stmt);

                if ($result) {
                    // Data successfully inserted into excel_import table, now insert into inflow_admin table
                    $code = $_POST['location_admin'];
                    $location = "Admin";
                    // Prepare the second query
                    $inflowQuery1 = "INSERT INTO inflow_admin (`date`, `barcode`, `dealer_price`, `supplier_price`,  `srp`, `units_received`, `totalvalue`, `totalvalue_dealer`, `totalvalue_srp`, `code`, `location`)
                                    SELECT e.date, p.barcode, e.dealerprice, e.supplierprice,  e.srp, e.units, e.supplier, e.dealer, e.ssrp, ?, ?
                                    FROM excel_inflow e 
                                    JOIN products p ON e.productname = p.productname";
                    $stmt2 = mysqli_prepare($conn, $inflowQuery1);

                    // Bind parameters
                    mysqli_stmt_bind_param($stmt2, "ss", $code, $location);

                    // Execute the statement
                    $inflowResult1 = mysqli_stmt_execute($stmt2);
                    
                    if($inflowResult1){
                        // Prepare and execute the delete query
                        $deleteQuery1 = "DELETE FROM excel_inflow";
                        $stmt3 = mysqli_prepare($conn, $deleteQuery1);
                        mysqli_stmt_execute($stmt3);
                    }
                } else {
                    $_SESSION['message'] = "Error Inserting Data!";
                    $_SESSION['message_type'] = "danger";
                    header('Location: ../inventory-management/stockinflow.php');
                    exit(0);
                }

                
            }
            else
            {
                $count = 1;
            }

            if(isset($msg))
                {
                    $_SESSION['message'] = "Successfully Imported";
                    $_SESSION['message_type'] = "success";
                    header('Location: ../inventory-management/stockinflow.php');
                    exit(0);
                }
        }

        if(isset($msg))
        {
            $_SESSION['message'] = "Successfully Imported";
            $_SESSION['message_type'] = "success";
            header('Location: ../inventory-management/stockinflow.php');
            exit(0);
        }
    }
    else
    {
        $_SESSION['message'] = "Invalid File";
        $_SESSION['message_type'] = "danger";
        header('Location: ../inventory-management/stockinflow.php');
        exit(0);
    }
}

?>