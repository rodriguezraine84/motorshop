<?php 
session_start();
include('../../connection.php');

require '../../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if(isset($_POST['save_excel_data']))
{
    $fileName = $_FILES['import_file']['name'];
    $file_ext = pathinfo($fileName, PATHINFO_EXTENSION);

    $allowed_ext = ['xls','csv','xlsx'];

    if(in_array($file_ext, $allowed_ext))
    {
        $inputFileNamePath = $_FILES['import_file']['tmp_name'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileNamePath);
        $data = $spreadsheet->getActiveSheet()->toArray();

        $count = 0;
        $existingProducts = array(); // Store existing product names
        foreach($data as $row)
        {
            if($count > 0)
            {
                $name = $row['0'];
                $dealer = str_replace(',', '', $row['1']); 
                $supplier = str_replace(',', '', $row['2']); 
                $srp = str_replace(',', '', $row['3']);

                // Prepare and execute statement to check if the product already exists
                $checkQuery = "SELECT COUNT(*) as count FROM products WHERE productname = ?";
                $stmt_check = mysqli_prepare($conn, $checkQuery);
                mysqli_stmt_bind_param($stmt_check, "s", $name);
                mysqli_stmt_execute($stmt_check);
                mysqli_stmt_store_result($stmt_check);
                mysqli_stmt_bind_result($stmt_check, $count_result);
                mysqli_stmt_fetch($stmt_check);

                if($count_result == 0)
                {
                    // Product does not exist, insert it
                    $productQuery = "INSERT INTO products (productname, barcode, dealer_price, supplier_price, srp) VALUES (?, ?, ?, ?, ?)";
                    $stmt_insert = mysqli_prepare($conn, $productQuery);
                    mysqli_stmt_bind_param($stmt_insert, "sssss", $name, $barcode, $dealer, $supplier, $srp);
                    $result = mysqli_stmt_execute($stmt_insert);
                    mysqli_stmt_close($stmt_insert);
                }
                else
                {
                    // Product with same name already exists, add to existingProducts array
                    $existingProducts[] = $name;
                }

                mysqli_stmt_close($stmt_check);
            }
            else
            {
                $count = 1;
            }
        }

        if(count($existingProducts) > 0)
        {
            // Some products already existed, display error message with list of existing products
            $_SESSION['message'] = "Successfully Imported Non-Existing Product! ";
            $_SESSION['message_type'] = "info";
        }
        else
        {
            // All products were successfully inserted
            $_SESSION['message'] = "Successfully Imported";
            $_SESSION['message_type'] = "success";
        }
        header('Location: ../product-management/product-list.php');
        exit(0);
    }
    else
    {
        $_SESSION['message'] = "Invalid File";
        $_SESSION['message_type'] = "danger";
        header('Location: ../product-management/product-list.php');
        exit(0);
    }
}
?>
