<?php
session_start();
include('../../connection.php');

if(isset($_POST['addstocks']))
{
    $barcode_branch = $_POST['barcode1'];
    $supplier_branch = $_POST['supplierprice'];
    $units_branch = $_POST['units'];
    $dealerprice = $_POST['dealerprice'];
    $srp = $_POST['srp'];
    $total_value = $supplier_branch * $units_branch;
    $totalvalue_dealer = $dealerprice * $units_branch;
    $totalvalue_srp = $srp * $units_branch;
    $total_value_branch = $supplier_branch * $units_branch;
    $user_branch_code = $_POST['location_admin'];
    $location = "Admin";
    // Using NOW() to insert the current date and time
    $inflow_branch = "INSERT INTO inflow_admin (`date`, `barcode`, `supplier_price`, `dealer_price`, `srp`, `units_received`, `totalvalue`, `totalvalue_dealer`, `totalvalue_srp`, `code`, `location`) VALUES (NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $inflow_branch_query = mysqli_prepare($conn, $inflow_branch);
    mysqli_stmt_bind_param($inflow_branch_query, "ssssssssss", $barcode_branch, $supplier_branch, $dealerprice, $srp, $units_branch, $total_value, $totalvalue_dealer, $totalvalue_srp, $user_branch_code, $location);
    $insert_user_query = mysqli_stmt_execute($inflow_branch_query);

    if ($insert_user_query) {
        $_SESSION['message'] = "Successfully Added Inflow";
        $_SESSION['message_type'] = "success";
        header("Location: ../inventory-management/stockinflow.php");
        exit();
    } else {
        $_SESSION['message'] = "Failed to Add Inflow";
        $_SESSION['message_type'] = "danger";
        header("Location: ../inventory-management/stockinflow.php");
        exit();
    }
}
?>