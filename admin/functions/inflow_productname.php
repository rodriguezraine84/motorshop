<?php 
session_start();
include('../../connection.php');

if(isset($_POST['addstocksname']))
{
    $barcode = $_POST['searchbarcode'];
    $supplier = $_POST['searchsupplierprice'];
    $units = $_POST['searchunits'];
    $dealerprice = $_POST['searchdealerprice'];
    $srp = $_POST['searchsrp'];
    $total_value = $supplier * $units;
    $totalvalue_dealer = $dealerprice * $units;
    $totalvalue_srp = $srp * $units;
    $location_branch = $_POST['branch1'];
    $admin = $_POST['code1'];
    // Using NOW() to insert the current date and time
    $inflow_branch = "INSERT INTO inflow_branch (`date`, `barcode`, `supplier_price`, `dealer_price`, `srp`, `units_received`, `totalvalue`, `totalvalue_dealer`, `totalvalue_srp`, `code`, `admin_code`) VALUES (NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $inflow_query = mysqli_prepare($conn, $inflow_branch);
    mysqli_stmt_bind_param($inflow_query, "ssssssssss", $barcode, $supplier, $dealerprice, $srp, $units, $total_value, $totalvalue_dealer, $totalvalue_srp, $location_branch, $admin);
    $insert_user_query = mysqli_stmt_execute($inflow_query);

    if ($insert_user_query) {
        $_SESSION['message'] = "Successfully Added Inflow";
        $_SESSION['message_type'] = "success";
        header("Location: ../inventory-management/stockinflow.php");
        exit();
    } else {
        $_SESSION['message'] = "Failed to Add Inflow";
        $_SESSION['message_type'] = "danger";
        header("Location: ../inventory-management/stockinflow.php");
        exit();
    }
}

?>