<?php
session_start();
include('../../connection.php');

if(isset($_POST['codeaddstocks']))
{
    $barcode_branch = $_POST['barcode'];
    $supplier_branch = $_POST['codesupplierprice'];
    $units_branch = $_POST['codeunits'];
    $dealerprice = $_POST['codedealerprice'];
    $srp = $_POST['codesrp'];
    $total_value = $supplier_branch * $units_branch;
    $totalvalue_dealer = $dealerprice * $units_branch;
    $totalvalue_srp = $srp * $units_branch;
    $total_value_branch = $supplier_branch * $units_branch;
    $user_branch_code = $_POST['branchID'];
    $admin = $_POST['code_admin'];
    $dealer = 0;
    // Using NOW() to insert the current date and time
    $inflow_branch = "INSERT INTO inflow_branch (`date`, `barcode`, `supplier_price`, `dealer_price`, `srp`, `units_received`, `totalvalue`, `totalvalue_dealer`, `totalvalue_srp`, `code`, `admin_code`) VALUES (NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $inflow_branch_query = mysqli_prepare($conn, $inflow_branch);
    mysqli_stmt_bind_param($inflow_branch_query, "ssssssssss", $barcode_branch, $supplier_branch, $dealerprice, $srp, $units_branch, $total_value, $totalvalue_dealer, $totalvalue_srp, $user_branch_code, $admin);
    $insert_user_query = mysqli_stmt_execute($inflow_branch_query);

    if ($insert_user_query) {
        $_SESSION['message'] = "Successfully Added Inflow";
        $_SESSION['message_type'] = "success";
        header("Location: ../inventory-management/stockinflow.php");
        exit();
    } else {
        $_SESSION['message'] = "Failed to Add Inflow";
        $_SESSION['message_type'] = "danger";
        header("Location: ../inventory-management/stockinflow.php");
        exit();
    }
}
?>