<?php
session_start();
include('../../connection.php');

if (isset($_POST['addproduct'])) {
    $barcode = $_POST['barcode'];
    $product_name = $_POST['productname'];
    $description = $_POST['description'];
    $model = $_POST['model'];
    $dealer = $_POST['dealerprice'];
    $supplier = $_POST['supplierprice'];
    $srp = $_POST['srp'];

    // Prepare the SELECT statement to check for duplicates
    $check_duplicate = "SELECT * FROM products WHERE barcode = ? OR productname = ?";
    $stmt_check_duplicate = mysqli_prepare($conn, $check_duplicate);
    mysqli_stmt_bind_param($stmt_check_duplicate, "ss", $barcode, $product_name);
    mysqli_stmt_execute($stmt_check_duplicate);
    mysqli_stmt_store_result($stmt_check_duplicate);

    if (mysqli_stmt_num_rows($stmt_check_duplicate) > 0) {
        // Duplicate found
        $_SESSION['message'] = "Error: Product with name '$product_name' already exists.";
        $_SESSION['message_type'] = "danger";
        mysqli_stmt_close($stmt_check_duplicate);
        header('Location: ../product-management/product-list.php');
        exit(0);
    }

    // Prepare the INSERT statement for adding new product
    $product_add = "INSERT INTO products (barcode, productname, dealer_price, supplier_price, srp) VALUES (?, ?, ?, ?, ?)";
    $stmt_product_add = mysqli_prepare($conn, $product_add);
    mysqli_stmt_bind_param($stmt_product_add, "sssss", $barcode, $product_name, $dealer, $supplier, $srp);
    $product_add_query = mysqli_stmt_execute($stmt_product_add);

    if ($product_add_query) {
        $_SESSION['message'] = "Successfully Added Product";
        $_SESSION['message_type'] = "success";
        mysqli_stmt_close($stmt_product_add);
        header('Location: ../product-management/product-list.php');
        exit(0);
    } else {
        $_SESSION['message'] = "Failed to Add Product";
        $_SESSION['message_type'] = "danger";
        mysqli_stmt_close($stmt_product_add);
        header('Location: ../product-management/product-list.php');
        exit(0);
    }
}
?>
