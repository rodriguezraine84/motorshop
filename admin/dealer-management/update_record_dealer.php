<?php
include('../../connection.php');
session_start();

if(isset($_POST['updaterecord'])) {   
    $code = $_POST['edit_code'];
    $dealer_name = $_POST['edit_dealername'];
    $dealer_address = $_POST['edit_address'];
    $contact_person = $_POST['edit_person'];
    $contact_number = $_POST['edit_number'];
    $email = $_POST['edit_email'];

    // Prepare and execute statement to update dealer record
    $query = mysqli_prepare($conn, "UPDATE dealer_record SET 
        dealer_name=?, dealer_address=?, contact_person=?, contact_number=?, email=?
        WHERE code=?");
    mysqli_stmt_bind_param($query, "ssssss", $dealer_name, $dealer_address, $contact_person, $contact_number, $email, $code);
    $query_run = mysqli_stmt_execute($query);

    if($query_run) {
        $_SESSION['message'] = "Successfully Updated Dealer";
        $_SESSION['message_type'] = "success";              
        header("Location: ../dealer-management/records.php");
        exit();
    } else {
        $_SESSION['message'] = "Failed to Update Dealer";
        $_SESSION['message_type'] = "danger";    
        header("Location: ../dealer-management/records.php");
        exit();
    }
}
?>
