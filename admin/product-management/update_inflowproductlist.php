<?php
include('../../connection.php');
session_start();

if(isset($_POST['resetrecord'])) {   
    $code = $_POST['edit_id'];
    $totalvalueDealer = $_POST['edit_totalvalueDealer'];
    $totalvalueSupplier = $_POST['edit_totalvalueSupplier'];
    $totalvaluesrp = $_POST['edit_totalvalueSrp'];
    $units = $_POST['edit_units'];

    $query = "UPDATE inflow_admin SET units_received =?, totalvalue=?, totalvalue_dealer=?, totalvalue_srp=? WHERE id=?";
    $stmt = mysqli_prepare($conn, $query);

    if ($stmt) {
        mysqli_stmt_bind_param($stmt, "sssss", $units, $totalvalueSupplier, $totalvalueDealer, $totalvaluesrp, $code);
        $query_run = mysqli_stmt_execute($stmt);

        if($query_run) {
            $_SESSION['message'] = "Successfully Updated Inflow";
            $_SESSION['message_type'] = "success";  
            header("Location: ../product-management/inflowproductlist.php");
            exit();
        } else {
            $_SESSION['message'] = "Failed to Update Account";
            $_SESSION['message_type'] = "danger";   
            header("Location: ../product-management/inflowproductlist.php");
            exit();
        }
    } else {
        $_SESSION['message'] = "Prepared statement error";
        $_SESSION['message_type'] = "danger";   
        header("Location: ../product-management/inflowproductlist.php");
        exit();
    }
}
?>

