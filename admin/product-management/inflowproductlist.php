<?php 
include('../../connection.php');
session_start();

if(isset($_SESSION["code"])) {
    $code = $_SESSION["code"];
} else {
    echo "Record not Found!";
    exit;
}

// Prepare and execute statement to fetch user information
// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype FROM users WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
}

// Prepare and execute statement to fetch prices from admin inflow and selling outflow
$product_query = "SELECT 
                      i.*, 
                      p.productname
                  FROM 
                      inflow_admin i
                  JOIN 
                      products p ON p.barcode = i.barcode"; 
$stmt_product = mysqli_prepare($conn, $product_query);
mysqli_stmt_execute($stmt_product);
$product_result = mysqli_stmt_get_result($stmt_product);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->
    <title>Herb and Angel | Inflow Product List</title>
</head>
<body>
    <style>
        .horizontal-line {
            display: block;
            width: 100%;
            border-bottom: 1px solid grey; /* Adjust color here */
        }

    </style>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
               <a href="./super-admin-dashboard.php" class="logo">
                 <img src="../../assets/img/logo (1).png" alt="">
               </a>

               <a href="super-admin-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

           <!--Underline Nav-->
           <ul class="nav user-menu">

            <!--Nav Items Dropdown-->
            <li class="nav-item dropdown has-arrow main-drop">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset" style="width: 120%; height: 10vh;">
                            <div class="profilesets" style="width: 120%; height: 20vh;">
                            <h6><?php echo $fullname?></h6>
                            <h5><?php echo $usertype?></h5>
                            </div>
                        </div>
                        <span class="horizontal-line"></span>
                        <a class="dropdown-item settingsBtn">
                            <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                            Settings
                        </a>
                        <a class="dropdown-item logout pb-0" href="../../index.php">
                            <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                            Logout
                        </a>
                    </div>
                </div>
            </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset">
                            <div class="profilesets">
                            <h5><?php echo $fullname?></h5>
                            <h6><?php echo $usertype?></h6>
                            </div>
                        </div>
                        <span class="horizontal-line"></span>
                        <a class="dropdown-item settingsBtn">
                            <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                            Settings
                        </a>
                        <a class="dropdown-item logout pb-0" href="../index.php">
                            <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                            Logout
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../super-admin-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-store-alt.svg" alt="img"><span> Dealer Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../dealer-management/records.php">Dealer Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-package.svg" alt="img"><span> Supplier Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../supplier-management/records.php">Supplier Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-git-branch.svg" alt="img"><span> Branch Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../branch-management/records.php">Branch Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-cog.svg" alt="img"><span> Services Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../services-management/services.php">Services</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../..//assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/product-list.php">Add Product</a></li>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                        <li><a href="../product-management/inflowproductlist.php">Inflow Product List</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                            </ul>
                                </li>
                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bxs-truck.svg" alt="img"><span> Logistics Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../logistics/records.php">Logistics Records</a></li>
                                    </ul>
                                </li>

                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wallet.svg" alt="img"><span> Expenses Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../expenses/records.php">Expenses Records</a></li>
                                    </ul>
                                </li>
                                
                                
                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> User Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../user-management/accounts.php">Accounts</a></li>
                                            </ul>
                                </li>   

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../report-generation/profit.php">Product Profit Report</a></li>
                                        <li><a href="../report-generation/mechanic.php">Mechanic Profit Report</a></li>
                                        <li><a href="../report-generation/logistics.php">Logistics Report</a></li>
                                        <li><a href="../report-generation/expenses.php">Expenses Report</a></li>
                                        <li><a href="../report-generation/productlist.php">Product List Report</a></li>
                                        <li><a href="../report-generation/report_stockinflow.php">Stock Inflow Report</a></li>
                                        <li><a href="../report-generation/report_stockoutflow.php">Stock Outflow Report</a></li>
                                    </ul>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="page-title">
                        <h2 style="font-weight: bold;">INFLOW PRODUCT LIST</h2>
                    </div>
                        <div class="table-responsive">
                            <table class="table table-striped" id="productTable">
                                <thead>
                                    <tr>
                                        <th scope="col" style="text-align: center;">Date</th>
                                        <th scope="col" style="text-align: center;">Barcode <br> Number</th>
                                        <th scope="col" class="wrap-text" style="text-align: center;">Product <br> Name</th>
                                        <th scope="col" style="text-align: center;">Dealer <br> Price</th>
                                        <th scope="col" style="text-align: center;">Supplier <br> Price</th>
                                        <th scope="col" style="text-align: center;">SRP</th>
                                        <th scope="col" style="text-align: center;">Units <br> Received</th>
                                        <th scope="col" class="wrap-text" style="text-align: center;">Dealer Price <br>Total Value</th>
                                        <th scope="col" class="wrap-text" style="text-align: center;">Supplier Price <br>Total Value</th>
                                        <th scope="col" class="wrap-text" style="text-align: center;">SRP <br> Total Value</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        // Check if the result is valid before fetching data
                                        if ($product_result) {
                                            while($product_row =  mysqli_fetch_assoc($product_result)){
                                                echo '<tr>';
                                                echo '<td>' . date('m-d-Y', strtotime($product_row['date'])) . '</td>';
                                                echo '<td>' . $product_row['barcode'] . '</td>';
                                                echo '<td>' . $product_row['productname'] . '</td>';
                                                echo '<td>' . number_format($product_row['dealer_price'], 2) . '</td>';
                                                echo '<td>' . number_format($product_row['supplier_price'], 2) . '</td>';
                                                echo '<td>' . number_format($product_row['srp'], 2) . '</td>';
                                                echo '<td>' . $product_row['units_received'] . '</td>';
                                                echo '<td>' . $product_row['totalvalue_dealer'] . '</td>';
                                                echo '<td>' . $product_row['totalvalue'] . '</td>';
                                                echo '<td>' . $product_row['totalvalue_srp'] . '</td>';
                                                echo '<td style="display:none;">' . $product_row['id'] . '</td>';
                                                echo '<td>';
                                                echo '<button type="button" class="btn btn-success editBtn" style="margin-right: 5px;" title="Edit"> <i class="fas fa-edit"></i> </button>';
                                                echo '<button type="button" class="btn btn-danger deleteBtn"><i class="fas fa-trash-alt" title="Delete"></i> </button>';
                                                echo '</td>';
                                                echo '</tr>'; 
                                            }
                                        } else {
                                            echo "Error: " . mysqli_error($conn);
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
    </div>

    <!--EDIT MODAL-->
    <div class="popup" id="editModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="addrecordModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addrecordModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="update_inflowproductlist.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="edit_code" id="edit_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Date</label>
                                                    <input type="text" class="form-control" name="edit_date" id="edit_date" value="" placeholder="Enter Barcode" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Barcode Number</label>
                                                    <input type="text" class="form-control" name="edit_barcode" id="edit_barcode" placeholder="Enter Barcode" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" class="form-control" name="edit_productname" id="edit_productname" placeholder="Enter Product Name" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Dealer Price</label>
                                                    <input type="text" class="form-control" name="edit_dealerprice" id="edit_dealerprice" placeholder="Enter Dealer Price" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Supplier Price</label>
                                                    <input type="text" class="form-control" name="edit_supplierprice" id="edit_supplierprice" placeholder="Enter Supplier Price" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>SRP</label>
                                                    <input type="text" class="form-control" name="edit_srp" id="edit_srp" placeholder="Enter SRP" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Units Received</label>
                                                    <input type="text" class="form-control" name="edit_units" id="edit_units" placeholder="Enter Units Received">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Total Value (Dealer Price)</label>
                                                    <input type="text" class="form-control" name="edit_totalvalueDealer" id="edit_totalvalueDealer" placeholder="Enter Total Value (Dealer Price)" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Total Value (Supplier Price)</label>
                                                    <input type="text" class="form-control" name="edit_totalvalueSupplier" id="edit_totalvalueSupplier" placeholder="Enter Total Value (Supplier Price)" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Total Value (SRP)</label>
                                                    <input type="text" class="form-control" name="edit_totalvalueSrp" id="edit_totalvalueSrp" placeholder="Enter Total Value (SRP)" readonly>
                                                </div>
                                            </div>
                                            <input type="hidden" class="form-control" name="edit_id" id="edit_id" value="">
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <!--DELETE MODAL-->
    <div class="popup" id="deleteModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalLabel">Delete Record</h5>
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="delete_record_dealer.php" method="POST">
                            
                            <div class="modal-body">   
                                <input type="hidden" class="form-control" name="delete_code" id="delete_code">
                                <h4>Are you sure you want to delete this record?</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="recorddelete" class="btn btn-primary">Delete Record</button>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="../settings.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
            $('#settings_usertype').val(usertype);
        }); 
    });
</script>
<script>
    $(document).ready(function () {

        $('.editBtn').on('click', function () {

            $('#editModal').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function () {
                return $(this).text();
            }).get();

            console.log(data);

            $('#edit_date').val(data[0]);
            $('#edit_barcode').val(data[1]);
            $('#edit_productname').val(data[2]);
            $('#edit_dealerprice').val(data[3]);
            $('#edit_supplierprice').val(data[4]);
            $('#edit_srp').val(data[5]);
            $('#edit_units').val(data[6]);
            $('#edit_totalvalueDealer').val(data[7]);
            $('#edit_totalvalueSupplier').val(data[8]);
            $('#edit_totalvalueSrp').val(data[9]);
            $('#edit_id').val(data[10]);
        }); 


    });

    $('#edit_units').on('input', function () {
        var units = parseFloat($(this).val());
        var dealerPrice = parseFloat($('#edit_dealerprice').val());
        var supplierPrice = parseFloat($('#edit_supplierprice').val());
        var srp = parseFloat($('#edit_srp').val());

        if (!isNaN(units) && !isNaN(dealerPrice)) {
            var totalValueDealer = units * dealerPrice;
            var totalValueSupplier = units * supplierPrice;
            var totalValueSrp = units * srp;
            $('#edit_totalvalueDealer').val(totalValueDealer.toFixed(2));
            $('#edit_totalvalueSupplier').val(totalValueSupplier.toFixed(2));
            $('#edit_totalvalueSrp').val(totalValueSrp.toFixed(2));

            console.log('Total Value Dealer:', totalValueDealer.toFixed(2));
            console.log('Total Value Supplier:', totalValueSupplier.toFixed(2));
            console.log('Total Value SRP:', totalValueSrp.toFixed(2));

        } else {
            $('#edit_totalvalueDealer').val('');
            $('#edit_totalvalueSupplier').val('');
            $('#edit_totalvalueSrp').val('');
        }
    });
</script>
</body>
</html>