<?php
include('../../connection.php');
session_start();

// Enable error reporting for debugging
error_reporting(E_ALL);
ini_set('display_errors', 1);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $branchSelector = $_POST['branchSelector'];
    $selected_branches = json_decode($_POST['selected_branches'], true);
    $fromDate = $_POST['fromDate'];
    $toDate = $_POST['toDate'];

    $products = [];

    if ($branchSelector == 'allBranches') {
        // Fetch all branches data
        $product_query = "
            SELECT lb.date, lb.description, lb.amount, br.branch_name FROM logistics_branch lb
            LEFT JOIN
                branch_record br ON br.code = lb.code
            WHERE 
                lb.date BETWEEN '$fromDate' AND '$toDate'";

        $product_result = mysqli_query($conn, $product_query);

        if (mysqli_num_rows($product_result) > 0) {
            while ($row = mysqli_fetch_assoc($product_result)) {
                $date = $row["date"];
                $description = $row["description"];
                $amount = $row["amount"];
                $branchname = $row["branch_name"];
                $products[] = [
                    'branchname' => $branchname,
                    'date' => $date,
                    'description' => $description,
                    'amount' => $amount
                ];
            }
        }
    } else {
        foreach ($selected_branches as $selected_branch_code) {
            // Fetch branch name
            $branch_name_query = mysqli_query($conn, "SELECT branch_name FROM branch_record WHERE code = '$selected_branch_code'");
            $branch_name_result = mysqli_fetch_assoc($branch_name_query);
            $branch_name = $branch_name_result['branch_name'];

            $product_query = "
                SELECT lb.date, lb.description, lb.amount, br.branch_name FROM logistics_branch lb
                LEFT JOIN
                    branch_record br ON br.code = lb.code
                INNER JOIN 
                    users_branch ub ON lb.code = ub.code
                WHERE 
                    ub.code = '$selected_branch_code'
                AND 
                    lb.date BETWEEN '$fromDate' AND '$toDate'";

            $product_result = mysqli_query($conn, $product_query);

            if (mysqli_num_rows($product_result) > 0) {
                while ($row = mysqli_fetch_assoc($product_result)) {
                    $date = $row["date"];
                    $description = $row["description"];
                    $amount = $row["amount"];
                    $branchname = $row["branch_name"];
                    $products[] = [
                        'branchname' => $branchname,
                        'date' => $date,
                        'description' => $description,
                        'amount' => $amount
                    ];
                }
            }
        }
    }

    header('Content-Type: application/json');
    echo json_encode($products);
} else {
    // Handle invalid request method
    header('HTTP/1.1 405 Method Not Allowed');
    echo json_encode(['error' => 'Invalid request method']);
}
?>
