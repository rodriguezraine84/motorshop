<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();

if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo "Record not Found!";
    exit;
}

// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype FROM users WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <script src="https://cdn.rawgit.com/serratus/quaggaJS/0.12.1/dist/quagga.min.js"></script>

    <!-- Bootstrap Selectpicker CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.14.0-beta2/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap Selectpicker JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.14.0-beta2/js/bootstrap-select.min.js"></script>

    
    
    <title>Herb and Angel | Report Generation Stock Outflow</title>
</head>
<style>
        .page-title {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .page-title h4 {
            margin: 0;
        }

        .btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }

        .custom-width {
            width: 100% !important;
        }

        .form-group .d-flex > div {
            flex: 1;
        }

        .horizontal-line {
            display: block;
            width: 100%;
            border-bottom: 1px solid grey; /* Adjust color here */
        }

        .container{
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
        }

    </style>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

<!--Main Content-->
<div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
                <a href="./super-admin-dashboard.php" class="logo">
                    <img src="../../assets/img/logo (1).png" alt="">
                </a>

                <a href="super-admin-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
                </a>

                <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

           <!--Underline Nav-->
           <ul class="nav user-menu">

            <!--Nav Items Dropdown-->
            <li class="nav-item dropdown has-arrow main-drop">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset" style="width: 120%; height: 10vh;">
                            <div class="profilesets" style="width: 120%; height: 20vh;">
                            <h6><?php echo $fullname?></h6>
                            <h5><?php echo $usertype?></h5>
                            </div>
                        </div>
                        <span class="horizontal-line"></span>
                        <a class="dropdown-item settingsBtn">
                            <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                            Settings
                        </a>
                        <a class="dropdown-item logout pb-0" href="../../index.php">
                            <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                            Logout
                        </a>
                    </div>
                </div>
            </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset">
                            <div class="profilesets">
                            <h5><?php echo $fullname?></h5>
                            <h6><?php echo $usertype?></h6>
                            </div>
                        </div>
                        <span class="horizontal-line"></span>
                        <a class="dropdown-item settingsBtn">
                            <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                            Settings
                        </a>
                        <a class="dropdown-item logout pb-0" href="../index.php">
                            <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                            Logout
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../super-admin-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-store-alt.svg" alt="img"><span> Dealer Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../dealer-management/records.php">Dealer Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-package.svg" alt="img"><span> Supplier Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../supplier-management/records.php">Supplier Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-git-branch.svg" alt="img"><span> Branch Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../branch-management/records.php">Branch Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-cog.svg" alt="img"><span> Services Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../services-management/services.php">Services</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../..//assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/product-list.php">Add Product</a></li>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                        <li><a href="../product-management/inflowproductlist.php">Inflow Product List</a></li>

                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>

                                            </ul>
                                </li>
                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bxs-truck.svg" alt="img"><span> Logistics Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../logistics/records.php">Logistics Records</a></li>
                                    </ul>
                                </li>

                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wallet.svg" alt="img"><span> Expenses Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../expenses/records.php">Expenses Records</a></li>
                                    </ul>
                                </li>
                                
                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> User Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../user-management/accounts.php">Accounts</a></li>
                                            </ul>
                                </li>   

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../report-generation/profit.php">Product Profit Report</a></li>
                                        <li><a href="../report-generation/mechanic.php">Mechanic Profit Report</a></li>
                                        <li><a href="../report-generation/logistics.php">Logistics Report</a></li>
                                        <li><a href="../report-generation/expenses.php">Expenses Report</a></li>
                                        <li><a href="../report-generation/productlist.php">Product List Report</a></li>
                                        <li><a href="../report-generation/report_stockinflow.php">Stock Inflow Report</a></li>
                                        <li><a href="../report-generation/report_stockoutflow.php">Stock Outflow Report</a></li>
                                    </ul>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

        <!--Under Main Content-->
        <div class="page-wrapper">
            <div class="content">
                <div class="container">
                    <div class="page-title" style="display: flex; justify-content: space-between;">
                        <h2 style="font-weight: bold;">STOCK OUTFLOW REPORT</h2>
                        <button type="button" class="btn btn-success" title="Export as Excel" onclick="exportToExcel()"> 
                            <i class="fas fa-file-excel" style="margin-right: 5px;"></i>Export as Excel
                        </button>
                    </div>
                        <form method="POST" action="" class="f">
                            <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="startfrom">Start From:</label>
                                            <input type="date" class="form-control" id="startfrom" name="startfrom" require>
                                        </div>
                                        
                                </div>
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="endto">End To:</label>
                                            <input type="date" class="form-control" id="endto" name="endto" require>
                                        </div>
                                </div>

                                    <!-- Search Bar -->
                                    <label style="font-weight: bold;">Select Branch/Dealer: </label>
                                        <div class="col-md-6 mt-2">
                                            <select name="branchSelector" id="branchSelector" placeholder="Nothing Selected" class="selectpicker custom-width">
                                                <option value="allBranches">All Branches</option>
                                                <option value="allDealer">All Dealer</option>
                                                <option value="branches">Branches</option>
                                                <option value="dealer">Dealer</option>
                                            </select>
                                        </div>


                                        <div id="selectedBranchesContainer" class="col-md-6 mt-2">
                                            <select name="selected_branches[]" id="selectedBranches" class="selectpicker custom-width" multiple aria-label="Default select example" data-live-search="true">
                                                <?php
                                                $users_branch_query = mysqli_query($conn, "SELECT * FROM users_branch");

                                                if (mysqli_num_rows($users_branch_query) > 0) {
                                                    while ($user_row = mysqli_fetch_assoc($users_branch_query)) {
                                                        // Fetch the branch information for each user
                                                        $user_branch_code = $user_row["branch_code"];
                                                        $branch_info_query = mysqli_query($conn, "SELECT * FROM branch_record WHERE code = '$user_branch_code'");

                                                        if (mysqli_num_rows($branch_info_query) > 0) {
                                                            $branch_info = mysqli_fetch_assoc($branch_info_query);
                                                            $branchName = $branch_info["branch_name"];
                                                            $branchCode = $branch_info["code"];

                                                            echo "<option value='$user_branch_code'>$branchName</option>";
                                                        } else {
                                                            echo "<option value=''>No branch found for user!</option>";
                                                        }
                                                    }
                                                } else {
                                                    echo "<option value=''>No users found!</option>";
                                                }
                                                ?>
                                            </select>  
                                        </div>

                                        <div id="selectedDealerContainer" class="container mt-2">
                                            <select name="selected_dealer[]" id="selectedDealers" class="selectpicker custom-width" multiple aria-label="Default select example" data-live-search="true">
                                                <?php
                                                $users_dealer_query = mysqli_query($conn, "SELECT * FROM users_dealer");

                                                if (mysqli_num_rows($users_dealer_query) > 0) {
                                                    while ($user_row = mysqli_fetch_assoc($users_dealer_query)) {
                                                        // Fetch the dealer information for each user
                                                        $user_dealer_code = $user_row["dealer_code"];
                                                        $dealer_info_query = mysqli_query($conn, "SELECT * FROM dealer_record WHERE code = '$user_dealer_code'");

                                                        if (mysqli_num_rows($dealer_info_query) > 0) {
                                                            $dealer_info = mysqli_fetch_assoc($dealer_info_query);
                                                            $dealerName = $dealer_info["dealer_name"];
                                                            $dealerCode = $dealer_info["code"];

                                                            echo "<option value='$user_dealer_code'>$dealerName</option>";
                                                        } else {
                                                            echo "<option value=''>No dealer found for user!</option>";
                                                        }
                                                    }
                                                } else {
                                                    echo "<option value=''>No users found!</option>";
                                                }
                                                ?>
                                            </select>  
                                        </div>
                            </div>
                        </form>
                </div>

                <br>
                <div class="container">
                    <div class="row" id="reportTable">
                            <h2 style="margin-top: 20px; font-weight: bold;">GENERATED REPORT</h2>
                            <table class="table table-striped mt-4" >
                                <div class="col-md-6 mt-3">
                                        <div class="form-group">
                                            <label style="font-weight: bold;">Branch/Dealer Name:</label>
                                            <div type="text" style="width: 200%" class="form-control" id="branchname" name="branchname" readonly></div>
                                        </div> 

                                        <div class="form-group">
                                            <label style="font-weight: bold;">Date Range:</label>
                                            <div class="d-flex justify-content-between">
                                                <div class="me-2">
                                                    <label for="startfrom">Start From:</label>
                                                    <div id="displayStartfrom" class="form-control"></div>
                                                    </div>
                                                <div>
                                                    <label for="endto">End To:</label>
                                                    <div id="displayEndto" class="form-control"></div>
                                                </div>
                                            </div>
                                        </div>
                                </div>

                                    <div class="col-md-6 mt-3">
                                        
                                    </div>
                                    <tr>
                                        <div class= "d-flex justify-content-between">
                                        <th scope="col" id="" name="">Profit from Products:</th>
                                        <th scope="col" id="profitProducts" name="profitProducts"></th>
                                        </div>
                                    </tr>
                                    <tr>
                                        <th scope="col" id="" name="">Profit from Mechanics:</th>
                                        <th scope="col" id="profitMechanic" name="profitMechanic"></th>

                                    </tr>

                                    <tr>
                                        <th scope="col" id="" name="">Total Expenses:</th>
                                        <th scope="col" id="totalExpenses" name="totalExpenses"></th>
                                    </tr>

                                    <tr>
                                        <th scope="col" id="" name="">Net Profit:</th>
                                        <th scope="col" id="netProfit" name="netProfit"></th>
                                    </tr>                                
                            </table>
                        </div>
                </div>

                <br>
                    <div class="container">
                        <div class="row" id="reportTable">
                            <table class="table table-striped mt-3" id="branchTable">
                                <thead>
                                    <tr>
                                        <th scope="col">Branch/Dealer Name</th>
                                        <th scope="col">Profit From Products</th>
                                        <th scope="col">Profit From Mechanic</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
    <div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="../settings.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
            $('#settings_usertype').val(usertype);
        }); 
    });
</script>

<script>
    document.getElementById('generate_report').addEventListener('click', function() {
        // Get the start date value
        var startfrom = document.getElementById('startfrom').value;
        // Get the end date value
        var endto = document.getElementById('endto').value;
        // Get the selected branch codes
        var selected_branches = document.querySelector('[name="selected_branches[]"]').value;

        // Log the values to the console for debugging
        console.log('Start From:', startfrom);
        console.log('End To:', endto);
        console.log('Selected Branch Codes:', selected_branches);
    });
</script>


<script>
   function exportToExcel() {
    // Create a form to submit to export_excel.php
    var form = document.createElement("form");
    form.method = "POST";
    form.action = "export/export_profit.php";

    // Add any data you need to send to the server here
    // For example, if you need to pass start and end dates or branch/dealer info
    var startFrom = document.getElementById("startfrom").value;
    var endTo = document.getElementById("endto").value;
    var branchSelector = document.getElementById("branchSelector").value;

    // Create hidden inputs for each parameter
    var startFromInput = document.createElement("input");
    startFromInput.type = "hidden";
    startFromInput.name = "startfrom";
    startFromInput.value = startFrom;
    form.appendChild(startFromInput);

    var endToInput = document.createElement("input");
    endToInput.type = "hidden";
    endToInput.name = "endto";
    endToInput.value = endTo;
    form.appendChild(endToInput);

    var branchSelectorInput = document.createElement("input");
    branchSelectorInput.type = "hidden";
    branchSelectorInput.name = "branchSelector";
    branchSelectorInput.value = branchSelector;
    form.appendChild(branchSelectorInput);

    // Append the form to the body and submit it
    document.body.appendChild(form);
    form.submit();
}
</script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
    const branchSelector = document.getElementById('branchSelector');
    const selectedBranchesContainer = document.getElementById('selectedBranchesContainer');
    const selectedDealerContainer = document.getElementById('selectedDealerContainer');
    const startFrom = document.getElementById('startfrom');
    const endTo = document.getElementById('endto');
    const displayStartfrom = document.getElementById('displayStartfrom');
    const displayEndto = document.getElementById('displayEndto');
    const selectedBranches = document.getElementById('selectedBranches');
    const selectedDealers = document.getElementById('selectedDealers');
    const branchname = document.getElementById('branchname');
    const branchTableBody = document.querySelector('#branchTable tbody');
    const totalProfitItem = document.getElementById('profitProducts');
    const totalProfitLabor = document.getElementById('profitMechanic');
    const totalExpenses = document.getElementById('totalExpenses');
    const netProfit = document.getElementById('netProfit');

    function updateSelectedBranchesContainer() {
        if (branchSelector.value === 'branches') {
            selectedBranchesContainer.style.display = 'block';
        } else {
            selectedBranchesContainer.style.display = 'none';
        }

        if(branchSelector.value === 'dealer'){
            selectedDealerContainer.style.display = 'block';
        }else{
            selectedDealerContainer.style.display = 'none';
        }

    }

    branchSelector.addEventListener('change', function() {
        console.log(`Selected option: ${branchSelector.value}`);
        updateSelectedBranchesContainer();

        if (branchSelector.value === 'allBranches') {
            fetchAllData();
        }
    });

    // Event listener for date inputs
    startFrom.addEventListener('change', function() {
        displayStartfrom.textContent = startFrom.value;
        fetchAndDisplayProfits();
    });

    endTo.addEventListener('change', function() {
        displayEndto.textContent = endTo.value;
        fetchAndDisplayProfits();
    });

    // Function to fetch and display the profit data
    async function fetchAndDisplayProfits() {
        const selectedBranchValues = Array.from(selectedBranches.selectedOptions).map(option => option.value);
        const formData = new FormData();
        formData.append('selected_branches', JSON.stringify(selectedBranchValues));
        formData.append('fromDate', startFrom.value);
        formData.append('toDate', endTo.value);

        try {
            const response = await fetch('calculate_profits.php', {
                method: 'POST',
                body: formData
            });
            const data = await response.json();
            console.log('Profit data:', data);

            branchTableBody.innerHTML = '';
            let totalProfit = 0;
            let totalLabor = 0;
            let totalExpensesSum = 0;
            let netProfitSum = 0;

            data.forEach(item => {
                console.log(`Processing branch: ${item.branch_name}, Profit Item: ${item.profit_item}, Profit Labor: ${item.profit_labor}, Total Expenses: ${item.total_expenses}, Net Profit: ${item.net_profit}`);

                const row = document.createElement('tr');
                row.innerHTML = `
                    <td>${item.branch_name}</td>
                    <td>${item.profit_item}</td>
                    <td>${item.profit_labor}</td>
                `;
                branchTableBody.appendChild(row);
                totalProfit += parseFloat(item.profit_item);
                totalLabor += parseFloat(item.profit_labor);
                totalExpensesSum += parseFloat(item.total_expenses);
                netProfitSum += parseFloat(item.net_profit);
            });

            console.log('Total Profit from Products:', totalProfit);
            totalProfitItem.textContent = totalProfit.toFixed(2);
            totalProfitLabor.textContent = totalLabor.toFixed(2);
            totalExpenses.textContent = totalExpensesSum.toFixed(2);
            netProfit.textContent = netProfitSum.toFixed(2);
        } catch (error) {
            console.error('Error fetching profit data:', error);
        }
    }
    
    async function fetchAllData() {
        const formData = new FormData();
        formData.append('fromDate', startFrom.value);
        formData.append('toDate', endTo.value);

        try {
            const response = await fetch('fetch_data.php', {
                method: 'POST',
                body: formData
            });
            
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }

            const contentType = response.headers.get('content-type');
            if (contentType && contentType.indexOf('application/json') !== -1) {
                const data = await response.json();
                console.log('Profit Data:', data);

                branchTableBody.innerHTML = '';
                let totalProfit = 0;
                let totalLabor = 0;
                let totalExpensesSum = 0;
                let netProfitSum = 0;

                data.forEach(item => {
                    console.log(`Processing branch: ${item.branch_name}, Profit Item: ${item.profit_item}, Profit Labor: ${item.profit_labor}, Total Expenses: ${item.total_expenses}, Net Profit: ${item.net_profit}`);

                    const row = document.createElement('tr');
                    row.innerHTML = `
                        <td>${item.branch_name}</td>
                        <td>${item.profit_item}</td>
                        <td>${item.profit_labor}</td>
                    `;
                    branchTableBody.appendChild(row);
                    totalProfit += parseFloat(item.profit_item);
                    totalLabor += parseFloat(item.profit_labor);
                    totalExpensesSum += parseFloat(item.total_expenses);
                    netProfitSum += parseFloat(item.net_profit);
                });

                console.log('Total Profit from Products:', totalProfit);
                totalProfitItem.textContent = totalProfit.toFixed(2);
                totalProfitLabor.textContent = totalLabor.toFixed(2);
                totalExpenses.textContent = totalExpensesSum.toFixed(2);
                netProfit.textContent = netProfitSum.toFixed(2);
            } else {
                const text = await response.text();
                throw new Error(`Unexpected response type: ${contentType}. Response text: ${text}`);
            }

        } catch (error) {
            console.error('Error fetching profit data:', error);
        }
    }


    // Event listener for the selected branches
    selectedBranches.addEventListener('change', function() {
        const selectedOptions = Array.from(selectedBranches.selectedOptions).map(option => option.text);
        console.log(`Selected branches: ${selectedOptions.join(', ')}`);
        branchname.textContent = selectedOptions.join(', ');

        // Fetch and display the profit data
        fetchAndDisplayProfits();
    });

    selectedDealers.addEventListener('change', function() {
        const selectedOptions = Array.from(selectedDealers.selectedOptions).map(option => option.text);
        console.log(`Selected dealers: ${selectedOptions.join(', ')}`);
        branchname.textContent = selectedOptions.join(', ');
    });
    // Initial check to hide the selected branches container if not 'branches' on page load
    updateSelectedBranchesContainer();
});
</script>

</body>
</html>