<?php
include('../../connection.php');
session_start();
// Enable error reporting for debugging
error_reporting(E_ALL);
ini_set('display_errors', 1);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $fromDate = $_POST['fromDate'];
    $toDate = $_POST['toDate'];

    $profits = [];

    $branch_name_query = mysqli_query($conn, "SELECT dealer_name, code FROM dealer_record");
    
    while ($branch = mysqli_fetch_assoc($branch_name_query)) {
        $branch_name = $branch['dealer_name'];
        $branch_code = $branch['code'];

        $total_profit_item = 0;
        $total_profit_query = mysqli_query($conn, "SELECT
            SUM(CAST(osb.total_profit AS DECIMAL(10, 2))) AS total_profit
            FROM
                outflow_selling_dealer osb
            JOIN users_dealer ub ON osb.code = ub.code
            WHERE
                DATE(osb.date) BETWEEN '$fromDate' AND '$toDate' AND ub.code = '$branch_code'");
        if (mysqli_num_rows($total_profit_query) > 0) {
            $total_profit_result = mysqli_fetch_assoc($total_profit_query);
            $total_profit_item += $total_profit_result["total_profit"];
        }

        $total_expenses = 0;
        $total_expenses_query = mysqli_query($conn, "SELECT 
            COALESCE(SUM(CAST(eb.amount AS DECIMAL(10,2))), 0) AS total_expenses
            FROM
                expenses_branch eb
            JOIN users_branch ub ON eb.code = ub.code
            WHERE
                DATE(eb.date) BETWEEN '$fromDate' AND '$toDate' AND ub.code = '$branch_code'");
        if (mysqli_num_rows($total_expenses_query) > 0) {
            $total_expenses_result = mysqli_fetch_assoc($total_expenses_query);
            $total_expenses = $total_expenses_result["total_expenses"];
        }

        $net_profit = $total_profit_item - $total_expenses;

        error_log("Branch: $branch_name, Profit Item: $total_profit_item, Total Expense: $total_expenses, Net Profit: $net_profit");

        $profits[] = [
            'branch_name' => $branch_name,
            'profit_item' => $total_profit_item,
            'total_expenses' => $total_expenses,
            'net_profit' => $net_profit
        ];
    }

    header('Content-Type: application/json');
    echo json_encode($profits);
} else {
    // Handle invalid request method
    header('HTTP/1.1 405 Method Not Allowed');
    echo json_encode(['error' => 'Invalid request method']);
}
?>
