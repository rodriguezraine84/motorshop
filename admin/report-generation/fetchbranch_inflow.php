<?php
include('../../connection.php');
session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $branchSelector = $_POST['branchSelector'];
    $selected_branches = json_decode($_POST['selected_branches'], true);
    $fromDate = $_POST['fromDate'];
    $toDate = $_POST['toDate'];
    $selected_dealers = json_decode($_POST['selected_dealers'], true);
    $products = [];
    
    if($branchSelector === 'branches'){
        foreach ($selected_branches as $selected_branch_code) {
            $branch_name_query = mysqli_query($conn, "SELECT branch_name FROM branch_record WHERE code = '$selected_branch_code'");
            $branch_name_result = mysqli_fetch_assoc($branch_name_query);
            $branch_name = $branch_name_result['branch_name'];

            $product_query = "
                SELECT 
                    ib.date,
                    ib.barcode,
                    p.productname,
                    ib.dealer_price,
                    ib.supplier_price,
                    ib.srp,
                    ib.units_received,
                    ib.totalvalue_dealer,
                    ib.totalvalue,
                    ib.totalvalue_srp,
                    br.branch_address AS location
                FROM 
                    inflow_branch ib
                JOIN 
                    branch_record br ON ib.code = br.code
                JOIN 
                    products p ON ib.barcode = p.barcode
                WHERE 
                    ib.date BETWEEN '$fromDate' AND '$toDate'
                    AND ib.code = '$selected_branch_code'
                ORDER BY 
                    date DESC";
            $product_result = mysqli_query($conn, $product_query);
            while ($product_row = mysqli_fetch_assoc($product_result)) {
                $products[] = [
                    'date' => date('m-d-Y', strtotime($product_row['date'])),
                    'barcode' => $product_row['barcode'],
                    'product_name' => $product_row['productname'],
                    'dealerprice' => $product_row['dealer_price'],
                    'supplierprice' => $product_row['supplier_price'],
                    'srp' => $product_row['srp'],
                    'units' => $product_row['units_received'],
                    'totalvalue_dealer' => $product_row['totalvalue_dealer'],
                    'totalvalue' => $product_row['totalvalue'],
                    'totalvalue_srp' => $product_row['totalvalue_srp'],
                    'location' => $product_row['location']
                ];
            }
        }
    } else {
        foreach ($selected_dealers as $selected_dealer_code) {
            $branch_name_query = mysqli_query($conn, "SELECT dealer_name FROM dealer_record WHERE code = '$selected_dealer_code'");
            $branch_name_result = mysqli_fetch_assoc($branch_name_query);
            $branch_name = $branch_name_result['dealer_name'];

            $product_query = "
                SELECT 
                    ib.date,
                    ib.barcode,
                    p.productname,
                    ib.dealer_price,
                    ib.supplier_price,
                    ib.srp,
                    ib.units_received,
                    ib.totalvalue_dealer,
                    ib.totalvalue,
                    ib.totalvalue_srp,
                    br.branch_address AS location
                FROM 
                    inflow_admin ib
                JOIN 
                    dealer_record br ON ib.code = br.code
                JOIN 
                    products p ON ib.barcode = p.barcode
                WHERE 
                    ib.date BETWEEN '$fromDate' AND '$toDate'
                    AND ib.code = '$selected_dealer_code'
                ORDER BY 
                    date DESC";
            $product_result = mysqli_query($conn, $product_query);
            while ($product_row = mysqli_fetch_assoc($product_result)) {
                $products[] = [
                    'date' => date('m-d-Y', strtotime($product_row['date'])),
                    'barcode' => $product_row['barcode'],
                    'product_name' => $product_row['productname'],
                    'dealerprice' => $product_row['dealer_price'],
                    'supplierprice' => $product_row['supplier_price'],
                    'srp' => $product_row['srp'],
                    'units' => $product_row['units_received'],
                    'totalvalue_dealer' => $product_row['totalvalue_dealer'],
                    'totalvalue' => $product_row['totalvalue'],
                    'totalvalue_srp' => $product_row['totalvalue_srp'],
                    'location' => $product_row['location']
                ];
            }
        }
    }
    header('Content-Type: application/json');
    echo json_encode($products);
} else {
    header('HTTP/1.1 405 Method Not Allowed');
    echo json_encode(['error' => 'Invalid request method']);
}
?>
