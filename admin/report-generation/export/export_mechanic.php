<?php
require '../../../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// Fetch data from POST parameters
$startFrom = $_POST['startfrom'];
$endTo = $_POST['endto'];
$branchSelector = $_POST['branchSelector'];

// Fetch the report data based on the parameters
// Example data, replace with your actual data fetching logic
$branchName = "Branch A"; // Replace this with actual branch/dealer name based on branchSelector
$profitProducts = 10000; // Replace this with actual data
$profitMechanic = 5000; // Replace this with actual data
$totalExpenses = 3000; // Replace this with actual data
$netProfit = $profitProducts + $profitMechanic - $totalExpenses;

// Create a new Spreadsheet object
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

// Set the report title
$sheet->setCellValue('A1', 'PRODUCT PROFIT REPORT');

// Set the headers
$sheet->setCellValue('A2', 'Branch/Dealer Name');
$sheet->setCellValue('B2', 'Date Range');
$sheet->setCellValue('C2', 'Profit From Products');
$sheet->setCellValue('D2', 'Profit From Mechanic');
$sheet->setCellValue('E2', 'Total Expenses');
$sheet->setCellValue('F2', 'Net Profit');

// Set the data
$sheet->setCellValue('A3', $branchName);
$sheet->setCellValue('B3', "$startFrom to $endTo");
$sheet->setCellValue('C3', $profitProducts);
$sheet->setCellValue('D3', $profitMechanic);
$sheet->setCellValue('E3', $totalExpenses);
$sheet->setCellValue('F3', $netProfit);

// Set the headers for download
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="product_profit_report.xlsx"');
header('Cache-Control: max-age=0');

// Create the Writer object
$writer = new Xlsx($spreadsheet);

// Write the file to the output
$writer->save('php://output');
exit;
?>
