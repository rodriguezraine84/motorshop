<?php
include('../../connection.php');
session_start();
// Enable error reporting for debugging
error_reporting(E_ALL);
ini_set('display_errors', 1);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $selected_branches = json_decode($_POST['selected_branches'], true);
    $selected_dealers = json_decode($_POST['selected_dealers'], true);

    $fromDate = $_POST['fromDate'];
    $toDate = $_POST['toDate'];

    $products = [];

    if ($branchSelector == 'branches') {
        // Fetch all branches data
        foreach ($selected_branches as $selected_branch_code) {
            // Fetch branch name
            $stmt = $conn->prepare("SELECT branch_name FROM branch_record WHERE code = ?");
            $stmt->bind_param('s', $selected_branch_code);
            $stmt->execute();
            $result = $stmt->get_result();
            $branch_name_result = $result->fetch_assoc();
            $branch_name = $branch_name_result['branch_name'];
    
            // Fetch product data
            $product_query = "
                SELECT lb.date, lb.description, lb.amount, br.branch_name FROM logistics_branch lb
                LEFT JOIN branch_record br ON br.code = lb.code
                INNER JOIN users_branch ub ON lb.code = ub.code
                WHERE ub.code = ? AND lb.date BETWEEN ? AND ?";
            
            $stmt = $conn->prepare($product_query);
            $stmt->bind_param('sss', $selected_branch_code, $fromDate, $toDate);
            $stmt->execute();
            $product_result = $stmt->get_result();
    
            if($product_result->num_rows > 0){
                while ($row = $product_result->fetch_assoc()) {
                    $date = $row["date"];
                    $description = $row["description"];
                    $amount = $row["amount"];
                    $branchname = $row["branch_name"];
                    $products[] = [
                        'branchname' => $branchname,
                        'description' => $description,
                        'amount' => $amount
                    ];
                }
            }
        }
    }
    else if($branchSelector == 'dealers'){
        foreach ($selected_dealers as $selected_branch_code) {
            // Fetch branch name
            $stmt = $conn->prepare("SELECT branch_name FROM branch_record WHERE code = ?");
            $stmt->bind_param('s', $selected_branch_code);
            $stmt->execute();
            $result = $stmt->get_result();
            $branch_name_result = $result->fetch_assoc();
            $branch_name = $branch_name_result['branch_name'];
    
            // Fetch product data
            $product_query = "
                SELECT lb.date, lb.description, lb.amount, br.branch_name FROM logistics_branch lb
                LEFT JOIN branch_record br ON br.code = lb.code
                INNER JOIN users_branch ub ON lb.code = ub.code
                WHERE ub.code = ? AND lb.date BETWEEN ? AND ?";
            
            $stmt = $conn->prepare($product_query);
            $stmt->bind_param('sss', $selected_branch_code, $fromDate, $toDate);
            $stmt->execute();
            $product_result = $stmt->get_result();
    
            if($product_result->num_rows > 0){
                while ($row = $product_result->fetch_assoc()) {
                    $date = $row["date"];
                    $description = $row["description"];
                    $amount = $row["amount"];
                    $branchname = $row["branch_name"];
                    $products[] = [
                        'branchname' => $branchname,
                        'description' => $description,
                        'amount' => $amount
                    ];
                }
            }
        }
    }
    

    header('Content-Type: application/json');
    echo json_encode($products);
} else {
    // Handle invalid request method
    header('HTTP/1.1 405 Method Not Allowed');
    echo json_encode(['error' => 'Invalid request method']);
}
?>
