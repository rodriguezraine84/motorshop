<?php
include('../../connection.php');
session_start();

// Enable error reporting for debugging
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $branchSelector = $_POST['branchSelector'];
    $selected_branches = json_decode($_POST['selected_branches'], true);
    $selected_dealer = json_decode($_POST['selected_dealer'], true);

    $fromDate = $_POST['fromDate'];
    $toDate = $_POST['toDate'];

    $products = [];

    if ($branchSelector == 'allBranches') {
        // Fetch all branches data
        $product_query = "
            SELECT lb.date, lb.description, lb.amount, br.branch_name FROM expenses_branch lb
            LEFT JOIN
                branch_record br ON br.code = lb.code
            WHERE 
                lb.date BETWEEN '$fromDate' AND '$toDate';";

        $product_result = mysqli_query($conn, $product_query);

        if (mysqli_num_rows($product_result) > 0) {
            while ($row = mysqli_fetch_assoc($product_result)) {
                $date = $row["date"];
                $description = $row["description"];
                $amount = $row["amount"];
                $branchname = $row["branch_name"];
                $products[] = [
                    'branchname' => $branchname,
                    'description' => $description,
                    'amount' => $amount
                ];
            }
        }
    } else if($branchSelector == 'branches'){
        foreach ($selected_branches as $selected_branch_code) {
            // Fetch branch name
            $branch_name_query = mysqli_query($conn, "SELECT branch_name FROM branch_record WHERE code = '$selected_branch_code'");
            $branch_name_result = mysqli_fetch_assoc($branch_name_query);
            $branch_name = $branch_name_result['branch_name'];

            $product_query = "
                SELECT lb.date, lb.description, lb.amount, br.dealer_name AS branch_name FROM expenses_branch lb
                LEFT JOIN
                    branch_record br ON br.code = lb.code
                WHERE 
                    lb.date BETWEEN '$fromDate' AND '$toDate'
                AND lb.code = '$selected_branch_code'";

            $product_result = mysqli_query($conn, $product_query);

            if (mysqli_num_rows($product_result) > 0) {
                while ($row = mysqli_fetch_assoc($product_result)) {
                    $date = $row["date"];
                    $description = $row["description"];
                    $amount = $row["amount"];
                    $branchname = $row["branch_name"];
                    $products[] = [
                        'branchname' => $branchname,
                        'date' => $date,
                        'description' => $description,
                        'amount' => $amount
                    ];
                }
            }
        }
    }else if($branchSelector == 'allDealer'){
        // Fetch all branches data
        $product_query = "
            SELECT lb.date, lb.description, lb.amount, br.dealer_name AS branch_name FROM expenses_dealer lb
            LEFT JOIN
                dealer_record br ON br.code = lb.code
            WHERE 
                lb.date BETWEEN '$fromDate' AND '$toDate';";

        $product_result = mysqli_query($conn, $product_query);

        if (mysqli_num_rows($product_result) > 0) {
            while ($row = mysqli_fetch_assoc($product_result)) {
                $date = $row["date"];
                $description = $row["description"];
                $amount = $row["amount"];
                $branchname = $row["branch_name"];
                $products[] = [
                    'branchname' => $branchname,
                    'description' => $description,
                    'amount' => $amount
                ];
            }
        }
    }else{
        foreach ($selected_dealer as $selected_branch_code) {
            // Fetch branch name
            $branch_name_query = mysqli_query($conn, "SELECT dealer_name FROM dealer_record WHERE code = '$selected_branch_code'");
            $branch_name_result = mysqli_fetch_assoc($branch_name_query);
            $branch_name = $branch_name_result['dealer_name'];

            $product_query = "
                SELECT lb.date, lb.description, lb.amount, br.dealer_name AS branch_name FROM expenses_dealer lb
                LEFT JOIN
                    dealer_record br ON br.code = lb.code
                WHERE 
                    lb.date BETWEEN '$fromDate' AND '$toDate'
                AND lb.code = '$selected_branch_code'";

            $product_result = mysqli_query($conn, $product_query);

            if (mysqli_num_rows($product_result) > 0) {
                while ($row = mysqli_fetch_assoc($product_result)) {
                    $date = $row["date"];
                    $description = $row["description"];
                    $amount = $row["amount"];
                    $branchname = $row["branch_name"];
                    $products[] = [
                        'branchname' => $branchname,
                        'date' => $date,
                        'description' => $description,
                        'amount' => $amount
                    ];
                }
            }
        }
    }

    header('Content-Type: application/json');
    echo json_encode($products);
} else {
    // Handle invalid request method
    header('HTTP/1.1 405 Method Not Allowed');
    echo json_encode(['error' => 'Invalid request method']);
}
?>
