<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();

if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo "Record not Found!";  
    exit;
}

// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype FROM users WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
}

if(isset($_POST['addexpenses'])){
    $description = $_POST['description'];
    $amount = $_POST['amount'];
    $location = $_POST['location_admin'];
    $expenses_query = "INSERT INTO logistics_admin (`date`, `description`, `amount`, `code`, `location`) VALUES (NOW(), ?, ?, ?, ?)";
    $expenses_result = mysqli_prepare($conn, $expenses_query);
    mysqli_stmt_bind_param($expenses_result, "ssss", $description, $amount, $code, $location);
    mysqli_stmt_execute($expenses_result);

    if($expenses_result){
        $_SESSION['message'] = "Successfully Added Logistics for Branch";
        $_SESSION['message_type'] = "success";
        header("Location: ../logistics/records.php");
        exit();
    } else {
        $_SESSION['message'] = "Failed to Add Logistics for Branch";
        $_SESSION['message_type'] = "danger";      
        header("Location: ../logistics/records.php");
        exit();
    }
}

if(isset($_POST['addexpensesbranch'])){
    $description = $_POST['description'];
    $amount = $_POST['amount'];
    $code_branch = $_POST['branch'];
    $expenses_query = "INSERT INTO logistics_branch (`date`, `description`, `amount`, `code`) VALUES (NOW(), ?, ?, ?)";
    $expenses_result = mysqli_prepare($conn, $expenses_query);
    mysqli_stmt_bind_param($expenses_result, "sss", $description, $amount, $code_branch);
    mysqli_stmt_execute($expenses_result);

    if($expenses_result){
        $_SESSION['message'] = "Successfully Added Logistics for Branch";
        $_SESSION['message_type'] = "success";
        header("Location: ../logistics/records.php");
        exit();
    } else {
        $_SESSION['message'] = "Failed to Add Logistics for Branch";
        $_SESSION['message_type'] = "danger";      
        header("Location: ../logistics/records.php");
        exit();
    }
}

require '../../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if(isset($_POST['save_excel_data_admin']))
{
    $fileName = $_FILES['import_file1']['name'];
    $file_ext = pathinfo($fileName, PATHINFO_EXTENSION);

    $allowed_ext = ['xls','csv','xlsx'];

    if(in_array($file_ext, $allowed_ext))
    {
        $inputFileNamePath = $_FILES['import_file1']['tmp_name'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileNamePath);
        $data = $spreadsheet->getActiveSheet()->toArray();

        $count = 0;
        foreach($data as $row)
        {
            if($count > 0)
            {
                $id = $row['0'];
                $date = $row['1'];
                $description = $row['2']; 
                $amount = str_replace(',', '', $row['3']);
                
                $unixTimestamp = strtotime(str_replace('-', '/', $date));

                $formatted_date = date('Y-m-d', $unixTimestamp);
                $location = $_POST['location_admin'];

                $productQuery = "INSERT INTO logistics_admin (id, date, description, amount, code, location) VALUES (?, ?, ?, ?, ?, ?)";
                $result = mysqli_prepare($conn, $productQuery);
                mysqli_stmt_bind_param($result, "isssss", $id, $formatted_date, $description, $amount, $code, $location);
                mysqli_stmt_execute($result);

                if ($result) {
                    $_SESSION['message'] = "Successfully Inserted Logistics for Admin!";
                    $_SESSION['message_type'] = "success";
                    header('Location: records.php');
                    exit(0);
                    
                } else {
                    $_SESSION['message'] = "Error Inserting Logistics for Admin!";
                    $_SESSION['message_type'] = "danger";
                    header('Location: records.php');
                    exit(0);
                }
                
            }
            else
            {
                $count = 1;
            }
        }

        if(isset($msg))
        {
            $_SESSION['message'] = "Successfully Imported";
            $_SESSION['message_type'] = "success";
            header('Location: records.php');
            exit(0);
        }
    }
    else
    {
        $_SESSION['message'] = "Invalid File";
        $_SESSION['message_type'] = "danger";
        header('Location: records.php');
        exit(0);
    }
}

if(isset($_POST['save_excel_data']))
{
    $fileName = $_FILES['import_file1']['name'];
    $file_ext = pathinfo($fileName, PATHINFO_EXTENSION);

    $allowed_ext = ['xls','csv','xlsx'];

    if(in_array($file_ext, $allowed_ext))
    {
        $inputFileNamePath = $_FILES['import_file1']['tmp_name'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileNamePath);
        $data = $spreadsheet->getActiveSheet()->toArray();

        $count = 0;
        foreach($data as $row)
        {
            if($count > 0)
            {
                $id = $row['0'];
                $date = $row['1'];
                $description = $row['2']; 
                $amount = str_replace(',', '', $row['3']);
                
                $unixTimestamp = strtotime(str_replace('-', '/', $date));

                $formatted_date = date('Y-m-d', $unixTimestamp);
                $code_branch = $_POST['branch'];
                $productQuery = "INSERT INTO logistics_branch (id, date, description, amount, code) VALUES (?, ?, ?, ?, ?)";
                $result = mysqli_prepare($conn, $productQuery);
                mysqli_stmt_bind_param($result, "issss", $id, $formatted_date, $description, $amount, $code_branch);
                mysqli_stmt_execute($result);

                if ($result) {
                    $_SESSION['message'] = "Successfully Inserted Logistics for Branch!";
                    $_SESSION['message_type'] = "success";
                    header('Location: records.php');
                    exit(0);
                    
                } else {
                    $_SESSION['message'] = "Error Inserting Logistics for Branch!";
                    $_SESSION['message_type'] = "danger";
                    header('Location: records.php');
                    exit(0);
                }
                
            }
            else
            {
                $count = 1;
            }
        }

        if(isset($msg))
        {
            $_SESSION['message'] = "Successfully Imported";
            $_SESSION['message_type'] = "success";
            header('Location: records.php');
            exit(0);
        }
    }
    else
    {
        $_SESSION['message'] = "Invalid File";
        $_SESSION['message_type'] = "danger";
        header('Location: records.php');
        exit(0);
    }
}

$logistics_select = "SELECT * FROM logistics_admin ORDER BY date DESC";
$logistics_select_query = mysqli_query($conn, $logistics_select);

$branch_logistics_select = " SELECT logistics.*, branch_record.branch_address FROM logistics_branch AS logistics JOIN branch_record ON logistics.code = branch_record.code ORDER BY date DESC";
$branch_logistics_select_query = mysqli_query($conn, $branch_logistics_select);
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <script src="https://cdn.rawgit.com/serratus/quaggaJS/0.12.1/dist/quagga.min.js"></script>
    <title>Herb and Angel | Logistics</title>
</head>
<style>
        .page-title {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .page-title h4 {
            margin: 0;
        }

        .btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }
        
        .horizontal-line {
            display: block;
            width: 100%;
            border-bottom: 1px solid grey; /* Adjust color here */
        }

    </style>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
                <a href="./super-admin-dashboard.php" class="logo">
                    <img src="../../assets/img/logo (1).png" alt="">
                </a>

                <a href="super-admin-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
                </a>

                <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset" style="width: 120%; height: 10vh;">
                                <div class="profilesets" style="width: 120%; height: 20vh;">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <span class="horizontal-line"></span>
                            <a class="dropdown-item settingsBtn">
                                <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                                Settings
                            </a>
                            <a class="dropdown-item logout pb-0" href="../../index.php">
                                <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                                Logout
                            </a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <div class="profilesets">
                                <h5><?php echo $fullname?></h5>
                                <h6><?php echo $usertype?></h6>
                                </div>
                            </div>
                            <span class="horizontal-line"></span>
                            <a class="dropdown-item settingsBtn">
                                <img src="../../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                                Settings
                            </a>
                            <a class="dropdown-item logout pb-0" href="../index.php">
                                <img src="../../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                                Logout
                            </a>
                        </div>
                    </div>
            </div>
        </div>

        <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../super-admin-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-store-alt.svg" alt="img"><span> Dealer Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../dealer-management/records.php">Dealer Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-package.svg" alt="img"><span> Supplier Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../supplier-management/records.php">Supplier Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-git-branch.svg" alt="img"><span> Branch Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../branch-management/records.php">Branch Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-cog.svg" alt="img"><span> Services Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../services-management/services.php">Services</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../..//assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/product-list.php">Add Product</a></li>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                            </ul>
                                </li>
                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bxs-truck.svg" alt="img"><span> Logistics Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../logistics/records.php">Logistics Records</a></li>
                                    </ul>
                                </li>

                                
                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wallet.svg" alt="img"><span> Expenses Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../expenses/records.php">Expenses Records</a></li>
                                    </ul>
                                </li>
                                
                                
                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> User Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../user-management/accounts.php">Accounts</a></li>
                                            </ul>
                                </li>   

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../report-generation/profit.php">Product Profit Report</a></li>
                                        <li><a href="../report-generation/mechanic.php">Mechanic Profit Report</a></li>
                                        <li><a href="../report-generation/logistics.php">Logistics Report</a></li>
                                        <li><a href="../report-generation/expenses.php">Expenses Report</a></li>
                                        <li><a href="../report-generation/productlist.php">Product List Report</a></li>
                                        <li><a href="../report-generation/report_stockinflow.php">Stock Inflow Report</a></li>
                                        <li><a href="../report-generation/report_stockoutflow.php">Stock Outflow Report</a></li>
                                    </ul>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

        <!--Under Main Content-->
        <div class="page-wrapper">
            <div class="content">
                <div class="col-lg-6 col-12" style="width: 100%;">
                    <?php
                        if (isset($_SESSION['message'])) {
                            $alertClass = ($_SESSION['message_type'] == 'success') ? 'alert-success' : 'alert-danger';
                            echo "<div class='alert $alertClass' role='alert'>" . $_SESSION['message'] . "</div>";
                            unset($_SESSION['message']); // Clear the session variable after displaying the message
                        }
                    ?>
                </div>
                <div class="page-title">
                    <h2 style="font-weight: bold;">LOGISTICS</h2>
                    <div class="btn-container">
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addlogistics" onclick="openPopup()">Add Logistics Admin</button>
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addlogisticsbranch" onclick="openPopupBranch()">Add Logistics Branch</button>
                        <button type="button" class="btn btn-dark editBtn" title="Print" onclick="generatePDF()"> <i class="fas fa-print" style="margin-right: 5px;"></i>Print Report</button>
                    </div>
                </div>
                
                <div id="reportTable">
                    <h4>Admin</h4>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Date</th>
                                <th scope="col">Description</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Location</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                // Loop through the product results and display them in the table
                                while ($product_row = mysqli_fetch_assoc($logistics_select_query)) {
                                    echo '<tr>';
                                    echo '<td>' . $product_row['id'] . '</td>';
                                    echo '<td>' . date('m-d-Y', strtotime($product_row['date'])) . '</td>';
                                    echo '<td>' . $product_row['description'] . '</td>';
                                    echo '<td>' . number_format($product_row['amount']) . '</td>';
                                    echo '<td>' . $product_row['location'] . '</td>';
                                    echo '</tr>';
                                }
                            ?>
                        </tbody>
                    </table>

                    <h4>Branch</h4>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Date</th>
                                <th scope="col">Description</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Location</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                // Loop through the product results and display them in the table
                                while ($product_row = mysqli_fetch_assoc($branch_logistics_select_query)) {
                                    echo '<tr>';
                                    echo '<td>' . $product_row['id'] . '</td>';
                                    echo '<td>' . date('m-d-Y', strtotime($product_row['date'])) . '</td>';
                                    echo '<td>' . $product_row['description'] . '</td>';
                                    echo '<td>' . number_format($product_row['amount']) . '</td>';
                                    echo '<td>' . $product_row['branch_address'] . '</td>';
                                    echo '</tr>';
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>

    <div class="popup" id="addlogistics-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addlogistics" tabindex="-1" role="dialog" aria-labelledby="addlogisticsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addlogisticsModalLabel">Add Admin Expenses Record</h5>
                            <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#importExcelAdmin" onclick="openExcelAdmin()">Import Excel</button>
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <input type="text" class="form-control" name="description" id="description" value="" placeholder="Description" autofocus required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Amount</label>
                                                    <input type="text" class="form-control" name="amount" id="amount" value="" placeholder="Enter Amount" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                    <label>Location</label>
                                                    <input type="text" class="form-control" name="location_admin" id="location_admin" value="Admin" placeholder="Admin" readonly>
                                                </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="addexpenses" class="btn btn-submit me-2">Add Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="addlogisticsbranch-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addlogisticsbranch" tabindex="-1" role="dialog" aria-labelledby="addlogisticsbranchModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addlogisticsbranchModalLabel">Add Branch Expenses Record</h5>
                            <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#importExcelBranch" onclick="openExcelBranch()">Import Excel</button>
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <input type="text" class="form-control" name="description" id="description" value="" placeholder="Description" autofocus required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Amount</label>
                                                    <input type="text" class="form-control" name="amount" id="amount" value="" placeholder="Enter Amount" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                    <label>Location</label>
                                                    <select id="branch" name="branch" class="form-control" required>
                                                        <option value="" selected>Select a Branch</option>
                                                        <?php
                                                        $users_branch_query = mysqli_query($conn, "SELECT * FROM users_branch");

                                                        if(mysqli_num_rows($users_branch_query) > 0) {
                                                            while($user_row = mysqli_fetch_assoc($users_branch_query)) {
                                                                // Fetch the branch information for each user
                                                                $user_branch_code = $user_row["branch_code"];
                                                                $branch_info_query = mysqli_query($conn, "SELECT * FROM branch_record WHERE code = '$user_branch_code'");
                                                                
                                                                if(mysqli_num_rows($branch_info_query) > 0) {
                                                                    $branch_info = mysqli_fetch_assoc($branch_info_query);
                                                                    $branchName = $branch_info["branch_address"];
                                                                    $branchCode = $branch_info["code"];

                                                                    echo "<option value='$user_branch_code'>$branchName</option>";
                                                                } else {
                                                                    echo "<option value=''>No branch found for user!</option>";
                                                                }
                                                            }
                                                        } else {
                                                            echo "<option value=''>No users found!</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="addexpensesbranch" class="btn btn-submit me-2">Add Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="importexcel-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="importExcelBranch" tabindex="-1" role="dialog" aria-labelledby="importExcelLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="importExcelModalLabel">Import Excel</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Branch</label>
                                                    <select id="branch" name="branch" class="form-control" required>
                                                        <option value="" selected>Select a Branch</option>
                                                        <?php
                                                        $users_branch_query = mysqli_query($conn, "SELECT * FROM users_branch");

                                                        if(mysqli_num_rows($users_branch_query) > 0) {
                                                            while($user_row = mysqli_fetch_assoc($users_branch_query)) {
                                                                // Fetch the branch information for each user
                                                                $user_branch_code = $user_row["branch_code"];
                                                                $branch_info_query = mysqli_query($conn, "SELECT * FROM branch_record WHERE code = '$user_branch_code'");
                                                                
                                                                if(mysqli_num_rows($branch_info_query) > 0) {
                                                                    $branch_info = mysqli_fetch_assoc($branch_info_query);
                                                                    $branchName = $branch_info["branch_address"];
                                                                    $branchCode = $branch_info["code"];

                                                                    echo "<option value='$user_branch_code'>$branchName</option>";
                                                                } else {
                                                                    echo "<option value=''>No branch found for user!</option>";
                                                                }
                                                            }
                                                        } else {
                                                            echo "<option value=''>No users found!</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Upload Excel File</label>
                                                        <div class="image-upload">
                                                            <input type="file" name="import_file1" class="custom-file-input" id="inputGroupFile01" required>
                                                            <div class="image-uploads">
                                                                <img src="../../assets/img/icons/upload.svg" alt="img">
                                                                    <h4>Drag and drop a file to upload</h4>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <button type="submit" name="save_excel_data" class="btn btn-submit me-2">Add Expenses for Branch</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="importexcelAdmin-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="importExcelAdmin" tabindex="-1" role="dialog" aria-labelledby="importExcelAdminLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="importExcelAdminModalLabel">Import Excel</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Location</label>
                                                    <input type="text" class="form-control" name="location_admin" id="location_admin" value="Admin" placeholder="Admin" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Upload Excel File</label>
                                                        <div class="image-upload">
                                                            <input type="file" name="import_file1" class="custom-file-input" id="inputGroupFile01" required>
                                                            <div class="image-uploads">
                                                                <img src="../../assets/img/icons/upload.svg" alt="img">
                                                                    <h4>Drag and drop a file to upload</h4>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <button type="submit" name="save_excel_data_admin" class="btn btn-submit me-2">Add Expenses for Admin</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="../settings.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
            $('#settings_usertype').val(usertype);
        }); 
    });
</script>
<script>
    function openPopup() {
        document.getElementById("addlogistics-popup").style.display = "block";
    }

    function openExcelAdmin(){
        document.getElementById("importexcelAdmin-popup").style.display = "block";
    }

    function openPopupBranch(){
        document.getElementById("addlogisticsbranch-popup").style.display = "block";
    }

    function openExcelBranch(){
        document.getElementById("importexcel-popup").style.display = "block";
    }
</script>

<script>
    function generatePDF() {
        // Function to print the table content
        var printContents = document.getElementById("reportTable").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    window.onafterprint = function(event) {
        setTimeout(function() {
            // Redirect back to product-management/barcode.php
            window.location.href = 'records.php';
        }, 100);
    };
</script>

</body>
</html>