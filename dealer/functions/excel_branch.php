<?php
session_start();
include('../../connection.php');

require '../../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if (isset($_POST['save_excel_data'])) {
    $fileName = $_FILES['import_file']['name'];
    $file_ext = pathinfo($fileName, PATHINFO_EXTENSION);

    $allowed_ext = ['xls', 'csv', 'xlsx'];

    if (in_array($file_ext, $allowed_ext)) {
        $inputFileNamePath = $_FILES['import_file']['tmp_name'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileNamePath);
        $data = $spreadsheet->getActiveSheet()->toArray();

        $count = 0;
        foreach ($data as $row) {
            if ($count > 0) {
                $date = $row['1'];
                $productname = $row['2'];
                $dealerprice = $row['3'];
                $supplierprice = $row['4'];
                $srp = $row['5'];
                $unitsreceived = $row['6'];

                // Validate that the essential fields are not null
                if ($date && $productname) {
                    $unixTimestamp = strtotime(str_replace('-', '/', $date));
                    $formatted_date = date('Y-m-d', $unixTimestamp);

                    $sql = "SELECT dealer_price, supplier_price, srp FROM products WHERE productname = '$productname'";
                    $result1 = mysqli_query($conn, $sql);
                    $row1 = mysqli_fetch_assoc($result1);
                    $old_dealer_price = $row1['dealer_price'];
                    $old_supplier_price = $row1['supplier_price'];
                    $old_srp = $row1['srp'];
                    
                    $totalvalueDealer = $old_dealer_price * $unitsreceived;
                    $totalvalueSupplier = $old_supplier_price * $unitsreceived;
                    $totalvalueSrp = $old_srp * $unitsreceived;
                    // Prepare the first query
                    $productQuery = "INSERT INTO excel_inflow (date, productname, dealerprice, supplierprice, srp, units, dealer, supplier, ssrp) VALUES (?, ?, '$old_dealer_price', '$old_supplier_price', '$old_srp', ?, '$totalvalueDealer', '$totalvalueSupplier', '$totalvalueSrp')";
                    $stmt = mysqli_prepare($conn, $productQuery);

                    // Bind parameters
                    mysqli_stmt_bind_param($stmt, "ssi", $formatted_date, $productname, $unitsreceived);

                    // Execute the statement
                    $result = mysqli_stmt_execute($stmt);

                    if ($result) {
                        // Data successfully inserted into excel_import table, now insert into outflow_selling_branch
                        $code_branch = $_POST['excel'];

                        // Prepare the second query
                        $inflowQuery = "INSERT INTO inflow_branch (date, barcode, supplier_price, dealer_price, srp, units_received, totalvalue_dealer, totalvalue_srp, totalvalue, dealer_code) 
                                        SELECT e.date, p.barcode, e.supplierprice, e.dealerprice, e.srp, e.units, e.dealer, e.ssrp, e.supplier, ?
                                        FROM excel_inflow e
                                        JOIN products p ON e.productname = p.productname";
                        $stmt2 = mysqli_prepare($conn, $inflowQuery);

                        // Bind parameter
                        mysqli_stmt_bind_param($stmt2, "s", $code_branch);

                        // Execute the statement
                        $inflowResult = mysqli_stmt_execute($stmt2);

                        if ($inflowResult) {
                            // Prepare and execute the delete query
                            $deleteQuery = "DELETE FROM excel_inflow";
                            $stmt3 = mysqli_prepare($conn, $deleteQuery);
                            mysqli_stmt_execute($stmt3);
                        }
                    } else {
                        $_SESSION['message'] = "Error Inserting Data!";
                        $_SESSION['message_type'] = "danger";
                        header('Location: ../inventory-management/stockinflow.php');
                        exit(0);
                    }
                }
            } else {
                $count = 1;
            }
        }

        $_SESSION['message'] = "Successfully Imported";
        $_SESSION['message_type'] = "success";
        header('Location: ../inventory-management/stockinflow.php');
        exit(0);
    } else {
        $_SESSION['message'] = "Invalid File";
        $_SESSION['message_type'] = "danger";
        header('Location: ../inventory-management/stockinflow.php');
        exit(0);
    }
}

?>