<?php
session_start();
include('../../connection.php');

// Fetch product name based on the scanned barcode
$productname = $_POST['searchproductname'];

// Prepare and execute the statement to select barcode by productname
$sql = "SELECT barcode FROM products WHERE productname = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("s", $productname);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows > 0) {
    // Return the barcode
    $row = $result->fetch_assoc();
    echo $row['barcode'];
} else {
    echo "Barcode not found!";
}

// Close the statement and the connection
$stmt->close();
$conn->close();
?>
