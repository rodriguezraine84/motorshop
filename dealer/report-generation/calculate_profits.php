<?php
include('../../connection.php');
session_start();
// Enable error reporting for debugging
error_reporting(E_ALL);
ini_set('display_errors', 1);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $selected_branches = json_decode($_POST['selected_branches'], true);
    $fromDate = $_POST['fromDate'];
    $toDate = $_POST['toDate'];

    $profits = [];

    foreach ($selected_branches as $selected_branch_code) {
        // Fetch branch name
        $branch_name_query = mysqli_query($conn, "SELECT branch_name FROM branch_record WHERE code = '$selected_branch_code'");
        $branch_name_result = mysqli_fetch_assoc($branch_name_query);
        $branch_name = $branch_name_result['branch_name'];

        $total_profit_item = 0;
        $total_profit_query = mysqli_query($conn, "SELECT
            SUM(CAST(osb.total_profit AS DECIMAL(10, 2))) AS total_profit
            FROM
                outflow_selling_branch osb
            JOIN users_branch ub ON osb.code = ub.code
            WHERE 
                ub.branch_code = '$selected_branch_code'
                AND DATE(osb.date) BETWEEN '$fromDate' AND '$toDate'");
        if (mysqli_num_rows($total_profit_query) > 0) {
            $total_profit_result = mysqli_fetch_assoc($total_profit_query);
            $total_profit_item += $total_profit_result["total_profit"];
        }

        $total_profit_labor = 0;
        $total_profit_query = mysqli_query($conn, "SELECT
            SUM(CAST(ms.price AS DECIMAL(10, 2))) AS total_profit
            FROM
                mechanic_services ms
            JOIN users_branch ub ON ms.code = ub.code
            WHERE 
                ub.branch_code = '$selected_branch_code'
                AND DATE(ms.date) BETWEEN '$fromDate' AND '$toDate'");
        if (mysqli_num_rows($total_profit_query) > 0) {
            $total_profit_result = mysqli_fetch_assoc($total_profit_query);
            $total_profit = $total_profit_result["total_profit"];
            if ($total_profit >= 500) {
                $total_profit_labor += $total_profit * 0.1;
            }
        }

        $total_expenses = 0;
        $total_expenses_query = mysqli_query($conn, "SELECT 
            COALESCE(SUM(CAST(eb.amount AS DECIMAL(10,2))), 0) AS total_expenses
            FROM
                expenses_branch eb
            JOIN users_branch ub ON eb.code = ub.code
            WHERE
                ub.branch_code = '$selected_branch_code'
                AND DATE(eb.date) BETWEEN '$fromDate' AND '$toDate'");
        if (mysqli_num_rows($total_expenses_query) > 0) {
            $total_expenses_result = mysqli_fetch_assoc($total_expenses_query);
            $total_expenses = $total_expenses_result["total_expenses"];
        }


        $net_profit = $total_profit_item + $total_profit_labor - $total_expenses;

        error_log("Branch: $branch_name, Profit Item: $total_profit_item, Profit Labor: $total_profit_labor, Total Expense: $total_expenses, Net Profit: $net_profit");

        $profits[] = [
            'branch_name' => $branch_name,
            'profit_item' => $total_profit_item,
            'profit_labor' => $total_profit_labor,
            'total_expenses' => $total_expenses,
            'net_profit' => $net_profit
        ];
    }

    header('Content-Type: application/json');
    echo json_encode($profits);
} else {
    // Handle invalid request method
    header('HTTP/1.1 405 Method Not Allowed');
    echo json_encode(['error' => 'Invalid request method']);
}
?>
