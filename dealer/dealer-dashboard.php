<!DOCTYPE html>
<?php 
include('../connection.php');
session_start();

if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo "Record not Found!";
    exit;
}

// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype FROM users_dealer WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/animate.css">
    <link rel="stylesheet" href="../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->
    <title>Herb and Angel | Dashboard</title>
</head>
<body>
    <style>
        body{
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
        }
        .horizontal-line {
                display: block;
                width: 100%;
                border-bottom: 1px solid grey; /* Adjust color here */
            }

    </style>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
               <a href="dealer-dashboard.php" class="logo">
                    <img src="../assets/img/logo (1).png" alt="">
               </a>

               <a href="dealer-dashboard.php" class="logo-small">
                    <img src="../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset" style="width: 120%; height: 10vh;">
                                <div class="profilesets" style="width: 120%; height: 20vh;">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <span class="horizontal-line"></span>
                            <a class="dropdown-item settingsBtn">
                                <img src="../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                                Settings
                            </a>
                            <a class="dropdown-item logout pb-0" href="../index.php">
                                <img src="../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                                Logout
                            </a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <div class="profilesets">
                                <h5><?php echo $fullname?></h5>
                                <h6><?php echo $usertype?></h6>
                                </div>
                            </div>
                            <span class="horizontal-line"></span>
                            <a class="dropdown-item settingsBtn">
                                <img src="../assets/img/icons/settings.svg" class="icon me-2" alt="Settings icon">
                                Settings
                            </a>
                            <a class="dropdown-item logout pb-0" href="../index.php">
                                <img src="../assets/img/icons/log-out.svg" class="icon me-2" alt="Logout icon">
                                Logout
                            </a>
                        </div>
                    </div>
            </div>
        </div>

            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="dealer-dashboard.php"><img src="../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                        <ul>
                                            <li><a href="../dealer/inventory-management/stockinflow.php">Stock Inflow</a></li>
                                            <li><a href="../dealer/inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                        </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../dealer/report-generation/">Report 1</a></li>
                                        <li><a href="../dealer/report-generation/">Report 2</a></li>
                                        <li><a href="../dealer/report-generation/">Report 3</a></li>
                                        <li><a href="../dealer/report-generation/">Report 4</a></li>
                                        <li><a href="../dealer/report-generation/">Report 5</a></li>
                                    </ul>
                                </li>   
                        </ul>
                    </div>
                </div>
            </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="col-lg-6 col-12" style="width: 100%;">

                    <div class="row">
                        <div class="col-xl-4 col-sm-8 col-14"> 
                            <div class="card shadow">
                                <div class="card-body rounded">
                                    <div class="media">
                                        <div class="align-self-center mr-3">
                                            <img src="../assets/img/icons/bxs-wallet.svg" alt="Wallet Icon">
                                        </div>
                                        <div class="media-body text-end">
                                            <h2 class="mb-0">200</h2>
                                            <h5 class="mb-0">Daily Sales</h5>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-sm-8 col-14"> 
                            <div class="card shadow">
                                <div class="card-body rounded">
                                    <div class="media">
                                        <div class="align-self-center mr-3">
                                        <img src="../assets/img/icons/bxs-purchase-tag-alt.svg" alt="Purchase">
                                        </div>
                                        <div class="media-body text-end">
                                            <h2 class="mb-0">200</h2>
                                            <h5 class="mb-0">Expenses</h5>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-sm-8 col-14"> 
                            <div class="card shadow">
                                <div class="card-body rounded">
                                    <div class="media">
                                        <div class="align-self-center mr-3">
                                            <img src="../assets/img/icons/bx-signal-5.svg" alt="Signal">
                                        </div>
                                        <div class="media-body text-end">
                                            <h2 class="mb-0">200</h2>
                                            <h5 class="mb-0">Monthly Sales</h5>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                        <?php
                            if (isset($_SESSION['message'])) {
                                $alertClass = ($_SESSION['message_type'] == 'success') ? 'alert-success' : 'alert-danger';
                                echo "<div class='alert $alertClass' role='alert'>" . $_SESSION['message'] . "</div>";
                                unset($_SESSION['message']); // Clear the session variable after displaying the message
                            }
                        ?>
                    </div>
                </div>
            </div>
    </div>

    <div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Account</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="settings.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <?php $usertype = "Dealer"?>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" value="<?php echo $usertype; ?>"disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
<script src="../assets/js/jquery-3.6.0.min.js"></script>

<script src="../assets/js/feather.min.js"></script>

<script src="../assets/js/jquery.slimscroll.min.js"></script>

<script src="../assets/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/dataTables.bootstrap4.min.js"></script>

<script src="../assets/js/bootstrap.bundle.min.js"></script>

<script src="../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../assets/plugins/apexchart/chart-data.js"></script>

<script src="../assets/js/script.js"></script>
<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
        }); 
    });
</script>

</body>
</html>