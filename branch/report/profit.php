<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();
if(isset($_SESSION["code"])) {
    $code = $_SESSION["code"];
} else {
    echo "Record not Found!";
    exit;
}

// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype, branch_code FROM users_branch WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
    $branch_code = $row["branch_code"];
}

$defaultDate = date('Y-m-d');
$fromDate = isset($_POST['startfrom']) ? date('Y-m-d', strtotime($_POST['startfrom'])) : $defaultDate;
$toDate = isset($_POST['endto']) ? date('Y-m-d', strtotime($_POST['endto'])) : $defaultDate;

if (isset($_POST['startfrom']) && isset($_POST['endto'])) {
    // Prepare the SQL statement
    if ($fromDate == $defaultDate && $toDate == $defaultDate) {
        // Default date, retrieve data for the current date
        $sql = "SELECT * FROM outflow_selling_branch WHERE DATE(date) = ?";
        $stmt_sql = mysqli_prepare($conn, $sql);
        mysqli_stmt_bind_param($stmt_sql, "s", $defaultDate);
    } else {
        // Date range is selected, retrieve data for the selected date range
        $sql = "SELECT * FROM outflow_selling_branch WHERE DATE(date) BETWEEN ? AND ?";
        $stmt_sql = mysqli_prepare($conn, $sql);
        mysqli_stmt_bind_param($stmt_sql, "ss", $fromDate, $toDate);

    }

// Execute the query
mysqli_stmt_execute($stmt_sql);
$query = mysqli_stmt_get_result($stmt_sql);
}

if (isset($_POST['confirm'])) {
    // Prepare the SQL statement
    $sql = "SELECT * FROM outflow_selling_branch WHERE DATE(date) BETWEEN ? AND ? AND branch_code = ?";
    $stmt_sql = mysqli_prepare($conn, $sql);
    // Check if the statement was prepared successfully
    if ($stmt_sql) {
        // Bind parameters
        mysqli_stmt_bind_param($stmt_sql, "sss", $fromDate, $toDate, $branch_code);
        // Execute the query
        mysqli_stmt_execute($stmt_sql);
        // Get the results
        $query = mysqli_stmt_get_result($stmt_sql);
    } else {
        // Handle the error if the statement was not prepared successfully
        die("Error: " . mysqli_error($conn));
    }
}


?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->

    <title>Herb and Angel | Branch Profit Report</title>

</head>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
               <a href="../branch-dashboard.php" class="logo">
                <img src="../../assets/img/logo (1).png" alt="">
                    
               </a>

               <a href="../branch-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

            <!--Nav Items Dropdown-->
            <li class="nav-item dropdown has-arrow main-drop">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset">
                            <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                            <span class="status online"></span></span>
                            <div class="profilesets">
                            <h6><?php echo $fullname?></h6>
                            <h5><?php echo $usertype?></h5>
                            </div>
                        </div>
                        <a class="dropdown-item settingsBtn"><img src="../../assets/img/icons/settings.svg" class="me-2" alt="img">Settings</a>
                        <a class="dropdown-item logout pb-0" href="../../index.php"><img src="../../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                    </div>
                </div>
            </li>
            </ul>
            </div>

            <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
            <div id="sidebar-menu" class="sidebar-menu">
                <ul>
                        <li class="active">
                            <a href="../branch-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                        <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                        <li><a href="../inventory-management/returned.php">Returned</a></li>
                                        <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                        <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                    </ul>
                        </li>                             

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wrench.svg" alt="img"><span> Mechanic</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../mechanic/mechanic.php">Mechanic</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bxs-truck.svg" alt="img"><span> Logistics</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../logistics/records.php">Logistics Records</a></li>
                            </ul>
                        </li>

                        
                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wallet.svg" alt="img"><span> Expenses</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../expenses/records.php">Expenses Records</a></li>
                            </ul>
                        </li>


                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);"><span style="font-weight:bold;"> Profit</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../report/product.php">Profit from Products</a></li>
                                                <li><a href="../report/labor.php">Profit from Labor</a></li>
                                                <li><a href="../report/profit.php">All Profit</a></li>
                                            </ul>
                                    </li>
                                    <hr>
                                    <li><a href="../report/inventory.php" style="font-weight:bold;">Inventory Report</a></li>
                        
                                </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> Mechanic Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../mechanic-management/records.php">Accounts</a></li>
                                    </ul>
                        </li>  
                </ul>
            </div>
            </div>
            </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="page-title" style="display: flex; justify-content: space-between;">
                        <h2>All Profits</h2>
                        <button type="button" class="btn btn-dark editBtn" title="Print" onclick="generatePDF()"> <i class="fas fa-print" style="margin-right: 5px;"></i>Print Report</button>
                    </div>
                    <div class="container mt-5">
                        <h3>Select Date</h3>
                            <form method="POST" action="">
                                <div style="display: flex; gap: 20px;">
                                    <div>
                                        <label for="startfrom" class="form-label">From:</label>
                                        <input type="date" id="startfrom" name="startfrom" class="form-control" style="width: 100%">
                                    </div>
                                    <div>
                                        <label for="endto" class="form-label">To:</label>
                                        <input type="date" id="endto" name="endto" class="form-control" style="width: 100%">
                                    </div>
                                    <button type="submit" name="confirm" id="confirm" class="btn btn-primary mt-4">Confirm</button>
                                </div>
                            </form>
                    </div>
                        <table class="table table-striped mt-5" style="overflow-x: auto;">
                            <tbody>
                                <tr>
                                    <td>Profit From Labor</td>
                                    <?php
                                        // Retrieve total profit from mechanic services table
                                        $profit = 0;
                                        $total_profit_query = mysqli_query($conn, "SELECT
                                        SUM(CAST(ms.price AS DECIMAL(10, 2))) AS total_profit_mechanic
                                        FROM
                                        mechanic_services ms
                                        WHERE 
                                        ms.code = $code AND DATE(ms.date) BETWEEN '$fromDate' AND '$toDate'");
                                        
                                        if (mysqli_num_rows($total_profit_query) > 0) {
                                            $total_profit_result = mysqli_fetch_assoc($total_profit_query);
                                            $total_profit_labor = $total_profit_result["total_profit_mechanic"];
                                            
                                            echo '<td>' . $total_profit_labor . '</td>';
                                        }
                                    ?>
                                </tr>

                                <tr>
                                    <td>Profit From Item
                                        
                                    </td>
                                    <?php
                                        // Retrieve total profit from outflow selling branch table
                                        $item_cost = 0;
                                        $total_profit_query_item = mysqli_query($conn, "SELECT
                                        SUM(CAST(osb.total_profit AS DECIMAL(10, 2))) AS total_profit
                                        FROM
                                        outflow_selling_branch osb
                                        WHERE 
                                        osb.code = $code AND DATE(osb.date) BETWEEN '$fromDate' AND '$toDate'");
                                        if (mysqli_num_rows($total_profit_query_item) > 0) {
                                            $total_profit_result = mysqli_fetch_assoc($total_profit_query_item);
                                            $total_profit_item = $total_profit_result["total_profit"];
                                            echo '<td>' . $total_profit_item . '</td>';
                                        }
                                    ?>
                                </tr>
                                <tr>
                                    <td>Returned
                                        
                                    </td>
                                        <?php
                                            $returned = 0;
                                            // Retrieve total profit from outflow selling branch table
                                            $total_profit_query_returned = mysqli_query($conn,"SELECT
                                            SUM(CAST(rp.price AS DECIMAL(10, 2))) AS total_returned_profit
                                            FROM
                                            returned_product rp
                                            WHERE 
                                            rp.code = $code AND DATE(rp.date_bought) BETWEEN '$fromDate' AND '$toDate'");
                                            if (mysqli_num_rows($total_profit_query_returned) > 0) {
                                                $total_profit_result = mysqli_fetch_assoc($total_profit_query_returned);
                                                $total_profit_returned = $total_profit_result["total_returned_profit"];

                                                echo '<td>' . $total_profit_returned . '</td>';
                                            }
                                        ?>
                                </tr>
                                <tr>
                                    <td>Gross Profit</td>
                                    <?php
                                        if (isset($profit) && isset($item_cost) && isset($returned)) {
                                            $gross_profit = $total_profit_labor + $total_profit_item - $total_profit_returned;
                                            echo "<td>" . number_format($gross_profit, 2) . "</td>";
                                        } else {
                                            echo "<td>No data available</td>";
                                        }
                                    ?>
                                </tr>
                            </tbody>
                        </table>
                </div>   
            </div>
</div>
<div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="../settings.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
            $('#settings_usertype').val(usertype);
        }); 
    });
</script>
<script>
    document.getElementById('confirm').addEventListener('click', function() {
        // Get the start date value
        var startfrom = document.getElementById('startfrom').value;
        // Get the end date value
        var endto = document.getElementById('endto').value;
        // Get the selected branch codes

        // Log the values to the console for debugging
        console.log('Start From:', startfrom);
        console.log('End To:', endto);
    });
</script>
<script>
    function generatePDF() {
        // Function to print the table content
        var printContents = document.getElementById("reportTable").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    window.onafterprint = function(event) {
        setTimeout(function() {
            // Redirect back to product-management/barcode.php
            window.location.href = 'profit.php';
        }, 100);
    };
</script>
</body>
</html>