<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();
if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo"Record not Found!";
    exit;
}

// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype FROM users_branch WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
}

date_default_timezone_set('Asia/Manila');

if(isset($_POST['add_mechanic'])) {
    $mechanic_id = $_POST['branch'];
    $services = isset($_POST['selected_services']) ? $_POST['selected_services'] : array();
    $service_prices = isset($_POST['service_prices']) ? $_POST['service_prices'] : array();

    if (count($services) > 0 && count($service_prices) > 0) {
        // Prepare and bind parameters for insertion
        $insert_query = mysqli_prepare($conn, "INSERT INTO mechanic_services (`date`, `mechanic_id`, `services_id`, `price`, `code`) VALUES (NOW(), ?, ?, ?, ?)");

        for ($i = 0; $i < count($services); $i++) {
            $service_id = $services[$i];
            $price = $service_prices[$i];

            // Bind parameters and execute query
            mysqli_stmt_bind_param($insert_query, "ssss", $mechanic_id, $service_id, $price, $code);
            $insert_query_result = mysqli_stmt_execute($insert_query);

            if($insert_query_result){
                $_SESSION['message'] = "Mechanic Service Added Successfully!";
                $_SESSION['message_type'] = "success";
                header("Location: ../mechanic/mechanic.php");
                exit();
            } else {
                $_SESSION['message'] = "Failed to Add Mechanic Service";
                $_SESSION['message_type'] = "danger";
                header("Location: ../mechanic/mechanic.php");
                exit();
            }
        }
    } else {
        $_SESSION['message'] = "No Service Selected!";
        $_SESSION['message_type'] = "danger";
        header("Location: ../mechanic/mechanic.php");
        exit(); 
    }
}

$mechanic = "SELECT 
                ms.date,
                ms.mechanic_id,
                mr.mechanic_name,
                SUM(ms.price) AS total_price
            FROM 
                mechanic_services ms
            INNER JOIN 
                mechanic_record mr ON ms.mechanic_id = mr.mechanic_id
            JOIN
                users_branch u ON ms.code = u.code
            WHERE
                ms.code = ?
            GROUP BY 
                ms.date, ms.mechanic_id, mr.mechanic_name;";

$mechanic_query = mysqli_prepare($conn, $mechanic);
mysqli_stmt_bind_param($mechanic_query, "s", $code);
mysqli_stmt_execute($mechanic_query);
$mechanic_result = mysqli_stmt_get_result($mechanic_query);
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->

     <!-- Bootstrap Selectpicker CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.14.0-beta2/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap Selectpicker JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.14.0-beta2/js/bootstrap-select.min.js"></script>

    <title>Herb and Angel | Mechanic</title>
</head>
<style>
.btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }
</style>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
               <a href="../branch-dashboard.php" class="logo">
                <img src="../../assets/img/logo (1).png" alt="">
               </a>

               <a href="../branch-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

            <!--Nav Items Dropdown-->
            <li class="nav-item dropdown has-arrow main-drop">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset">
                            <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                            <span class="status online"></span></span>
                            <div class="profilesets">
                            <h6><?php echo $fullname?></h6>
                            <h5><?php echo $usertype?></h5>
                            </div>
                        </div>
                        <a class="dropdown-item settingsBtn"><img src="../../assets/img/icons/settings.svg" class="me-2" alt="img">Settings</a>
                        <a class="dropdown-item logout pb-0" href="../../index.php"><img src="../../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                    </div>
                </div>
            </li>
            </ul>
            </div>

            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                <ul>
                        <li class="active">
                            <a href="../branch-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                        <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                        <li><a href="../inventory-management/returned.php">Returned</a></li>
                                        <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                        <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                    </ul>
                        </li>                             

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wrench.svg" alt="img"><span> Mechanic</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../mechanic/mechanic.php">Mechanic</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bxs-truck.svg" alt="img"><span> Logistics</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../logistics/records.php">Logistics Records</a></li>
                            </ul>
                        </li>

                            
                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wallet.svg" alt="img"><span> Expenses</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../expenses/records.php">Expenses Records</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);"><span style="font-weight:bold;"> Profit</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../report/product.php">Profit from Products</a></li>
                                                <li><a href="../report/labor.php">Profit from Labor</a></li>
                                                <li><a href="../report/profit.php">All Profit</a></li>
                                            </ul>
                                    </li>
                                    <hr>
                                    <li><a href="../report/inventory.php" style="font-weight:bold;">Inventory Report</a></li>
                        
                                </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> Mechanic Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../mechanic-management/records.php">Accounts</a></li>
                                    </ul>
                        </li>  
                </ul>
            </div>
            </div>
            </div>
            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="col-lg-6 col-12" style="width: 100%;">
                        <?php
                            if (isset($_SESSION['message'])) {
                                $alertClass = ($_SESSION['message_type'] == 'success') ? 'alert-success' : 'alert-danger';
                                echo "<div class='alert $alertClass' role='alert'>" . $_SESSION['message'] . "</div>";
                                unset($_SESSION['message']); // Clear the session variable after displaying the message
                            }
                        ?>
                    </div>
                    <div class="page-title" style="display: flex; justify-content: space-between;">
                        <h2 style="font-weight: bold;">MECHANIC SERVICES</h2>
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addstocks" onclick="openPopup()">Mechanic Services</button>
                    </div>
                    <table class="table table-striped">
                        <thead class="vertical-headers">
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Mechanic Name</th>
                                <th scope="col"><center>Services</center></th>
                                <th scope="col">Labor Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        <?php
                            while($mechanic_row = mysqli_fetch_assoc($mechanic_result)){
                                $labor_cost = $mechanic_row['total_price'];
                                echo "
                                    <tr>
                                        <td>" . date('m-d-y', strtotime($mechanic_row['date'])) . "</td>
                                        <td>" . $mechanic_row['mechanic_name'] . "</td>
                                    
                                        <td style='margin: 0 auto; width: fit-content;'> <!-- colspan='4' ensures this row spans all columns -->
                                            <center><table>
                                                <thead>
                                                    <tr>
                                                        <th>Service</th>
                                                        <th>Price</th>
                                                    </tr>
                                                </thead>
                                                <tbody>";

                                                    // Prepare statement
                                                    $mechanic_service_query = mysqli_prepare($conn, "SELECT s.service, m.price
                                                        FROM mechanic_services m
                                                        JOIN services s ON m.services_id = s.service_id
                                                        JOIN mechanic_record mr ON m.mechanic_id = mr.mechanic_id
                                                        JOIN users_branch u ON m.code = u.code
                                                        WHERE m.date = ? AND mr.mechanic_name = ? AND m.code= ?");
                                                    
                                                    // Bind parameters
                                                    mysqli_stmt_bind_param($mechanic_service_query, "sss", $mechanic_row['date'], $mechanic_row['mechanic_name'], $code);

                                                    // Execute query
                                                    mysqli_stmt_execute($mechanic_service_query);

                                                    // Bind result variables
                                                    mysqli_stmt_bind_result($mechanic_service_query, $service, $price);

                                                    // Fetch values
                                                    while(mysqli_stmt_fetch($mechanic_service_query)){
                                                        echo "
                                                        <tr>
                                                            <td>$service</td>
                                                            <td>$price</td>
                                                        </tr>";
                                                    }
                                                    mysqli_stmt_close($mechanic_service_query);

                                                echo "
                                                </tbody>
                                            </table> </center> 
                                        </td>
                                        <td>$labor_cost</td>
                                    </tr>
                                ";
                            }
                        ?>


                        </tbody>
                    </table>
                </div>
            </div>
    </div>
                        
    <div class="popup" id="addservice-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addstocks" tabindex="-1" role="dialog" aria-labelledby="addstocksModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg"role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addstocksModalLabel">Mechanic Service</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                        <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Mechanic Name</label>
                                                    <select id="branch" name="branch" class="form-control" required>
                                                        
                                                    <option value="" selected>Select a Mechanic</option>
                                                    <?php
                                                        // Prepare statement for fetching mechanic records
                                                        $mechanic_name_query = mysqli_prepare($conn, "SELECT * FROM mechanic_record");
                                                        mysqli_stmt_execute($mechanic_name_query);
                                                        mysqli_stmt_store_result($mechanic_name_query);

                                                        if(mysqli_stmt_num_rows($mechanic_name_query) > 0) {
                                                            mysqli_stmt_bind_result($mechanic_name_query, $mechanic_id, $mechanic_name);
                                                            
                                                            while(mysqli_stmt_fetch($mechanic_name_query)) {
                                                                // Fetch the branch information for each user
                                                                $mechanic_info_query = mysqli_prepare($conn, "SELECT * FROM mechanic_record WHERE mechanic_id = ?");
                                                                mysqli_stmt_bind_param($mechanic_info_query, "s", $mechanic_id);
                                                                mysqli_stmt_execute($mechanic_info_query);
                                                                mysqli_stmt_store_result($mechanic_info_query);
                                                                
                                                                if(mysqli_stmt_num_rows($mechanic_info_query) > 0) {
                                                                    mysqli_stmt_bind_result($mechanic_info_query, $mechanic_id, $mechanic_name);
                                                                    mysqli_stmt_fetch($mechanic_info_query);
                                                                    
                                                                    echo "<option value='$mechanic_id'>$mechanic_name</option>";
                                                                } else {
                                                                    echo "<option value=''>No branch found for user!</option>";
                                                                }
                                                                
                                                                mysqli_stmt_close($mechanic_info_query);
                                                            }
                                                        } else {
                                                            echo "<option value=''>No users found!</option>";
                                                        }

                                                        mysqli_stmt_close($mechanic_name_query);
                                                    ?>


                                                    </select>
                                                </div>
                                            </div>
                                                    <label>Services</label>
                                                    <div class="container mt-2">
                                                        <select name="selected_services[]" class="selectpicker custom-width form-control" multiple aria-label="Default select example" data-live-search="true" onchange="addPriceField()">

                                                        <?php
                                                            // Prepare statement for fetching services
                                                            $services_query = mysqli_prepare($conn, "SELECT * FROM services");
                                                            mysqli_stmt_execute($services_query);
                                                            mysqli_stmt_store_result($services_query);

                                                            if(mysqli_stmt_num_rows($services_query) > 0) {
                                                                mysqli_stmt_bind_result($services_query, $service_id, $service_name);

                                                                while(mysqli_stmt_fetch($services_query)) {
                                                                    // Fetch the branch information for each user
                                                                    $service_info_query = mysqli_prepare($conn, "SELECT * FROM services WHERE service_id = ?");
                                                                    mysqli_stmt_bind_param($service_info_query, "s", $service_id);
                                                                    mysqli_stmt_execute($service_info_query);
                                                                    mysqli_stmt_store_result($service_info_query);
                                                                    
                                                                    if(mysqli_stmt_num_rows($service_info_query) > 0) {
                                                                        mysqli_stmt_bind_result($service_info_query, $service_id, $service_name);
                                                                        mysqli_stmt_fetch($service_info_query);
                                                                        
                                                                        echo "<option value='$service_id'>$service_name</option>";
                                                                    } else {
                                                                        echo "<option value=''>No service found!</option>";
                                                                    }
                                                                    
                                                                    mysqli_stmt_close($service_info_query);
                                                                }
                                                            } else {
                                                                echo "<option value=''>No service found!</option>";
                                                            }

                                                            mysqli_stmt_close($services_query);
                                                        ?>

                                                        </select>
                                                    </div>
                                                    <div id="priceFieldContainer" class="mt-1"></div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="add_mechanic" class="btn btn-submit mt-2">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
    function addPriceField() {
        var selectedServices = document.getElementsByName("selected_services[]")[0].selectedOptions;
        var priceFieldContainer = document.getElementById("priceFieldContainer");
        priceFieldContainer.innerHTML = ""; // Clear existing content

        for (var i = 0; i < selectedServices.length; i++) {
            var selectedService = selectedServices[i];
            var inputGroup = document.createElement("div");
            inputGroup.className = "input-group mb-3";
            
            var inputField = document.createElement("input");
            inputField.type = "text";
            inputField.className = "form-control";
            inputField.name = "service_prices[]"; // Ensure it's an array
            inputField.placeholder = "Enter price for " + selectedService.text;

            var breakLine = document.createElement("br");

            var inputGroupText = document.createElement("span");
            inputGroupText.className = "input-group-text";
            inputGroupText.innerHTML = "₱";

            inputGroup.appendChild(inputGroupText);
            inputGroup.appendChild(inputField);
            priceFieldContainer.appendChild(inputGroup);
            priceFieldContainer.appendChild(breakLine);
        }
    }
</script>
<div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="../settings.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
            $('#settings_usertype').val(usertype);
        }); 
    });
</script>

<script>
    function openPopup() {
        document.getElementById("addservice-popup").style.display = "block";
    }
</script>


</body>
</html>