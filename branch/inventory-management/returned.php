<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();
if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo"Record not Found!";
    exit;
}

// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype FROM users_branch WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
}

date_default_timezone_set('Asia/Manila');
if(isset($_POST['barcode'])) {
    $barcode = $_POST['barcode'];

    $product_query = "SELECT products.productname FROM products 
                JOIN outflow_selling_branch ON products.barcode = outflow_selling_branch.barcode
                WHERE products.barcode = ?";
    $product_stmt = mysqli_prepare($conn, $product_query);
    mysqli_stmt_bind_param($product_stmt, "s", $barcode);
    mysqli_stmt_execute($product_stmt);
    $product_result = mysqli_stmt_get_result($product_stmt);

    if(mysqli_num_rows($product_result) > 0) {
        $rows = mysqli_fetch_assoc($product_result);
        $productname = $rows["productname"];
    }
}

if(isset($_POST['addreturned_product'])){
    $barcode = $_POST['barcode'];
    $price = $_POST['returned_price'];
    $quantity = $_POST['quantity'];
    $remarks = $_POST['remarks'];
    $date_bought = date('Y-m-d', strtotime($_POST['date_bought']));
    $date_returned = date('Y-m-d', strtotime($_POST['date_returned']));

    // Use the original dates in your query
    $returned_query = "INSERT INTO returned_product (barcode, returned_units, price, date_bought, date_returned, remarks, code) VALUES (?, ?, ?, ?, ?, ?, ?)";
    $returned_stmt = mysqli_prepare($conn, $returned_query);
    mysqli_stmt_bind_param($returned_stmt, "sddssss", $barcode, $quantity, $price, $date_bought, $date_returned, $remarks, $code);
    $result = mysqli_stmt_execute($returned_stmt);

    if ($result) {
        $_SESSION['message'] = "Successfully Returned Product for Branch!";
        $_SESSION['message_type'] = "success";
        header('Location: returned.php');
        exit(0);

    } else {
        $_SESSION['message'] = "Error Returning Product for Branch!";
        $_SESSION['message_type'] = "danger";
        header('Location: returned.php');
        exit(0);
    }
}



require '../../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if(isset($_POST['save_excel_data'])){
    $fileName = $_FILES['import_file1']['name'];
    $file_ext = pathinfo($fileName, PATHINFO_EXTENSION);

    $allowed_ext = ['xls','csv','xlsx'];

    if(in_array($file_ext, $allowed_ext))
    {
        $inputFileNamePath = $_FILES['import_file1']['tmp_name'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileNamePath);
        $data = $spreadsheet->getActiveSheet()->toArray();

        $count = 0;
        foreach($data as $row)
        {
            if($count > 0)
            {
                $name = $row['0'];
                $quantity1 = $row['1'];
                $prices = str_replace(',', '', $row['2']);
                $bought_date = $row['3'];
                $returned_date = $row['4'];
                $remark = $row['5'];

                $unixTimestamp_bought = strtotime(str_replace('-', '/', $bought_date));
                $unixTimestamp_returned = strtotime(str_replace('-', '/', $returned_date));

                $formatted_date_bought = date('Y-m-d', $unixTimestamp_bought);
                $formatted_date_returned = date('Y-m-d', $unixTimestamp_returned);

                // Prepare the first query
                $productQuery = "INSERT INTO excel_returned (productname, returned_units, price, date_bought, date_returned, remarks) VALUES (?, ?, ?, ?, ?, ?)";
                $product_stmt = mysqli_prepare($conn, $productQuery);
                mysqli_stmt_bind_param($product_stmt, "siddds", $name, $quantity1, $prices, $unixTimestamp_bought, $unixTimestamp_returned, $remark);
                $result1 = mysqli_stmt_execute($product_stmt);

                if ($result1) {

                    // Prepare the second query
                    $returnedQuery = "INSERT INTO returned_product (`barcode`, `returned_units`, `price`, `date_bought`, `date_returned`, `remarks`, `code`)
                                    SELECT p.barcode, e.returned_units, e.price, e.date_bought, e.date_returned, e.remarks, ?
                                    FROM excel_returned e
                                    JOIN products p ON e.productname = p.productname";
                    $stmt2 = mysqli_prepare($conn, $returnedQuery);

                    // Bind parameters
                    mysqli_stmt_bind_param($stmt2, "s", $code);

                    // Execute the statement
                    $inflowResult1 = mysqli_stmt_execute($stmt2);
                    
                    if($inflowResult1){
                        // Prepare and execute the delete query
                        $deleteQuery1 = "DELETE FROM excel_import";
                        $stmt3 = mysqli_prepare($conn, $deleteQuery1);
                        mysqli_stmt_execute($stmt3);

                        // $inflow_query = "SELECT * FROM inflow_branch WHERE code = ?";
                        // $inflow_query_stmt = mysqli_prepare($conn, $inflow_query);
                        // mysqli_stmt_bind_param($inflow_query_stmt, "s", $code);
                        // $result2 = mysqli_stmt_execute($inflow_query_stmt);

                        // if(mysqli_num_rows($result2) > 0) {
                        //     $rows_inflow = mysqli_fetch_assoc($result2);
                        //     $barcode = $rows_inflow["barcode"];
                        //     $quantity_inflow = $rows_inflow["units_received"];
                        // }

                        // $update_inflow = "UPDATE inflow_branch SET units_received = ? WHERE code = ?";
                        // $update_inflow_stmt = mysqli_prepare($conn, $update_inflow);
                        // mysqli_stmt_bind_param($stmt2, "ss", $quantity1, $code);
                        // $result3 = mysqli_stmt_execute($update_inflow_stmt);

                        // if($result3){
                        //     $_SESSION['message'] = " :)";
                        //     $_SESSION['message_type'] = "success";
                        //     header('Location: returned.php');
                        //     exit(0);
                        // }else{
                        //     $_SESSION['message'] = ":(";
                        //     $_SESSION['message_type'] = "danger";
                        //     header('Location: returned.php');
                        //     exit(0);
                        // }
                    }
                } else {
                    $_SESSION['message'] = "Error Inserting Data!";
                    $_SESSION['message_type'] = "danger";
                    header('Location: returned.php');
                    exit(0);
                }

                
            }
            else
            {
                $count = 1;
            }
        }

        if(isset($msg))
        {
            $_SESSION['message'] = "Successfully Imported";
            $_SESSION['message_type'] = "success";
            header('Location: returned.php');
            exit(0);
        }
    }
    else
    {
        $_SESSION['message'] = "Invalid File";
        $_SESSION['message_type'] = "danger";
        header('Location: returned.php');
        exit(0);
    }
}
// Prepare statement to fetch product details
$product_query = "SELECT DISTINCT
                    rp.id,
                    rp.date_bought,
                    rp.date_returned,
                    p.productname,
                    rp.barcode,
                    rp.price,
                    rp.returned_units,
                    rp.remarks
                    FROM
                    returned_product rp
                    JOIN
                    products p ON rp.barcode = p.barcode
                    JOIN
                    users_branch ub ON rp.code = ub.code
                    WHERE ub.code = ?
                    ORDER BY rp.date_returned DESC";
$product_stmt = mysqli_prepare($conn, $product_query);
mysqli_stmt_bind_param($product_stmt, "s", $code);
mysqli_stmt_execute($product_stmt);
$product_result = mysqli_stmt_get_result($product_stmt);

?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->
    <title>Herb and Angel | Returned</title>
</head>
<body>

<style></style>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
                <a href="/branch/branch-dashboard.php" class="logo">
                    <img src="../../assets/img/logo (1).png" alt="">
                </a>

               <a href="../branch-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                                <span class="status online"></span></span>
                                <div class="profilesets">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <a class="dropdown-item settingsBtn"><img src="../../assets/img/icons/settings.svg" class="me-2" alt="img">Settings</a>
                            <a class="dropdown-item logout pb-0" href="../../index.php"><img src="../../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="../../index.php">Logout</a>
                </div>
            </div>
        </div>

        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                            <li class="active">
                                <a href="../branch-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                            </li>

                            <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                    <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                </ul>
                            </li>

                            <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                        <ul>
                                            <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                            <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                            <li><a href="../inventory-management/returned.php">Returned</a></li>
                                            <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                            <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                        </ul>
                            </li>                             

                            <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wrench.svg" alt="img"><span> Mechanic</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li><a href="../mechanic/mechanic.php">Mechanic</a></li>
                                </ul>
                            </li>

                            <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bxs-truck.svg" alt="img"><span> Logistics</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li><a href="../logistics/records.php">Logistics Records</a></li>
                                </ul>
                            </li>

                            
                            <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wallet.svg" alt="img"><span> Expenses</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li><a href="../expenses/records.php">Expenses Records</a></li>
                                </ul>
                            </li>

                            <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0);"><span style="font-weight:bold;"> Profit</span> <span class="menu-arrow"></span></a>
                                                <ul>
                                                    <li><a href="../report/product.php">Profit from Products</a></li>
                                                    <li><a href="../report/labor.php">Profit from Labor</a></li>
                                                    <li><a href="../report/profit.php">All Profit</a></li>
                                                </ul>
                                        </li>
                                        <hr>
                                        <li><a href="../report/inventory.php" style="font-weight:bold;">Inventory Report</a></li>
                            
                                    </ul>
                            </li>
                            <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> Mechanic Management</span> <span class="menu-arrow"></span></a>
                                        <ul>
                                            <li><a href="../mechanic-management/records.php">Accounts</a></li>
                                        </ul>
                            </li>  
                    </ul>
                </div>
            </div>
        </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="col-lg-6 col-12" style="width: 100%;">
                        <?php
                            if (isset($_SESSION['message'])) {
                                $alertClass = ($_SESSION['message_type'] == 'success') ? 'alert-success' : 'alert-danger';
                                echo "<div class='alert $alertClass' role='alert'>" . $_SESSION['message'] . "</div>";
                                unset($_SESSION['message']); // Clear the session variable after displaying the message
                            }
                        ?>
                    </div>
                    <div class="page-title d-flex justify-content-between align-items-center">
                        <h2 style="font-weight: bold;">RETURNED PRODUCTS</h2>
                        <div class="btn-container">
                            <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addreturned" onclick="openPopup()">Returned Products</button>
                        </div>
                        
                    </div>
                    
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Item #</th>
                                <th scope="col">Product Barcode <br> Number</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Units Returned</th>
                                <th scope="col">Returned Price</th>
                                <th scope="col">Date Bought</th>
                                <th scope="col">Date Returned</th>
                                <th scope="col">Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                                // Loop through the product results and display them in the table
                                while ($product_row = mysqli_fetch_assoc($product_result)) {
                                    echo '<tr>';
                                    echo '<td>' . $product_row['id'] . '</td>';
                                    echo '<td>' . $product_row['barcode'] . '</td>';
                                    echo '<td>' . $product_row['productname'] . '</td>';
                                    echo '<td>' . $product_row['returned_units'] . '</td>';
                                    echo '<td>' . $product_row['price'] . '</td>';
                                    echo '<td>' . date('m-d-y', strtotime($product_row['date_bought'])) . '</td>';
                                    echo '<td>' . date('m-d-y', strtotime($product_row['date_returned'])) . '</td>';
                                    echo '<td>' . $product_row['remarks'] . '</td>';
                                    echo '</tr>';
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>

    <div class="popup" id="returned-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addreturned" tabindex="-1" role="dialog" aria-labelledby="addreturnedModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header d-flex justify-content-between align-items-center">
                            <h5 class="modal-title">Returned Products</h5>
                            <div style="display: flex; align-items: center;">
                                <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#importExcelAdmin" onclick="openExcelBranch()">Import Excel</button>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                            </div>
                        </div>

                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Barcode</label>
                                                    <input type="text" class="form-control" name="barcode" id="barcode" value="<?php echo @$_POST['barcode']?>" onblur="fetchProductName()" placeholder="Enter Barcode" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" class="form-control" name="productname" id="productname" value="" placeholder="Enter Product Name" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Returned Price</label>
                                                    <input type="text" class="form-control" name="returned_price" id="returned_price" placeholder="Returned Price per Piece" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Units Returned</label>
                                                    <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Enter Units Returned" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Date Bought</label>
                                                    <input type="date" class="form-control" name="date_bought" id="date_bought" placeholder="Enter Date Bought" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Date Returned</label>
                                                    <input type="date" class="form-control" name="date_returned" id="date_returned" placeholder="Enter Date Returned" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Remarks</label>
                                                    <input type="text" class="form-control" name="remarks" id="remarks" placeholder="Enter Remarks" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="addreturned_product" class="btn btn-submit me-2">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="importexcel-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="importExcelBranch" tabindex="-1" role="dialog" aria-labelledby="importExcelLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="importExcelModalLabel">Import Excel</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                    <label>Location</label>
                                                    <?php
                                                        $location_query = "SELECT branch_record.branch_address FROM branch_record branch_record JOIN users_branch users_branch ON branch_record.code = users_branch.branch_code WHERE users_branch.code = ?";
                                                        $location_query_result = mysqli_prepare($conn, $location_query);
                                                        mysqli_stmt_bind_param($location_query_result, "s", $code);
                                                        mysqli_stmt_execute($location_query_result);
                                                        mysqli_stmt_store_result($location_query_result);

                                                        if (mysqli_stmt_num_rows($location_query_result) > 0) {
                                                            // Fetch the data from the result set
                                                            mysqli_stmt_bind_result($location_query_result, $location_address);
                                                            mysqli_stmt_fetch($location_query_result);
                                                        } else {
                                                            // Handle query error
                                                            $location_address = "Error fetching location";
                                                        }
                                                    ?>

                                                        <input type="hidden" class="form-control" name="location_branch_hidden" id="location_branch_hidden" value="<?php echo $location_code?>" placeholder="<?php echo $location_address?>" readonly>
                                                        <input type="text" class="form-control" name="location_branch" id="location_branch" value="" placeholder="<?php echo $location_address?>" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Upload Excel File</label>
                                                        <div class="image-upload">
                                                            <input type="file" name="import_file1" class="custom-file-input" id="inputGroupFile01" required>
                                                            <div class="image-uploads">
                                                                <img src="../../assets/img/icons/upload.svg" alt="img">
                                                                    <h4>Drag and drop a file to upload</h4>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <button type="submit" name="save_excel_data" class="btn btn-submit me-2">Add Returned Products for Branch</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="../settings.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>

<script src="../assets/js/script.js"></script>
<script>
    function openPopup() {
        document.getElementById("returned-popup").style.display = "block";
    }

    function openExcelBranch(){
        document.getElementById("importexcel-popup").style.display = "block";
    }
</script>

<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
            $('#settings_usertype').val(usertype);
        }); 
    });
</script>

<script>
    function fetchProductName() {
        var barcode = document.getElementById('barcode').value;
        if (barcode.trim() !== '') {
            $.ajax({
                type: 'POST',
                url: 'fetch_name_returned.php',
                data: { barcode: barcode },
                success: function(response) {
                    document.getElementById('productname').value = response;
                },
                error: function(error) {
                    alert('Error fetching product name from the database.');
                    console.error(error);
                }
            });
        }
    }
</script>
</body>
</html>