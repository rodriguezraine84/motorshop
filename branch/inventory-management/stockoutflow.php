<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();

if(isset($_SESSION["code"])) {
    $code = $_SESSION["code"];
} else {
    echo "Record not Found!";
    exit;
}

// Using prepared statement to prevent SQL injection
$name_query = mysqli_prepare($conn, "SELECT firstname, email, password, contact, usertype, branch_code FROM users_branch WHERE code = ?");
mysqli_stmt_bind_param($name_query, "s", $code);
mysqli_stmt_execute($name_query);
$result = mysqli_stmt_get_result($name_query);

if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_assoc($result);

    $fullname = $row["firstname"];
    $email = $row["email"];
    $password = $row["password"];
    $contact = $row["contact"];
    $usertype = $row["usertype"];
    $branch_code = $row["branch_code"];
}

$productname = "";

if(isset($_POST['barcode'])) {
    $barcode = $_POST['barcode'];

    $product_query = "SELECT products.productname FROM products 
                JOIN inflow_branch ON products.barcode = inflow_branch.barcode
                WHERE products.barcode = ?";
    $product_stmt = mysqli_prepare($conn, $product_query);
    mysqli_stmt_bind_param($product_stmt, "s", $barcode);
    mysqli_stmt_execute($product_stmt);
    $product_result = mysqli_stmt_get_result($product_stmt);

    if(mysqli_num_rows($product_result) > 0) {
        $rows = mysqli_fetch_assoc($product_result);
        $productname = $rows["productname"];
    }
}

if(isset($_POST['addstocks'])) {
    $date = date("Y-m-d H:i:s");
    $barcode = $_POST['barcode'];
    $selling = $_POST['sellingprice'];
    $units = $_POST['units'];
    $discount = $_POST['discount'];
    $supplier_price = $_POST['supplierPrice'];
    
    $total_value = $selling * $units;
    $total_value_supplier = $supplier_price * $units;
    $profit = $selling - ($supplier_price * $units);
    $total_profit = $profit - $discount;

    $outflow_selling = "INSERT INTO outflow_selling_branch (`date`, `barcode`, `selling_price`, `units_sold`, `supplier_price`, `discount`, `total_value_selling`, `total_value_supplier`, `profit`, `total_profit`, `code`) VALUES (NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $outflow_stmt = mysqli_prepare($conn, $outflow_selling);
    mysqli_stmt_bind_param($outflow_stmt, "siiiidddddd", $barcode, $selling, $units, $supplier_price, $discount, $total_value, $total_value_supplier, $profit, $total_profit, $code, $branch_code);
    mysqli_stmt_execute($outflow_stmt);
}
require '../../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if (isset($_POST['save_excel_data'])) {
    $fileName = $_FILES['import_file']['name'];
    $file_ext = pathinfo($fileName, PATHINFO_EXTENSION);

    $allowed_ext = ['xls', 'csv', 'xlsx'];

    if (in_array($file_ext, $allowed_ext)) {
        $inputFileNamePath = $_FILES['import_file']['tmp_name'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileNamePath);
        $data = $spreadsheet->getActiveSheet()->toArray();

        $count = 0;
        foreach ($data as $row) {
            if ($count > 0) {
                $date = !empty($row[1]) ? $row[1] : null;
                $productname = !empty($row[2]) ? $row[2] : null;
                $srp = !empty($row[3]) ? str_replace(',', '', $row[3]) : 0;
                $units_sold = !empty($row[4]) ? $row[4] : 0;
                $totalvalue_srp = !empty($row[5]) ? str_replace(',', '', $row[5]) : 0;
                $supplier_price = !empty($row[6]) ? str_replace(',', '', $row[6]) : 0;
                $totalvalue_supplier = !empty($row[7]) ? str_replace(',', '', $row[7]) : 0;
                $profit = !empty($row[8]) ? str_replace(',', '', $row[8]) : 0;
                $discount = isset($row[9]) ? $row[9] : 0;
                $totalvalue = isset($row[10]) ? str_replace(',', '', $row[10]) : 0;

                // Validate that the essential fields are not null
                if ($date && $productname) {
                    $unixTimestamp = strtotime(str_replace('-', '/', $date));
                    $formatted_date = date('Y-m-d', $unixTimestamp);

                    // Prepare the first query
                    $productQuery = "INSERT INTO excel_import (date, productname, srp, units_sold, totalvalue_srp, supplier_price, totalvalue_supplier, profit, discount, totalvalue) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    $stmt = mysqli_prepare($conn, $productQuery);

                    // Bind parameters
                    mysqli_stmt_bind_param($stmt, "ssiiiiiiii", $formatted_date, $productname, $srp, $units_sold, $totalvalue_srp, $supplier_price, $totalvalue_supplier, $profit, $discount, $totalvalue);

                    // Execute the statement
                    $result = mysqli_stmt_execute($stmt);

                    if ($result) {
                        // Data successfully inserted into excel_import table, now insert into outflow_selling_branch
                        $code_branch = $branch_code;

                        // Prepare the second query
                        $inflowQuery = "INSERT INTO outflow_selling_branch (date, barcode, selling_price, units_sold, supplier_price, total_value_selling, total_value_supplier, profit, discount, total_profit, code) 
                                        SELECT e.date, p.barcode, e.srp, e.units_sold, e.supplier_price, e.totalvalue_srp, e.totalvalue_supplier, e.profit, e.discount, e.totalvalue, ?
                                        FROM excel_import e
                                        JOIN products p ON e.productname = p.productname";
                        $stmt2 = mysqli_prepare($conn, $inflowQuery);

                        // Bind parameter
                        mysqli_stmt_bind_param($stmt2, "s", $code_branch);

                        // Execute the statement
                        $inflowResult = mysqli_stmt_execute($stmt2);

                        if ($inflowResult) {
                            // Prepare and execute the delete query
                            $deleteQuery = "DELETE FROM excel_import";
                            $stmt3 = mysqli_prepare($conn, $deleteQuery);
                            mysqli_stmt_execute($stmt3);
                        }
                    } else {
                        $_SESSION['message'] = "Error Inserting Data!";
                        $_SESSION['message_type'] = "danger";
                        header('Location: stockinflow.php');
                        exit(0);
                    }
                }
            } else {
                $count = 1;
            }
        }

        $_SESSION['message'] = "Successfully Imported";
        $_SESSION['message_type'] = "success";
        header('Location: stockinflow.php');
        exit(0);
    } else {
        $_SESSION['message'] = "Invalid File";
        $_SESSION['message_type'] = "danger";
        header('Location: stockinflow.php');
        exit(0);
    }
}


date_default_timezone_set('Asia/Manila');
$product_query = "SELECT DISTINCT
                    o.date,
                    p.productname,
                    o.selling_price,
                    o.units_sold,
                    o.supplier_price,
                    i.totalvalue,
                    o.total_value_selling,
                    o.total_value_supplier,
                    o.profit,
                    o.discount,
                    o.total_profit,
                    o.barcode
                    FROM
                    outflow_selling_branch o
                    JOIN
                    inflow_branch i ON o.barcode = i.barcode
                    JOIN
                    products p ON o.barcode = p.barcode
                    WHERE o.code = ? AND DATE(o.date) = CURDATE()";
$product_stmt = mysqli_prepare($conn, $product_query);
mysqli_stmt_bind_param($product_stmt, "s", $code);
mysqli_stmt_execute($product_stmt);
$product_result = mysqli_stmt_get_result($product_stmt);
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href=".../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->
    <title>Herb and Angel | Stock Outflow</title>
</head>
<style>
    .btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }
    
    .table-responsive{
        overflow-x: auto;
    }
</style>

<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
               <a href="/branch/branch-dashboard.php" class="logo">
                <img src="../../assets/img/logo (1).png" alt="">
               </a>

               <a href="../branch-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                                <span class="status online"></span></span>
                                <div class="profilesets">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <a class="dropdown-item settingsBtn"><img src="../../assets/img/icons/settings.svg" class="me-2" alt="img">Settings</a>
                            <a class="dropdown-item logout pb-0" href="../../index.php"><img src="../../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="../../index.php">Logout</a>
                </div>
            </div>
        </div>

        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                            <li class="active">
                                <a href="../branch-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                            </li>

                            <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                    <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                </ul>
                            </li>

                            <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                        <ul>
                                            <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                            <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                            <li><a href="../inventory-management/returned.php">Returned</a></li>
                                            <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                            <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                        </ul>
                            </li>                             

                            <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wrench.svg" alt="img"><span> Mechanic</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li><a href="../mechanic/mechanic.php">Mechanic</a></li>
                                </ul>
                            </li>

                            <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bxs-truck.svg" alt="img"><span> Logistics</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li><a href="../logistics/records.php">Logistics Records</a></li>
                                </ul>
                            </li>

                            
                            <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wallet.svg" alt="img"><span> Expenses</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li><a href="../expenses/records.php">Expenses Records</a></li>
                                </ul>
                            </li>

                            <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0);"><span style="font-weight:bold;"> Profit</span> <span class="menu-arrow"></span></a>
                                                <ul>
                                                    <li><a href="../report/product.php">Profit from Products</a></li>
                                                    <li><a href="../report/labor.php">Profit from Labor</a></li>
                                                    <li><a href="../report/profit.php">All Profit</a></li>
                                                </ul>
                                        </li>
                                        <hr>
                                        <li><a href="../report/inventory.php" style="font-weight:bold;">Inventory Report</a></li>
                            
                                    </ul>
                            </li>
                            <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> Mechanic Management</span> <span class="menu-arrow"></span></a>
                                        <ul>
                                            <li><a href="../mechanic-management/records.php">Accounts</a></li>
                                        </ul>
                            </li>  
                    </ul>
                </div>
            </div>
        </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="page-title d-flex justify-content-between align-items-center">
                        <h2 style="font-weight: bold; margin: 0;">STOCK OUTFLOW</h2>
                        <div class="btn-container">
                            <button type="button" class="btn btn-submit" data-bs-toggle="modal" data-bs-target="#addstocks" onclick="openPopup()">Sold Stocks</button>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="vertical-headers">
                                <tr>
                                    <th scope="col">Date</th>
                                    <th scope="col">Product Barcode <br>Number</th>
                                    <th scope="col">Product Name</th>
                                    <th scope="col">Selling Price</th>
                                    <th scope="col">Units Sold</th>
                                    <th scope="col">Total value of <br>Selling Price</th>
                                    <th scope="col">Supplier Price</th>
                                    <th scope="col">Total value of <br>Supplier Price</th>
                                    <th scope="col">Profit</th>
                                    <th scope="col">Customer <br>Discount</th>
                                    <th scope="col">Total <br>Profit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    // Loop through the product results and display them in the table
                                    while ($product_row = mysqli_fetch_assoc($product_result)) {
                                        echo '<tr>';
                                        echo '<td>' . date('m-d-y', strtotime($product_row['date'])) . '</td>';
                                        echo '<td>' . $product_row['barcode'] . '</td>';
                                        echo '<td>' . $product_row['productname'] . '</td>';
                                        echo '<td>' . $product_row['selling_price'] . '</td>';
                                        echo '<td>' . $product_row['units_sold'] . '</td>';
                                        echo '<td>' . $product_row['total_value_selling'] . '</td>';
                                        echo '<td>' . $product_row['supplier_price'] . '</td>';
                                        echo '<td>' . $product_row['total_value_supplier'] . '</td>';
                                        echo '<td>' . $product_row['profit'] . '</td>';
                                        echo '<td>' . $product_row['discount'] . '</td>';
                                        echo '<td>' . $product_row['total_profit'] . '</td>';
                                        echo '</tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
    </div>

    <div class="popup" id="addstocks-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addstocks" tabindex="-1" role="dialog" aria-labelledby="addstocksModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addstocksModalLabel">Sold Product</h5>
                            <div class="d-flex align-items-center ms-auto">
                                <!-- <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#importExcel" onclick="openExcel()">Import Excel</button> -->
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                            </div>
                        </div>

                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Barcode</label>
                                                    <input type="text" class="form-control" name="barcode" id="barcode" value="<?php echo @$_POST['barcode']?>" onblur="fetchProductName()" placeholder="Enter Barcode" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" class="form-control" name="productname" id="productname" value="<?php echo $productname ?>" placeholder="Enter Product Name" readonly>
                                                </div>
                                            </div>
                                            <div class="alert alert-danger" role="alert" id="error-message" style="display:<?php echo empty($error_message) ? 'none' : 'block'; ?>">
                                                <?php echo $error_message; ?>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">

                                                    <label>Supplier Price</label>
                                                    <input type="text" class="form-control" id="supplierPrice" name="supplierPrice" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Selling Price</label>
                                                    <input type="text" class="form-control" name="sellingprice" id="sellingprice" value="<?php echo @$_POST['sellingprice']?>" placeholder="Enter Selling Price" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Units Sold</label>
                                                    <input type="text" class="form-control" name="units" id="units" placeholder="Enter Units Received" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Customer Discount</label>
                                                    <input type="text" class="form-control" name="discount" id="discount" placeholder="Enter Customer Discount">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="addstocks" class="btn btn-submit me-2">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="importexcel-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="importExcelLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="importExcelModalLabel">Import Excel</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Upload Excel File</label>
                                                        <div class="image-upload">
                                                            <input type="file" name="import_file" class="custom-file-input" id="inputGroupFile01">
                                                            <div class="image-uploads">
                                                                <img src="../../assets/img/icons/upload.svg" alt="img">
                                                                    <h4>Drag and drop a file to upload</h4>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <button type="submit" name="save_excel_data" class="btn btn-submit me-2">Add Stocks for Branch</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="settingsModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="settingsModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="../settings.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="settings_code" id="settings_code">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="settings_fullname" id="settings_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="settings_email" id="settings_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="settings_password" id="settings_password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="settings_number" id="settings_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Usertype</label>
                                                    <input type="text" class="form-control" name="settings_usertype" id="settings_usertype" placeholder="Enter Usertype" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="resetrecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    $(document).ready(function () {
        $('.settingsBtn').on('click', function () {
            $('#settingsModal').modal('show');

            var code = "<?php echo $code; ?>";
            var fullname = "<?php echo $fullname; ?>";
            var email = "<?php echo $email; ?>";
            var password = "<?php echo $password; ?>";
            var contact = "<?php echo $contact; ?>";
            var usertype = "<?php echo $usertype; ?>";

            $('#settings_code').val(code);
            $('#settings_fullname').val(fullname);
            $('#settings_email').val(email);
            $('#settings_password').val(password);
            $('#settings_number').val(contact);
            $('#settings_usertype').val(usertype);
        }); 
    });
</script>

<script>
    function openPopup() {
        document.getElementById("addstocks-popup").style.display = "block";
    }

    function openExcel(){
        document.getElementById("importexcel-popup").style.display = "block";
    }
</script>

<script>
    function fetchProductName() {
        var barcode = document.getElementById('barcode').value;
        if (barcode.trim() !== '') {
            $.ajax({
                type: 'POST',
                url: 'fetch_name.php',
                data: { barcode: barcode },
                success: function(response) {
                    document.getElementById('productname').value = response;
                },
                error: function(error) {
                    alert('Error fetching product name from the database.');
                    console.error(error);
                }
            });
        }
    }
</script>

<script>
    function validateSellingPrice() {
        var sellingPrice = parseFloat($('#sellingprice').val());
        var errorMessage = document.getElementById("error-message");

        if (!isNaN(sellingPrice) && !isNaN(<?php echo $supplier_price; ?>) && sellingPrice <= <?php echo $supplier_price; ?>) {
            errorMessage.style.display = "block";
        } else {
            errorMessage.style.display = "none";
        }
    }

    $(document).ready(function() {
        // Trigger form validation when selling price is changed
        $('#sellingprice').on('input', function() {
            validateSellingPrice();
        });

        // Display error message if it exists
        <?php echo empty($error_message) ? '' : 'validateSellingPrice();'; ?>
    });
</script>

<script>
    $(document).ready(function(){
        $('#barcode').on('input', function() {
            var barcode = $(this).val();

            $.ajax({
                url: 'fetch_supplier_price.php',
                type: 'POST',
                data: {barcode: barcode},
                success: function(response) {
                    $('#supplierPrice').val(response);
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        });
    });
</script>

</body>
</html>