<?php
session_start();
include('../../connection.php');

// Fetch product name based on the scanned barcode
$barcode = $_POST['barcode'];
$sql = "SELECT products.productname FROM products 
        JOIN outflow_selling_branch ON products.barcode = outflow_selling_branch.barcode
        WHERE products.barcode = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("s", $barcode);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows > 0) {
    // Return the product name
    $row = $result->fetch_assoc();
    echo $row['productname'];
} else {
    echo "Product not found";
}

// Close the statement
$stmt->close();

// Close the connection
$conn->close();
?>
