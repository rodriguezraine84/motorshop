<?php
include('../connection.php');
session_start();

if(isset($_POST['updaterecord'])) {   
    $code = $_POST['edit_code'];
    $user_name = $_POST['edit_fullname'];
    $user_email = $_POST['edit_email'];
    $password = $_POST['edit_password'];
    $contact_number = $_POST['edit_number'];
    $usertype = "Branch Manager";

    $query = "UPDATE users_branch SET 
        firstname=?, email=?, password=?, contact=?, usertype=?
        WHERE code=?";
    $stmt = mysqli_prepare($conn, $query);

    if ($stmt) {
        mysqli_stmt_bind_param($stmt, "ssssss", $user_name, $user_email, $password, $contact_number, $usertype, $code);
        $query_run = mysqli_stmt_execute($stmt);

        if($query_run) {
            $_SESSION['message'] = "Successfully Updated Account";
            $_SESSION['message_type'] = "success";
            header("Location: branch-dashboard.php");
            exit();
        } else {
            $_SESSION['message'] = "Failed to Update Account";
            $_SESSION['message_type'] = "danger";   
            header("Location: branch-dashboard.php");
            exit();
        }
    } else {
        $_SESSION['message'] = "Prepared statement error";
        $_SESSION['message_type'] = "danger";   
        header("Location: branch-dashboard.php");
        exit();
    }
}
?>
