<?php 
include('../../connection.php');
session_start();

if(isset($_POST['recorddelete']))
{
    $code = $_POST['delete_code'];

    // Prepare and execute the delete statement
    $query_delete = mysqli_prepare($conn, "DELETE FROM mechanic_record WHERE code=?");
    mysqli_stmt_bind_param($query_delete, "s", $code);
    $query_run_delete = mysqli_stmt_execute($query_delete);

    if($query_run_delete)
    {
        $_SESSION['message'] = "Mechanic Deleted Successfully!";
        $_SESSION['message_type'] = "success";
        header("Location: ../mechanic/mechanic.php");
        exit();
    }
    else
    {
        $_SESSION['message'] = "Failed to Delete Mechanic!";
        $_SESSION['message_type'] = "danger";
        header("Location: ../mechanic/mechanic.php");
        exit();
    }
}
?>
