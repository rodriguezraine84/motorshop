<?php
include('../../connection.php');
session_start();

if(isset($_POST['updaterecord']))
{   
    $code = $_POST['edit_code'];
    $user_name = $_POST['edit_mechanic_name'];

    // Prepare the update statement
    $query = "UPDATE mechanic_record SET mechanic_name=? WHERE mechanic_id=?";
    $stmt = mysqli_prepare($conn, $query);
    
    if ($stmt) {
        // Bind parameters and execute the statement
        mysqli_stmt_bind_param($stmt, "ss", $user_name, $code);
        $query_run = mysqli_stmt_execute($stmt);
        
        if($query_run)
        {
            $_SESSION['message'] = "Mechanic Service Updated Successfully!";
            $_SESSION['message_type'] = "success";
            header("Location: ../mechanic/mechanic.php");
            exit();
        }
        else
        {
            $_SESSION['message'] = "Failed to Update Mechanic!";
            $_SESSION['message_type'] = "danger";
            header("Location: ../mechanic/mechanic.php");
            exit();
        }
    }
    else
    {
        $_SESSION['message'] = "Failed to Update Mechanic!";
        $_SESSION['message_type'] = "danger";
        header("Location: ../mechanic/mechanic.php");
        exit();
    }
}
?>
