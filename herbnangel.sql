-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2024 at 09:30 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `herbnangel`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch_record`
--

CREATE TABLE `branch_record` (
  `code` varchar(11) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `branch_address` varchar(255) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `branch_record`
--

INSERT INTO `branch_record` (`code`, `branch_name`, `branch_address`, `contact_person`, `contact_number`, `email`) VALUES
('1', 'CALUMPANG', 'Calumpang, Binangonan, Rizal', 'Mark Doblada', '09271174533', 'marknino175@gmail.com'),
('10', 'MARILAQUE', 'Marilaque, Antipolo Rizal', 'Dan Carlo Alvarado', '09455517380', 'dobladaherb09@gmail.com'),
('11', 'LAYUNAN 2', 'FBaltazar Layunan Binangonan Rizal', 'Jude V. Fresnido', '09389844433', 'judefresnido7@gmail.com'),
('2', 'LAYUNAN', 'Estacio JP Rizal Street Layunan , Binangonan, Rizal', 'John Lyod Camero', '09602377565', 'johnlloydcamero123@gmail.com'),
('3', 'CARDONA', 'Cardona, Rizal', 'Aulie Van August G. Raymundo', '09530344672', 'nget1703@gmail.com'),
('4', 'PALANGOY', 'Palangoy, Binangonan Rizal', 'Terence Gabriel I. Madera', '09515819708', 'tgim.gabs@gmail.com'),
('5', 'MABUHAY', 'Phase 2 Mabuhay City Mamatid City of Cabuyao, Laguna', 'Fernando Jr. Pontillas', '09487414785', 'pontillasfern@gmail.com'),
('6', '3 SQUARE CORNER', '3 Square Corner Unit 4 Lot1 A Nia Rd, Banay-banay, City of Cabuyao Laguna', 'Ken Ross T. Ladica', '09547506485', 'koopalkagar@gmail.com'),
('7', 'ALFONSO', 'Alfonso Cavite', 'Jhon Michael R. Medenilla', '09301986196', 'jhon11michael1992@gmail.com'),
('8', 'EDENVILLE ANGONO', 'Edenville Angono, Rizal', 'Jefferson Villanueva Lopez', '09957107070', 'jhoeffgeisha08@gmail.com'),
('9', 'SAN ISIDRO ANGONO', 'San Isidro Angono, Rizal', 'Ricky Mariñas', '09536888389', 'marinasricky218@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `dealer_record`
--

CREATE TABLE `dealer_record` (
  `code` varchar(11) NOT NULL,
  `dealer_name` varchar(100) NOT NULL,
  `dealer_address` varchar(255) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `dealer_record`
--

INSERT INTO `dealer_record` (`code`, `dealer_name`, `dealer_address`, `contact_person`, `contact_number`, `email`) VALUES
('1', 'Dealer 1', 'Dealer 1', 'Dealer 1', 'Dealer 1', 'dealer1@gmail.com'),
('2', 'adds', 'adad', 'sds', 'dadd', 'sdsd');

-- --------------------------------------------------------

--
-- Table structure for table `excel_import`
--

CREATE TABLE `excel_import` (
  `date` date NOT NULL,
  `productname` varchar(255) NOT NULL,
  `supplier_price` decimal(10,2) NOT NULL,
  `units_received` int(11) NOT NULL,
  `total_value` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `excel_inflow`
--

CREATE TABLE `excel_inflow` (
  `date` date NOT NULL,
  `productname` varchar(255) NOT NULL,
  `dealerprice` decimal(10,2) DEFAULT NULL,
  `supplierprice` decimal(10,2) DEFAULT NULL,
  `srp` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `excel_returned`
--

CREATE TABLE `excel_returned` (
  `productname` varchar(255) NOT NULL,
  `returned_units` int(11) NOT NULL,
  `price` float NOT NULL,
  `date_bought` date NOT NULL,
  `date_returned` date NOT NULL,
  `remarks` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expenses_admin`
--

CREATE TABLE `expenses_admin` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `code` int(11) NOT NULL,
  `location` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expenses_branch`
--

CREATE TABLE `expenses_branch` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inflow_admin`
--

CREATE TABLE `inflow_admin` (
  `date` date NOT NULL,
  `barcode` int(11) NOT NULL,
  `supplier_price` float NOT NULL,
  `dealer_price` float NOT NULL,
  `srp` float NOT NULL,
  `units_received` int(11) NOT NULL,
  `totalvalue` varchar(100) NOT NULL,
  `totalvalue_dealer` varchar(255) NOT NULL,
  `totalvalue_srp` varchar(255) NOT NULL,
  `code` int(11) NOT NULL,
  `location` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `inflow_admin`
--

INSERT INTO `inflow_admin` (`date`, `barcode`, `supplier_price`, `dealer_price`, `srp`, `units_received`, `totalvalue`, `totalvalue_dealer`, `totalvalue_srp`, `code`, `location`) VALUES
('2024-04-30', 1170, 150, 100, 200, 0, '', '', '', 1, 'Admin'),
('2024-04-30', 1167, 88, 50, 150, 0, '', '', '', 1, 'Admin'),
('2024-04-30', 1171, 120, 100, 150, 0, '', '', '', 1, 'Admin'),
('2024-04-30', 1168, 88, 50, 150, 0, '', '', '', 1, 'Admin'),
('2024-04-30', 1169, 90, 50, 150, 0, '', '', '', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `inflow_branch`
--

CREATE TABLE `inflow_branch` (
  `date` date NOT NULL,
  `barcode` int(11) NOT NULL,
  `supplier_price` float NOT NULL,
  `dealer_price` float NOT NULL,
  `srp` float NOT NULL,
  `units_received` int(11) NOT NULL,
  `totalvalue_dealer` varchar(255) NOT NULL,
  `totalvalue_srp` varchar(255) NOT NULL,
  `totalvalue` varchar(100) NOT NULL,
  `code` varchar(199) NOT NULL,
  `admin_code` int(11) DEFAULT NULL,
  `dealer_code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `inflow_branch`
--

INSERT INTO `inflow_branch` (`date`, `barcode`, `supplier_price`, `dealer_price`, `srp`, `units_received`, `totalvalue_dealer`, `totalvalue_srp`, `totalvalue`, `code`, `admin_code`, `dealer_code`) VALUES
('2024-06-27', 1513, 30, 20, 40, 5, '100', '200', '150', '3', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logistics_admin`
--

CREATE TABLE `logistics_admin` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `code` int(11) NOT NULL,
  `location` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logistics_branch`
--

CREATE TABLE `logistics_branch` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mechanic_record`
--

CREATE TABLE `mechanic_record` (
  `mechanic_id` int(11) NOT NULL,
  `mechanic_name` varchar(100) NOT NULL,
  `user_branch_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mechanic_services`
--

CREATE TABLE `mechanic_services` (
  `date` date NOT NULL,
  `mechanic_service_id` int(11) NOT NULL,
  `mechanic_id` int(11) NOT NULL,
  `services_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `outflow_selling`
--

CREATE TABLE `outflow_selling` (
  `date` datetime NOT NULL,
  `barcode` int(11) NOT NULL,
  `supplier_price` float NOT NULL,
  `selling_price` float NOT NULL,
  `units_sold` int(11) NOT NULL,
  `total_value_selling` int(11) NOT NULL,
  `total_value_supplier` int(11) NOT NULL,
  `profit` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `total_profit` int(11) NOT NULL,
  `code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `outflow_selling_branch`
--

CREATE TABLE `outflow_selling_branch` (
  `date` date NOT NULL,
  `barcode` int(11) NOT NULL,
  `selling_price` float NOT NULL,
  `units_sold` int(11) NOT NULL,
  `supplier_price` float NOT NULL,
  `total_value_selling` int(11) NOT NULL,
  `total_value_supplier` int(11) NOT NULL,
  `profit` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `total_profit` int(11) NOT NULL,
  `code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `barcode` int(11) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `productname` varchar(100) NOT NULL,
  `dealer_price` float NOT NULL,
  `supplier_price` float NOT NULL,
  `srp` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`barcode`, `product_id`, `productname`, `dealer_price`, `supplier_price`, `srp`) VALUES
(1, '001', '200ML PLATINUM 4T', 0, 0, 0),
(2, '002', '28MM THAI MADE CARBURATOR', 0, 0, 0),
(3, '003', '28MM VELOCITY', 0, 0, 0),
(4, '004', 'B65 BELT AEROX', 0, 0, 0),
(5, '005', '2DP BELT GENUINE', 0, 0, 0),
(6, '006', '2DP BELT REPLACEMENT', 0, 0, 0),
(7, '007', '2DP DRIVE FACE', 0, 0, 0),
(8, '008', '2DP PULLEY NMAX', 0, 0, 0),
(9, '009', '2PH BELT REPLACEMENT', 0, 0, 0),
(10, '0010', '2PH BELT GENUINE', 0, 0, 0),
(11, '0011', '2PH BLOCK SET MIO I 125 YAMAHA ORIGINAL', 0, 0, 0),
(12, '0012', '2PH CAM GEAR', 0, 0, 0),
(13, '0013', '2PH CONNECTING ROD MIO I 125 M3', 0, 0, 0),
(14, '0014', '2PH-E3750-00 TPS YAMAHA', 0, 0, 0),
(15, '0015', '2SX PULLEY MIO I 125', 0, 0, 0),
(16, '0016', '415 ALLOY SPROCKET', 0, 0, 0),
(17, '0017', '415 CHAIN RED', 0, 0, 0),
(18, '0018', '420 CHAIN LOCK', 0, 0, 0),
(19, '0019', '428 CHAIN LOCK', 0, 0, 0),
(20, '0020', '428H CHAIN LOCK', 0, 0, 0),
(21, '0021', '44D BELT GENUINE', 0, 0, 0),
(22, '0022', '44D DRIVEFACE', 0, 0, 0),
(23, '0023', '44K FI CHEMICAL', 0, 0, 0),
(24, '0024', '4D0 CONNECTING ROD MIO SPORTY', 0, 0, 0),
(25, '0025', '4L BATTERY DAYWAY', 0, 0, 0),
(26, '0026', '4L BATTERY EXTREME', 0, 0, 0),
(27, '0027', '5L BATTERY DAYWAY', 0, 0, 0),
(28, '0028', '5L BATTERY EXTREME SMALL', 0, 0, 0),
(29, '0029', '5L BATTERY EXTREME SPORTY', 0, 0, 0),
(30, '0030', '5MX CAM GEAR MIO', 0, 0, 0),
(31, '0031', '5TL BELT REPLACEMENT', 0, 0, 0),
(32, '0032', '5TL BELT GENUINE', 0, 0, 0),
(33, '0033', '5TL CLUTCH SPRING YAMAHA MIO STOCK', 0, 0, 0),
(34, '0034', '5TL DIAPHRAM MIO SPORTY', 0, 0, 0),
(35, '0035', '5TL HEAD 24 28', 0, 0, 0),
(36, '0036', '5TL HEAD STD', 0, 0, 0),
(37, '0037', '5TL CLUTCH LINING PAD ONLY GENUINE', 0, 0, 0),
(38, '0038', '5TL SPORTY BELL GENUINE', 0, 0, 0),
(39, '0039', '6X20 FLAT CNC BOLTS', 0, 0, 0),
(40, '0040', '7L EXTREME BATTERY', 0, 0, 0),
(41, '0041', 'AEROX 155 LIGHTEN FRONT SHOCK', 0, 0, 0),
(42, '0042', 'AEROX VISOR', 0, 0, 0),
(43, '0043', 'AIR BREATHER FILTER', 0, 0, 0),
(44, '0044', 'AIR FILTER CLICK ORDINARY', 0, 0, 0),
(45, '0045', 'AIR FILTER DASH 110', 0, 0, 0),
(46, '0046', 'AIR FILTER MIO I 125', 0, 0, 0),
(47, '0047', 'AIR FILTER MIO SOUL', 0, 0, 0),
(48, '0048', 'AIR FILTER MIO SPORTY', 0, 0, 0),
(49, '0049', 'AIR FILTER NMAX', 0, 0, 0),
(50, '0050', 'AIR FILTER SHOGUN', 0, 0, 0),
(51, '0051', 'AIR FILTER SNIPER 135', 0, 0, 0),
(52, '0052', 'AIR FILTER SNIPER 150', 0, 0, 0),
(53, '0053', 'AIRFILTER MIO GEAR/GRAVIS', 0, 0, 0),
(54, '0054', 'AIRFILTER PCX 150', 0, 0, 0),
(55, '0055', 'AIRFILTER SOULTY', 0, 0, 0),
(56, '0056', 'AIRFILTER TMX SUPREMO', 0, 0, 0),
(57, '0057', 'AIRFILTER WAVE 110', 0, 0, 0),
(58, '0058', 'ALLEN BOLTS ASSORTED', 0, 0, 0),
(59, '0059', 'ALLOY SPACER', 0, 0, 0),
(60, '0060', 'VOLTMETER', 0, 0, 0),
(61, '0061', 'APIDO AIRFILTER PCX ADV', 0, 0, 0),
(62, '0062', 'APIDO RACING CDI SPORTY', 0, 0, 0),
(63, '0063', 'APR FLAT SEAT CLICK BLACK', 0, 0, 0),
(64, '0064', 'APR FLAT SEAT FINO BLACK', 0, 0, 0),
(65, '0065', 'APR FLAT SEAT FINO BLUE', 0, 0, 0),
(66, '0066', 'APR FLAT SEAT M3 BLACK', 0, 0, 0),
(67, '0067', 'APR FLAT SEAT RAIDER 150 FI RED', 0, 0, 0),
(68, '0068', 'APR FLAT SEAT RAIDER150  CARB RED', 0, 0, 0),
(69, '0069', 'APR FLAT SEAT RED RAIDER150', 0, 0, 0),
(70, '0070', 'APR FLAT SEAT SPORTY BLACK', 0, 0, 0),
(71, '0071', 'APR FLAT SEAT SPORTY RED', 0, 0, 0),
(72, '0072', 'APR GAS TANK SPORTY', 0, 0, 0),
(73, '0073', 'APR SEAT COVER WITH TAHI ASSORTED', 0, 0, 0),
(74, '0074', 'ARARO SPORTY', 0, 0, 0),
(75, '0075', 'ARIETE GRIP', 0, 0, 0),
(76, '0076', 'ARIETE HANDLE GRIP (BROWN)', 0, 0, 0),
(77, '0077', 'AUTOMOTIVE SINGLE WIRE', 0, 0, 0),
(78, '0078', 'B65 CLUTCH LINING PAD ONLY GENUINE', 0, 0, 0),
(79, '0079', 'BAJAJ OIL FILTER', 0, 0, 0),
(80, '0080', 'BALL RACE C100 WAVE', 0, 0, 0),
(81, '0081', 'BALL RACE CLICK WAVE 125', 0, 0, 0),
(82, '0082', 'BALL RACE CT 100 BARAKO', 0, 0, 0),
(83, '0083', 'BALL RACE MIO', 0, 0, 0),
(84, '0084', 'BALL RACE RAIDER 150', 0, 0, 0),
(85, '0085', 'BALL RACE SUPREMO', 0, 0, 0),
(86, '0086', 'BALL RACE TMX', 0, 0, 0),
(87, '0087', 'BALL RACE XRM110', 0, 0, 0),
(88, '0088', 'BALLRACE CLICK', 0, 0, 0),
(89, '0089', 'BANGKA SPORTY', 0, 0, 0),
(90, '0090', 'BAR END SIDE MIRROR HEXAGON (BLUE LENSE)', 0, 0, 0),
(91, '0091', 'BAR END SIDE MIRROR ROUND (CLEAR LENSE)', 0, 0, 0),
(92, '0092', 'BARAKO FRONT BRAKE PANEL', 0, 0, 0),
(93, '0093', 'K44 BELT BEAT FI GENUINE', 0, 0, 0),
(94, '0094', 'K97 BELT PCX GENUINE', 0, 0, 0),
(95, '0095', 'BENDIX GY6 125', 0, 0, 0),
(96, '0096', 'BENDIX MIO L9', 0, 0, 0),
(97, '0097', 'BENDIX SUZUKI 125', 0, 0, 0),
(98, '0098', 'BENDIX XRM 110', 0, 0, 0),
(99, '0099', 'BETAGREY ', 0, 0, 0),
(100, '00100', 'BG 44K CHEMICAL', 0, 0, 0),
(101, '00101', 'BG THROTTLE BODY CLEANER', 0, 0, 0),
(102, '00102', 'BH BOLT 6MM W/NUT', 0, 0, 0),
(103, '00103', 'BH BOLT 8MM W/NUT', 0, 0, 0),
(104, '00104', 'BODY SCREW WITH CLIP', 0, 0, 0),
(105, '00105', 'BOM RANGSIT FLAT SEAT CLICK', 0, 0, 0),
(106, '00106', 'BOM RANGSIT FLAT SEAT MIO SPORTY', 0, 0, 0),
(107, '00107', 'BOM RANGSIT FLAT SEAT RAIDER 150 CARB', 0, 0, 0),
(108, '00108', 'BOM RANGSIT FLAT SEAT RAIDER 150 FI', 0, 0, 0),
(109, '00109', 'BOM RANGSIT FLATSEAT CLICK', 0, 0, 0),
(110, '00110', 'BOSCH HORN RELAY WITH SOCKET ', 0, 0, 0),
(111, '00111', 'BOSCH SPARK PLUG A6TC', 0, 0, 0),
(112, '00112', 'BOSCH SPARK PLUG B7RC', 0, 0, 0),
(113, '00113', 'BOSCH SPARK PLUG DATC', 0, 0, 0),
(114, '00114', 'BOSCH SPARKPLUG A7TC', 0, 0, 0),
(115, '00115', 'BOSCH SPARKPLUG D8TC(X5DC)', 0, 0, 0),
(116, '00116', 'BOSCH SPARKPLUG UR2CC', 0, 0, 0),
(117, '00117', 'BRAKE CABLE MAKOTO MIO', 0, 0, 0),
(118, '00118', 'BRAKE CALIPER 4POT AEORX W/ BRACKET', 0, 0, 0),
(119, '00119', 'BRAKE FLUID', 0, 0, 0),
(120, '00120', 'DOMINO BRAKE LEVER MIO', 0, 0, 0),
(121, '00121', 'HONDA BRAKE LEVER SET CLICK', 0, 0, 0),
(122, '00122', 'BRAKE LIGHT BULB', 0, 0, 0),
(123, '00123', 'BRAKE LIGHT SWITCH FRONT RIGHT MIO', 0, 0, 0),
(124, '00124', 'BRAKE LIGHT SWITCH LEFT MIO', 0, 0, 0),
(125, '00125', 'BRAKE LIGHT SWITCH LEFT WAVE', 0, 0, 0),
(126, '00126', 'BRAKE LIGHT SWITCH REAR ORANGE', 0, 0, 0),
(127, '00127', 'BRAKE LIKE SWITCH HONDA', 0, 0, 0),
(128, '00128', 'BRAKE MASTER MIO', 0, 0, 0),
(129, '00129', 'BRAKE MASTER RAIDER 150', 0, 0, 0),
(130, '00130', 'BRAKE MASTER WAVE', 0, 0, 0),
(131, '00131', 'BRAKE PAD BEAT RZ', 0, 0, 0),
(132, '00132', 'BRAKE PAD CLICK RZ', 0, 0, 0),
(133, '00133', 'BRAKE REPAIR KIT  BEAT', 0, 0, 0),
(134, '00134', 'BRAKE REPAIR KIT  XRM', 0, 0, 0),
(135, '00135', 'HONDA BRAKE REPAIR KIT CLICK', 0, 0, 0),
(136, '00136', 'BRAKE SHOE HACHI CT100', 0, 0, 0),
(137, '00137', 'BRAKE SHOE HACHI HD3', 0, 0, 0),
(138, '00138', 'BRAKE SHOE HACHI RS100 REAR', 0, 0, 0),
(139, '00139', 'BRAKE SHOE MTR BEAT', 0, 0, 0),
(140, '00140', 'BRAKE SHOE MTR CLICK', 0, 0, 0),
(141, '00141', 'BRAKE SHOE MTR MIO', 0, 0, 0),
(142, '00142', 'BRAKE SHOE SKYDRIVE FI NEXT', 0, 0, 0),
(143, '00143', 'BRAKE SHOE TMX 155 FRONT', 0, 0, 0),
(144, '00144', 'BRAKE SHOE TMX 155 REAR', 0, 0, 0),
(145, '00145', 'BRAKE SYSTEM MIO', 0, 0, 0),
(146, '00146', 'BRAKE SYSTEM SUZUKI', 0, 0, 0),
(147, '00147', 'BRAKEMASTER WAVE ', 0, 0, 0),
(148, '00148', 'BRAMBO CALIPER CLICK 4POT', 0, 0, 0),
(149, '00149', 'BREMBO 2POT NMAX REAR', 0, 0, 0),
(150, '00150', 'BREMBO 4 POT NMAX FRONT', 0, 0, 0),
(151, '00151', 'BREMBO CALIPER 4POT AEROX', 0, 0, 0),
(152, '00152', 'BREMBO CALIPER 4POT COCKROACH', 0, 0, 0),
(153, '00153', 'BREMBO CALIPER BATMAN CLICK', 0, 0, 0),
(154, '00154', 'BREMBO CALIPER CLICK 2POT', 0, 0, 0),
(155, '00155', 'BREMBO CALIPER DORAEMON', 0, 0, 0),
(156, '00156', 'BREMBO CALIPER MINI GP', 0, 0, 0),
(157, '00157', 'BREMBO CALIPER MOUSE SNIPER ', 0, 0, 0),
(158, '00158', 'BREMBO CALIPER NGO MIO', 0, 0, 0),
(159, '00159', 'BREMBO CALIPER RED SNIPER', 0, 0, 0),
(160, '00160', 'BREMBO CALIPER TURTLE BACK', 0, 0, 0),
(161, '00161', 'BREMBO DISC 200MM', 0, 0, 0),
(162, '00162', 'BREMBO DISC 220MM', 0, 0, 0),
(163, '00163', 'BREMBO MASTER BRAKE LEFT ONLY', 0, 0, 0),
(164, '00164', 'BREMBO MONOBLOCK CALIPER ', 0, 0, 0),
(165, '00165', 'BREMBO NICKEL WAVE', 0, 0, 0),
(166, '00166', 'BREMBO P19 BRAKEMASTER', 0, 0, 0),
(167, '00167', 'BREMBO P19 NMAX/PCX MASTER', 0, 0, 0),
(168, '00168', 'BREMBO PS16 SET BRAKE MASTER', 0, 0, 0),
(169, '00169', 'BREMBO ROSSI PREMIUM X KHOKEN', 0, 0, 0),
(170, '00170', 'BREMBO VENTILATED DISC', 0, 0, 0),
(171, '00171', 'BULSA SPORTY', 0, 0, 0),
(172, '00172', 'BUTA DISC FRONT NMAX', 0, 0, 0),
(173, '00173', 'BUTA DISC MIO I 125', 0, 0, 0),
(174, '00174', 'BUTA DISC REAR NMAX', 0, 0, 0),
(175, '00175', 'BUTA DISC SPORTY', 0, 0, 0),
(176, '00176', 'BUTA DISC. WAVE 125', 0, 0, 0),
(177, '00177', 'BWIN BACKPLATE BEAT FI', 0, 0, 0),
(178, '00178', 'BWIN BACKPLATE MIO I 125', 0, 0, 0),
(179, '00179', 'BWIN BACKPLATE MIO SPORTY', 0, 0, 0),
(180, '00180', 'BWIN BACKPLATE NMAX', 0, 0, 0),
(181, '00181', 'BWIN CENTER SPRING CLICK', 0, 0, 0),
(182, '00182', 'BWIN CENTER SPRING MIO SPORTY', 0, 0, 0),
(183, '00183', 'BWIN CENTER SPRING NMAX', 0, 0, 0),
(184, '00184', 'BWIN CENTER SPRING PCX', 0, 0, 0),
(185, '00185', 'BWIN CENTER SPRING BEAT FI', 0, 0, 0),
(186, '00186', 'BWIN CENTER SPRING MIO I125  1500RPM', 0, 0, 0),
(187, '00187', 'BWIN CLUTCH BELL BEAT FI', 0, 0, 0),
(188, '00188', 'BWIN CLUTCH BELL CLICK', 0, 0, 0),
(189, '00189', 'BWIN CLUTCH BELL MIO 100 SPORTY', 0, 0, 0),
(190, '00190', 'BWIN CLUTCH BELL MIO I 125 M3', 0, 0, 0),
(191, '00191', 'BWIN CLUTCH BELL NMAX AEROX', 0, 0, 0),
(192, '00192', 'BWIN CLUTCH BELL PCX', 0, 0, 0),
(193, '00193', 'BWIN CLUTCH SHOE LINING BEAT FI', 0, 0, 0),
(194, '00194', 'BWIN CLUTCH SHOE LINING CLICK', 0, 0, 0),
(195, '00195', 'BWIN CLUTCH SHOE LINING MIO', 0, 0, 0),
(196, '00196', 'BWIN CLUTCH SHOE LINING MIO M3', 0, 0, 0),
(197, '00197', 'BWIN CLUTCH SHOE LINING NMAX155', 0, 0, 0),
(198, '00198', 'BWIN CLUTCH SHOE LINING PCX 160', 0, 0, 0),
(199, '00199', 'BWIN CLUTCH SPRING BEAT FI', 0, 0, 0),
(200, '00200', 'BWIN CLUTCH SPRING MIO I 125 M3', 0, 0, 0),
(201, '00201', 'BWIN CLUTCH SPRING MIO SPORTY', 0, 0, 0),
(202, '00202', 'BWIN CLUTCH SPRING NMAX', 0, 0, 0),
(203, '00203', 'BWIN CLUTCH SPRING CLICK', 0, 0, 0),
(204, '00204', 'BWIN CYLINDER BLOCK CLICK 125 STD 52.4MM', 0, 0, 0),
(205, '00205', 'BWIN CYLINDER BLOCK MIO I 125 M3 59MM', 0, 0, 0),
(206, '00206', 'BWIN CYLINDER BLOCK MIO I 125 M3 STD 52.4MM', 0, 0, 0),
(207, '00207', 'BWIN CYLINDER BLOCK MIO SPORTY 54MM', 0, 0, 0),
(208, '00208', 'BWIN CYLINDER BLOCK MIO SPORTY STD 50MM', 0, 0, 0),
(209, '00209', 'BWIN DEGREASER', 0, 0, 0),
(210, '00210', 'BWIN ENGINE VALVES LIGHTEN MIO 100', 0, 0, 0),
(211, '00211', 'BWIN ENGINE VALVES LIGHTEN NMAX 19 22', 0, 0, 0),
(212, '00212', 'BWIN ENGINE VALVES LIGHTEN NMAX 20 23', 0, 0, 0),
(213, '00213', 'BWIN FLYBALL CLICK', 0, 0, 0),
(214, '00214', 'BWIN FLYBALL BEAT FI', 0, 0, 0),
(215, '00215', 'BWIN FLYBALL MIO I 125 NMAX AEROX', 0, 0, 0),
(216, '00216', 'BWIN FLYBALL MIO SPORTY', 0, 0, 0),
(217, '00217', 'BWIN PULLEY SET BEAT FI', 0, 0, 0),
(218, '00218', 'BWIN PULLEY SET CLICK 125', 0, 0, 0),
(219, '00219', 'BWIN PULLEY SET MIO I 125 M3', 0, 0, 0),
(220, '00220', 'BWIN PULLEY SET MIO M3', 0, 0, 0),
(221, '00221', 'BWIN PULLEY SET MIO SPORTY', 0, 0, 0),
(222, '00222', 'BWIN PULLEY SET NMAX 155', 0, 0, 0),
(223, '00223', 'BWIN PULLEY SET PCX 160', 0, 0, 0),
(224, '00224', 'BWIN ROCKER ARM MIO', 0, 0, 0),
(225, '00225', 'BWIN ROCKER ARM MIO I 125 M3', 0, 0, 0),
(226, '00226', 'BWIN ROLLER TYPE ROCKER ARM MIO', 0, 0, 0),
(227, '00227', 'BWIN SLIDER PIECE BEAT FI', 0, 0, 0),
(228, '00228', 'BWIN SLIDER PIECE CLICK', 0, 0, 0),
(229, '00229', 'BWIN SLIDER PIECE MIO', 0, 0, 0),
(230, '00230', 'BWIN SLIDER PIECE NMAX', 0, 0, 0),
(231, '00231', 'BWIN SLIDING SHEAVE WITH ADAPTOR AEROX NMAX HALF', 0, 0, 0),
(232, '00232', 'BWIN SLIDING SHEAVE WITH ADAPTOR MIO 100 SPORTY HALF', 0, 0, 0),
(233, '00233', 'BWIN SLIDING SHEAVE WITH ADAPTOR MIO I 125 M3 HALF', 0, 0, 0),
(234, '00234', 'BWIN TORQUE DRIVE ASSY BEAT FI', 0, 0, 0),
(235, '00235', 'BWIN TORQUE DRIVE ASSY CLICK 125', 0, 0, 0),
(236, '00236', 'BWIN TORQUE DRIVE ASSY MIO SPORTY', 0, 0, 0),
(237, '00237', 'BWIN TORQUE DRIVE ASSY MIO I125 M3', 0, 0, 0),
(238, '00238', 'BWIN TORQUE DRIVE ASSY NMAX AEROX', 0, 0, 0),
(239, '00239', 'BWIN TORQUE DRIVE ASSY PCX', 0, 0, 0),
(240, '00240', 'BWIN VALVE SEAL MIO', 0, 0, 0),
(241, '00241', 'BWIN VALVE SEAL NMAX/AEROX', 0, 0, 0),
(242, '00242', 'CARB COVER MIO (FIBER)', 0, 0, 0),
(243, '00243', 'CARB REPAIR KIT 24 MM', 0, 0, 0),
(244, '00244', 'CARB REPAIR KIT GY6 125', 0, 0, 0),
(245, '00245', 'CARB REPAIR KIT MIO', 0, 0, 0),
(246, '00246', 'CARB REPAIR KIT W FLOATER XRM 110', 0, 0, 0),
(247, '00247', 'CARB REPAIR KIT W FLOATER XRM 125', 0, 0, 0),
(248, '00248', 'CARB REPAIR KIT WAVE 100 R', 0, 0, 0),
(249, '00249', 'CARB REPAIR KIT WAVE 125', 0, 0, 0),
(250, '00250', 'CARB REPAIR KIT XRM 110', 0, 0, 0),
(251, '00251', 'CARBON BRUSH MIO STX', 0, 0, 0),
(252, '00252', 'CARBON BRUSH SMASH 110', 0, 0, 0),
(253, '00253', 'CARBON BRUSH SNIPER 135', 0, 0, 0),
(254, '00254', 'CARBON BRUSH WAVE 125 SHOGUN', 0, 0, 0),
(255, '00255', 'CARBON BRUSH XRM', 0, 0, 0),
(256, '00256', 'CARBURATOR PISTON REPAIR KIT PLUNGER', 0, 0, 0),
(257, '00257', 'CARBURATOR VELOCITY 28MM', 0, 0, 0),
(258, '00258', 'CDI 4 PIN', 0, 0, 0),
(259, '00259', 'CELLPHONE HOLDER ORDINARY', 0, 0, 0),
(260, '00260', 'CELLPHONE HOLDER WATER PROOF', 0, 0, 0),
(261, '00261', 'CHAIN LUBE KOBY', 0, 0, 0),
(262, '00262', 'CKP SENSOR BEAT CLICK', 0, 0, 0),
(263, '00263', 'CKP SENSOR YAMAHA AEROX NMAX', 0, 0, 0),
(264, '00264', 'CLEAR VIZOR NMAX V2', 0, 0, 0),
(265, '00265', 'HONDA CLICK 125 CLUTCH LINING PAD ONLY GENUINE', 0, 0, 0),
(266, '00266', 'CLICK FLATSEAT NATHONG', 0, 0, 0),
(267, '00267', 'CLUTCH SHOE SKYDRIVE L9', 0, 0, 0),
(268, '00268', 'CLUTCH SPRING RAIDER 150', 0, 0, 0),
(269, '00269', 'CLUTCH SPRING SMASH', 0, 0, 0),
(270, '00270', 'CLUTCH SPRING SPARK 135', 0, 0, 0),
(271, '00271', 'CLUTCH SPRING WAVE 125', 0, 0, 0),
(272, '00272', 'CLUTCH SPRING XRM', 0, 0, 0),
(273, '00273', 'CNC AXLE WAVE REAR ', 0, 0, 0),
(274, '00274', 'CNC BAR-END', 0, 0, 0),
(275, '00275', 'CNC BLOCK 53MM WAVE 100', 0, 0, 0),
(276, '00276', 'CNC BLOCK 54MM WAVE 100', 0, 0, 0),
(277, '00277', 'CNC BLOCK 54MM SPORTY', 0, 0, 0),
(278, '00278', 'CNC BLOCK 59 SPORTY', 0, 0, 0),
(279, '00279', 'CNC BLOCK 59MM MIO I 125 M3', 0, 0, 0),
(280, '00280', 'CNC BLOCK 59MM SPORTY', 0, 0, 0),
(281, '00281', 'CNC BLOCK MIO 50MM STD', 0, 0, 0),
(282, '00282', 'CNC BLOCK MIO I 125 M3 STD 52.4MM', 0, 0, 0),
(283, '00283', 'CNC BLOCK MIO SOUL I 115 STD', 0, 0, 0),
(284, '00284', 'CNC BLOCK SMASH STD', 0, 0, 0),
(285, '00285', 'CNC BLOCK WAVE 125 57MM', 0, 0, 0),
(286, '00286', 'CNC BLOCK WAVE 125 STD', 0, 0, 0),
(287, '00287', 'CNC BLOCK XRM110 53MM', 0, 0, 0),
(288, '00288', 'CNC BOLT AXLE MIO/SNIPER', 0, 0, 0),
(289, '00289', 'CNC BOLT AXLE RAIDER/SONIC', 0, 0, 0),
(290, '00290', 'CNC BOLTS 10X25', 0, 0, 0),
(291, '00291', 'CNC BOLTS WHEEL AXLE AEROX V1 FRONT', 0, 0, 0),
(292, '00292', 'CNC BOLTS WHEEL AXLE CLICK/BEAT FRONT', 0, 0, 0),
(293, '00293', 'CNC BOLTS WHEEL AXLE NMAX V1', 0, 0, 0),
(294, '00294', 'CNC BOLTS WHEEL AXLE WAVE LONG', 0, 0, 0),
(295, '00295', 'CNC BOLTS WHEEL AXLE WAVE REAR STD', 0, 0, 0),
(296, '00296', 'CNC CALIPER BOLT YAMAHA ', 0, 0, 0),
(297, '00297', 'CNC CRANKCASE BOLT GEAR TYPE SNIPER 150', 0, 0, 0),
(298, '00298', 'CNC CRANKCASE BOLT GEAR TYPE W100/W110', 0, 0, 0),
(299, '00299', 'CNC CRANKCASE BOLTS CLICK', 0, 0, 0),
(300, '00300', 'CNC CRANKCASE BOLTS PCX', 0, 0, 0),
(301, '00301', 'CNC CRANKCASE BOLTS RAIDER', 0, 0, 0),
(302, '00302', 'CNC CRANKCASE BOLTS SNIPER 150', 0, 0, 0),
(303, '00303', 'CNC CRANKCASE BOLTS SPORTY', 0, 0, 0),
(304, '00304', 'CNC CRANKCASE BOLTS WAVE 100 110', 0, 0, 0),
(305, '00305', 'CRANK CASE CNC RAIDER FI', 0, 0, 0),
(306, '00306', 'CNC DISC BOLTS', 0, 0, 0),
(307, '00307', 'CNC FLUID BOLTS m4x12', 0, 0, 0),
(308, '00308', 'CNC REAR AXLE NUT PCX/CLICK/ADV/M3', 0, 0, 0),
(309, '00309', 'CNC REAR NUT SPORTY', 0, 0, 0),
(310, '00310', 'CNC SPEEDOMETER SENSOR NMAX/AEROX', 0, 0, 0),
(311, '00311', 'CNC STARTER SKYDRIVE', 0, 0, 0),
(312, '00312', 'CNC VANJO BOLTS FOR BREMBO', 0, 0, 0),
(313, '00313', 'CNC VANJO BOLTS FOR STOCK CALIPER', 0, 0, 0),
(314, '00314', 'CNC VISOR BOLT ', 0, 0, 0),
(315, '00315', 'COLORED BRAKE FLUID', 0, 0, 0),
(316, '00316', 'CONNECTING ROD MIO 1.5 MTRT', 0, 0, 0),
(317, '00317', 'COPPER WASHER', 0, 0, 0),
(318, '00318', 'CVT/DEGREASER 250 ML BWIN', 0, 0, 0),
(319, '00319', 'CYLINDER HEAD AEROX/NMAX 2020 MTRT', 0, 0, 0),
(320, '00320', 'CYLINDER HEAD DASH110 MTRT', 0, 0, 0),
(321, '00321', 'CYLINDER HEAD HACHI WAVE125', 0, 0, 0),
(322, '00322', 'CYLINDER HEAD NMAX MTRT', 0, 0, 0),
(323, '00323', 'DAENG SEAT COVER', 0, 0, 0),
(324, '00324', 'DAYTONA HANDLE GRIP', 0, 0, 0),
(325, '00325', 'DAYTONA QUICK THROTTLE', 0, 0, 0),
(326, '00326', 'DAYWAY 5L SPORTY', 0, 0, 0),
(327, '00327', 'DAYWAY BATTERY 4L', 0, 0, 0),
(328, '00328', 'DAYWAY BATTERY 5L SMALL', 0, 0, 0),
(329, '00329', 'DELO GOLD 1L', 0, 0, 0),
(330, '00330', 'DIBDIB MIO1', 0, 0, 0),
(331, '00331', 'DIBDIB SPORTY', 0, 0, 0),
(332, '00332', 'DISC MIO 190MM 4 HOLES', 0, 0, 0),
(333, '00333', 'DISC MIO 200MM 4HOLES', 0, 0, 0),
(334, '00334', 'DISC MIO SOUL 190MM 3 HOLES', 0, 0, 0),
(335, '00335', 'DIY COOLANT HOSE RADIATOR HOSE SNIPER LC150', 0, 0, 0),
(336, '00336', 'DLH DRIVE FACE', 0, 0, 0),
(337, '00337', 'DOMINO BRAKE LEVER BLACK SPORTY', 0, 0, 0),
(338, '00338', 'DOMINO BRAKE LEVER CARBON SPORTY', 0, 0, 0),
(339, '00339', 'DOMINO BRAKE LEVER GOLD SPORTY', 0, 0, 0),
(340, '00340', 'DOMINO BRAKE LEVER MIO I 125 GOLD', 0, 0, 0),
(341, '00341', 'DOMINO GRIP', 0, 0, 0),
(342, '00342', 'DOMINO HANDLE SWITCH FOR HONDA CLICK', 0, 0, 0),
(343, '00343', 'DOMINO HANDLE SWITCH SET MIO STOCK', 0, 0, 0),
(344, '00344', 'DOMINO HANDLE SWITCH W/ HAZZARD MIO/AEROX', 0, 0, 0),
(345, '00345', 'DOMINO LEVER MIO I 125 M3', 0, 0, 0),
(346, '00346', 'DOMINO LEVER XRM WAVE', 0, 0, 0),
(347, '00347', 'DOMINO MDL TRI SWITCH', 0, 0, 0),
(348, '00348', 'DOMINO QUICK THROTTLE (UNIVERSAL)', 0, 0, 0),
(349, '00349', 'DOMINO QUICK THROTTLE DUAL CABLE W/ CABLE', 0, 0, 0),
(350, '00350', 'DOMINO SWITCH MIO CARBON', 0, 0, 0),
(351, '00351', 'DOMINO THROTTLE CABLE UNIVERSAL', 0, 0, 0),
(352, '00352', 'DOMINO TRI SWITCH HONDA BEAT', 0, 0, 0),
(353, '00353', 'DOMINO UNIVERSAL QUICK THROTTLE', 0, 0, 0),
(354, '00354', 'DRAIN PLUG SPORTY L9', 0, 0, 0),
(355, '00355', 'DRIVE FACE MTRT MIO', 0, 0, 0),
(356, '00356', 'DS4 RADIATOR COVER', 0, 0, 0),
(357, '00357', 'DUAL CONTACT ', 0, 0, 0),
(358, '00358', 'DUST SEAL XRM', 0, 0, 0),
(359, '00359', 'DYNAMO FUEL PUMP HONDA ', 0, 0, 0),
(360, '00360', 'DYNAMO YAMAHA', 0, 0, 0),
(361, '00361', 'EARLS HOSE FRONT ', 0, 0, 0),
(362, '00362', 'EARLS HOSE REAR', 0, 0, 0),
(363, '00363', 'EAT MY DUST TIRES 45 90 17', 0, 0, 0),
(364, '00364', 'EAT MY DUST TIRES 60 80 17', 0, 0, 0),
(365, '00365', 'ECO DRIVE 4L BATTERY', 0, 0, 0),
(366, '00366', 'ECO DRIVE BATTERY 5L', 0, 0, 0),
(367, '00367', 'ELECTRICAL TAPE', 0, 0, 0),
(368, '00368', 'ENGINE SHROUD 1 & 2 MIO SPORTY', 0, 0, 0),
(369, '00369', 'ENGINE SPROCKET BARAKO', 0, 0, 0),
(370, '00370', 'ENGINE VALVES CT100 L9', 0, 0, 0),
(371, '00371', 'ENGINE VALVES L9 TMX CDI', 0, 0, 0),
(372, '00372', 'ENGINE VALVES SPORTY L9', 0, 0, 0),
(373, '00373', 'ENGINE VALVES STOCK CG150 L9', 0, 0, 0),
(374, '00374', 'ENGINE VALVES STOCK TMX SUPREMO L9', 0, 0, 0),
(375, '00375', 'ENGINE VALVES STOCK W125 L9', 0, 0, 0),
(376, '00376', 'EOT TEMPERATURE SENSOR', 0, 0, 0),
(377, '00377', 'EXHAUST GASKET XRM MIO', 0, 0, 0),
(378, '00378', 'EXHAUST THERMAL WRAP (BLACK)', 0, 0, 0),
(379, '00379', 'EXHAUST THERMAL WRAP (TITANIUM)', 0, 0, 0),
(380, '00380', 'EXTREME BATTERY 4L', 0, 0, 0),
(381, '00381', 'EXTREME BATTERY 5L SMALL', 0, 0, 0),
(382, '00382', 'EXTREME BATTERY 5L SPORTY', 0, 0, 0),
(383, '00383', 'EXTREME BATTERY 6.5', 0, 0, 0),
(384, '00384', 'EXTREME BATTERY 7L', 0, 0, 0),
(385, '00385', 'FALCON RIM ASSORTED', 0, 0, 0),
(386, '00386', 'FAN AEROX 155 YELLOW', 0, 0, 0),
(387, '00387', 'FKI MAGNETO KIT TMX', 0, 0, 0),
(388, '00388', 'FKI MAGNETO KIT XRM', 0, 0, 0),
(389, '00389', 'FLAT WASHERS 8MM', 0, 0, 0),
(390, '00390', 'FORK OIL', 0, 0, 0),
(391, '00391', 'FORMULA CALIPER WAVE125', 0, 0, 0),
(392, '00392', 'FRONT SHOCK ASSY LIGHTEN CLICK', 0, 0, 0),
(393, '00393', 'FRONT SHOCK ASSY LIGHTEN NMAX', 0, 0, 0),
(394, '00394', 'FTC TORQUE DRIVE NUT MIO', 0, 0, 0),
(395, '00395', 'FUEL FILTER', 0, 0, 0),
(396, '00396', 'FUEL FILTER YAMAHA ', 0, 0, 0),
(397, '00397', 'FUEL HOSE', 0, 0, 0),
(398, '00398', 'RCM FUEL INJECTOR MIO 125', 0, 0, 0),
(399, '00399', 'FUEL INJECTOR YAMAHA 2PH', 0, 0, 0),
(400, '00400', 'FUEL INJECTOR YAMAHA AEROX', 0, 0, 0),
(401, '00401', 'HONDA FUEL PUMP ORING BEAT/CLICK ', 0, 0, 0),
(402, '00402', 'FULL FINGER TACTICAL GLOVES (BLACK)', 0, 0, 0),
(403, '00403', 'FUSE', 0, 0, 0),
(404, '00404', 'FUSE BOX', 0, 0, 0),
(405, '00405', 'GRILLED SIGNAL LIGHT (PLASTIC)', 0, 0, 0),
(406, '00406', 'GY6 BELT REPLACEMENT', 0, 0, 0),
(407, '00407', 'HACHI CHAINSET R150 14 41 428 X 120L', 0, 0, 0),
(408, '00408', 'HACHI CHAINSET SMASH 14 36 428 X 110L', 0, 0, 0),
(409, '00409', 'HANDLE BAR PCX 160 LOW RISE', 0, 0, 0),
(410, '00410', 'HAVOLIN GEAR OIL', 0, 0, 0),
(411, '00411', 'HAZARD SWITCH', 0, 0, 0),
(412, '00412', 'HEAD GASKET BARAKO', 0, 0, 0),
(413, '00413', 'HEAD LIGHT SOCKET WAVE XRM', 0, 0, 0),
(414, '00414', 'HEADLIGHT BULB ORDINARY', 0, 0, 0),
(415, '00415', 'HEADLIGHT SOCKET', 0, 0, 0),
(416, '00416', 'HENG DISC ABS AEROX/NMAX GOLD', 0, 0, 0),
(417, '00417', 'HERO HELMET HOOK GOLD', 0, 0, 0),
(418, '00418', 'HI TEMP KOBY GREASE', 0, 0, 0),
(419, '00419', 'HIGH AND LOW SWITCH WITH OFF', 0, 0, 0),
(420, '00420', 'HMA HEADLIGHT AND WINKER', 0, 0, 0),
(421, '00421', 'HMA SIDE MIRROR', 0, 0, 0),
(422, '00422', 'HONDA BEAT CLICK CRANKCASE BEARING', 0, 0, 0),
(423, '00423', 'HONDA CLICK RUBBER LINK', 0, 0, 0),
(424, '00424', 'HONDA COVER CENTER 80151 K81 N00ZA', 0, 0, 0),
(425, '00425', 'HONDA COVER FR LOWER 64308 K81 N00ZA', 0, 0, 0),
(426, '00426', 'HONDA COVER INNER LOWER SEAT 81141 K81 N00ZA', 0, 0, 0),
(427, '00427', 'HONDA FRONT SHOCK OIL SEAL WITH DUST SEAL', 0, 0, 0),
(428, '00428', 'HONDA GENUINE FUEL FILTER ', 0, 0, 0),
(429, '00429', 'HONDA JOINT COMP/WATER HOUSE ', 0, 0, 0),
(430, '00430', 'HONDA SPEEDOMETER SENSOR', 0, 0, 0),
(431, '00431', 'HONDA STUD BOLTS M7X17', 0, 0, 0),
(432, '00432', 'HONDA STUD BOLTS NUT M7', 0, 0, 0),
(433, '00433', 'HONEYWELL SWITCH SET', 0, 0, 0),
(434, '00434', 'HORN BUTTON', 0, 0, 0),
(435, '00435', 'HORN INNTERRUPTOR ADJUSTABLE', 0, 0, 0),
(436, '00436', 'HORN INTERUPTER RELAY POSH', 0, 0, 0),
(437, '00437', 'HORN PIPIP', 0, 0, 0),
(438, '00438', 'HORN RELAY', 0, 0, 0),
(439, '00439', 'HORN RELAY SOCKET', 0, 0, 0),
(440, '00440', 'HORN RELAY WITH LONG WIRE', 0, 0, 0),
(441, '00441', 'HORN SWITCH HONDA', 0, 0, 0),
(442, '00442', 'HUB AND MILE SPACER', 0, 0, 0),
(443, '00443', 'HYDRAULIC SWITCH', 0, 0, 0),
(444, '00444', 'HYLOS MAGS ASSORTED', 0, 0, 0),
(445, '00445', 'IDLE GEAR MIO SPORTY YAMAHA', 0, 0, 0),
(446, '00446', 'IDLE GEAR SPORTY L9', 0, 0, 0),
(447, '00447', 'INSULATOR MANIFOLD WAVE 125', 0, 0, 0),
(448, '00448', 'INSULATOR MANIFOLD XRM', 0, 0, 0),
(449, '00449', 'INTERIOR 1.85 17', 0, 0, 0),
(450, '00450', 'INTERIOR 2.25 17', 0, 0, 0),
(451, '00451', 'INTERIOR 2.50 14', 0, 0, 0),
(452, '00452', 'INTERIOR 2.75 14', 0, 0, 0),
(453, '00453', 'INTERIOR 3.00 17', 0, 0, 0),
(454, '00454', 'INTERRIOR 1.75 17', 0, 0, 0),
(455, '00455', 'INTERRIOR 2.50 17', 0, 0, 0),
(456, '00456', 'INTERRIOR 3.00 17', 0, 0, 0),
(457, '00457', 'INTERRUPTER RELAY', 0, 0, 0),
(458, '00458', 'INTERRUPTOR RELAY', 0, 0, 0),
(459, '00459', 'IRC TIRE MAXXING FRONT', 0, 0, 0),
(460, '00460', 'IRC TIRE MAXXING REAR', 0, 0, 0),
(461, '00461', 'IRC TIRES PAIR', 0, 0, 0),
(462, '00462', 'J2 PULLEY BUSHING MIO SPORTY', 0, 0, 0),
(463, '00463', 'JAPAN WIRE', 0, 0, 0),
(464, '00464', 'JRP FLAT SEAT CLICK', 0, 0, 0),
(465, '00465', 'JRP FLAT SEAT OREO M3 MIO 125', 0, 0, 0),
(466, '00466', 'JRP FLAT SEAT OREO NMAX V2', 0, 0, 0),
(467, '00467', 'JRP GRIP ORDINARY', 0, 0, 0),
(468, '00468', 'JRP GRIP ORIGINAL', 0, 0, 0),
(469, '00469', 'JRP OREO CLICK 125', 0, 0, 0),
(470, '00470', 'JRP SEAT COVER MAY TAHI ASSORTED', 0, 0, 0),
(471, '00471', 'JVT CLUTCH BELL AEROX NMAX', 0, 0, 0),
(472, '00472', 'JVT CLUTCH BELL BEAT FI', 0, 0, 0),
(473, '00473', 'JVT CLUTCH BELL CLICK/PCX', 0, 0, 0),
(474, '00474', 'JVT CLUTCH BELL MIO ', 0, 0, 0),
(475, '00475', 'JVT CLUTCH BELL MIO I125 M3', 0, 0, 0),
(476, '00476', 'JVT CLUTCH BELL NMAX/AEROX', 0, 0, 0),
(477, '00477', 'JVT CLUTCH SHOE BEAT FI', 0, 0, 0),
(478, '00478', 'JVT CLUTCH SHOE NMAX AEROX', 0, 0, 0),
(479, '00479', 'JVT CLUTCH SHOE MIO', 0, 0, 0),
(480, '00480', 'JVT CLUTCH SHOE NMAX/AEROX/CLICK', 0, 0, 0),
(481, '00481', 'JVT PIPE NMAX', 0, 0, 0),
(482, '00482', 'KEIHIN CARB TMX', 0, 0, 0),
(483, '00483', 'KEIHIN CARB XRM 125', 0, 0, 0),
(484, '00484', 'KEIHIN CARBURATOR 24MM ROUNDSLIDE', 0, 0, 0),
(485, '00485', 'KEIHIN CARBURATOR 26MM ROUND SLIDE', 0, 0, 0),
(486, '00486', 'KEIHIN CARBURATOR 28MM ROUND SLIDE', 0, 0, 0),
(487, '00487', 'KEIHIN CARBURATOR BARAKO', 0, 0, 0),
(488, '00488', 'KEIHIN FLAT SLIDE 24MM', 0, 0, 0),
(489, '00489', 'KEIHIN FLAT SLIDE 26MM', 0, 0, 0),
(490, '00490', 'KEIHIN FLAT SLIDE 28MM', 0, 0, 0),
(491, '00491', 'KEIHIN FLAT SLIDE 30MM', 0, 0, 0),
(492, '00492', 'KHC ORDINARY THROTTLE CABLE CRYPTON', 0, 0, 0),
(493, '00493', 'KHC ORDINARY THROTTLE CABLE DREAM C100', 0, 0, 0),
(494, '00494', 'KHC ORDINARY THROTTLE CABLE MIO', 0, 0, 0),
(495, '00495', 'KHC ORDINARY THROTTLE CABLE RAIDER 110', 0, 0, 0),
(496, '00496', 'KHC ORDINARY THROTTLE CABLE RAIDER 150', 0, 0, 0),
(497, '00497', 'KHC ORDINARY THROTTLE CABLE RS 100', 0, 0, 0),
(498, '00498', 'KHC ORDINARY THROTTLE CABLE SHOGUN', 0, 0, 0),
(499, '00499', 'KHC ORDINARY THROTTLE CABLE SMASH', 0, 0, 0),
(500, '00500', 'KHC ORDINARY THROTTLE CABLE STX', 0, 0, 0),
(501, '00501', 'KING DRAG BRAKE HOSE FRONT', 0, 0, 0),
(502, '00502', 'KING DRAG BRAKE HOSE REAR', 0, 0, 0),
(503, '00503', 'KING DRAG FORK ASSY CLICK/BEAT', 0, 0, 0),
(504, '00504', 'KING DRAG LIGHTEN FRONT SHOCK MIO', 0, 0, 0),
(505, '00505', 'KING DRAG LIGHTEN FRONT SHOCK WAVE 125', 0, 0, 0),
(506, '00506', 'KING DRAG LIGHTEN FRONT SHOCK WAVE125', 0, 0, 0),
(507, '00507', 'KING OF DRAG OUTER TUBE MIO', 0, 0, 0),
(508, '00508', 'KING OF DRAG OUTER TUBE W125', 0, 0, 0),
(509, '00509', 'KITACO RIGHT HANDLE SWITCH', 0, 0, 0),
(510, '00510', 'KOBY CHAIN LUBE', 0, 0, 0),
(511, '00511', 'KOBY TIRESEALANT', 0, 0, 0),
(512, '00512', 'KOYO BEARING 6000', 0, 0, 0),
(513, '00513', 'KOYO BEARING 6001', 0, 0, 0),
(514, '00514', 'KOYO BEARING 6002', 0, 0, 0),
(515, '00515', 'KOYO BEARING 6003', 0, 0, 0),
(516, '00516', 'KOYO BEARING 6004', 0, 0, 0),
(517, '00517', 'KOYO BEARING 6005', 0, 0, 0),
(518, '00518', 'KOYO BEARING 6006', 0, 0, 0),
(519, '00519', 'KOYO BEARING 62/22', 0, 0, 0),
(520, '00520', 'KOYO BEARING 6200', 0, 0, 0),
(521, '00521', 'KOYO BEARING 6201', 0, 0, 0),
(522, '00522', 'KOYO BEARING 6202', 0, 0, 0),
(523, '00523', 'KOYO BEARING 6203', 0, 0, 0),
(524, '00524', 'KOYO BEARING 6204', 0, 0, 0),
(525, '00525', 'KOYO BEARING 6205', 0, 0, 0),
(526, '00526', 'KOYO BEARING 6206', 0, 0, 0),
(527, '00527', 'KOYO BEARING 63/22', 0, 0, 0),
(528, '00528', 'KOYO BEARING 6300', 0, 0, 0),
(529, '00529', 'KOYO BEARING 6301', 0, 0, 0),
(530, '00530', 'KOYO BEARING 6302', 0, 0, 0),
(531, '00531', 'KOYO BEARING 6303', 0, 0, 0),
(532, '00532', 'KOYO BEARING 6304', 0, 0, 0),
(533, '00533', 'KOYO BEARING 6305', 0, 0, 0),
(534, '00534', 'KOYO BEARING 6322', 0, 0, 0),
(535, '00535', 'KOYO BEARING 638', 0, 0, 0),
(536, '00536', 'KOYO BEARING 6905', 0, 0, 0),
(537, '00537', 'KRAYON ORDINARY THROTTLE CABLE BARAKO', 0, 0, 0),
(538, '00538', 'KRAYON ORDINARY THROTTLE CABLE WAVE 100', 0, 0, 0),
(539, '00539', 'KRYON HORN ORDINARY', 0, 0, 0),
(540, '00540', 'KTD HEADLIGHT', 0, 0, 0),
(541, '00541', 'KYB PREMIUM SHOCK 318MM', 0, 0, 0),
(542, '00542', 'L9 BACK PLATE MIO I 125 M3', 0, 0, 0),
(543, '00543', 'L9 BACKPLATE BEAT CARB', 0, 0, 0),
(544, '00544', 'L9 BACKPLATE BEAT FI', 0, 0, 0),
(545, '00545', 'L9 BACKPLATE CLICK 125', 0, 0, 0),
(546, '00546', 'L9 BACKPLATE GY6', 0, 0, 0),
(547, '00547', 'L9 BACKPLATE M3', 0, 0, 0),
(548, '00548', 'L9 BACKPLATE MX CARB', 0, 0, 0),
(549, '00549', 'L9 BACKPLATE NMAX', 0, 0, 0),
(550, '00550', 'L9 BACKPLATE SKYDRIVE', 0, 0, 0),
(551, '00551', 'L9 BACKPLATE SPORTY', 0, 0, 0),
(552, '00552', 'L9 CAMSHAFT BEARING BEAT CARB BEAT FI', 0, 0, 0),
(553, '00553', 'L9 CAMSHAFT BEARING MIO I125', 0, 0, 0),
(554, '00554', 'L9 CAMSHAFT BEARING NMAX AEROX ', 0, 0, 0),
(555, '00555', 'L9 CAMSHAFT BEARING SPORTY', 0, 0, 0),
(556, '00556', 'L9 CAMSHAFT BEARING WAVE 125 WAVE 100', 0, 0, 0),
(557, '00557', 'L9 CAMSHAFT MIO I125 M3 STD', 0, 0, 0),
(558, '00558', 'L9 CAMSHAFT MIO SPORTY STD', 0, 0, 0),
(559, '00559', 'L9 CAMSHAFT NMAX STD', 0, 0, 0),
(560, '00560', 'L9 CAMSHAFT SMASH STD', 0, 0, 0),
(561, '00561', 'L9 CAMSHAFT SPORTY STD', 0, 0, 0),
(562, '00562', 'L9 CAMSHAFT STD BEAT CARB STD', 0, 0, 0),
(563, '00563', 'L9 CAMSHAFT STD BEAT FI STD', 0, 0, 0),
(564, '00564', 'L9 CAMSHAFT WAVE 125 STD', 0, 0, 0),
(565, '00565', 'L9 CAMSHAFT XRM110 STD', 0, 0, 0),
(566, '00566', 'L9 CLUTCH CABLE RAIDER 150', 0, 0, 0),
(567, '00567', 'L9 CLUTCH SHOE MIO SPORTY', 0, 0, 0),
(568, '00568', 'L9 CLUTCH SHOE NMAX/AEROX', 0, 0, 0),
(569, '00569', 'L9 DRAIN PLUG MIO SPORTY', 0, 0, 0),
(570, '00570', 'L9 ENGINE VALVE MIO', 0, 0, 0),
(571, '00571', 'L9 ENGINE VALVES BARAKO ', 0, 0, 0),
(572, '00572', 'L9 ENGINE VALVES CG 125', 0, 0, 0),
(573, '00573', 'L9 ENGINE VALVES CG 150', 0, 0, 0),
(574, '00574', 'L9 ENGINE VALVES CT 100', 0, 0, 0),
(575, '00575', 'L9 ENGINE VALVES GY6 150', 0, 0, 0),
(576, '00576', 'L9 ENGINE VALVES MIO SPORTY', 0, 0, 0),
(577, '00577', 'L9 ENGINE VALVES SMASH', 0, 0, 0),
(578, '00578', 'L9 ENGINE VALVES STX 125', 0, 0, 0),
(579, '00579', 'L9 ENGINE VALVES TMX CDI', 0, 0, 0),
(580, '00580', 'L9 ENGINE VALVES TMX SUPREMO', 0, 0, 0),
(581, '00581', 'L9 ENGINE VALVES WAVE 125', 0, 0, 0),
(582, '00582', 'L9 ENGINE VALVES XRM 110', 0, 0, 0),
(583, '00583', 'L9 GEAR BOX MIO SPORTY', 0, 0, 0),
(584, '00584', 'L9 IDLE GEAR MIO SPORTY', 0, 0, 0),
(585, '00585', 'L9 IDLE GEAR SPORTY', 0, 0, 0),
(586, '00586', 'L9 INJECTOR BEAT FI', 0, 0, 0),
(587, '00587', 'L9 INJECTOR CLICK', 0, 0, 0),
(588, '00588', 'L9 INJECTOR MIO I 125', 0, 0, 0),
(589, '00589', 'L9 INJECTOR NMAX', 0, 0, 0),
(590, '00590', 'L9 KICK GEAR PAMAYPAY MIO SPORTY', 0, 0, 0),
(591, '00591', 'L9 MAGNETO COIL MIO', 0, 0, 0),
(592, '00592', 'L9 MANIFOLD MIO SPORTY', 0, 0, 0),
(593, '00593', 'L9 PISTON KIT SPORTY 50MM STD', 0, 0, 0),
(594, '00594', 'L9 PISTON KIT SPORTY 59MM', 0, 0, 0),
(595, '00595', 'L9 REGULATOR 5 WIRES FULLWAVE', 0, 0, 0),
(596, '00596', 'L9 SPEEDOMETER GEAR BOX', 0, 0, 0),
(597, '00597', 'L9 SPORTY MANIFOLD', 0, 0, 0),
(598, '00598', 'L9 STARTER BIDS MIO SPORTY', 0, 0, 0),
(599, '00599', 'L9 STARTER 500CC MIO', 0, 0, 0),
(600, '00600', 'L9 STARTER BIDS XRM', 0, 0, 0),
(601, '00601', 'L9 TORQUE DRIVE MIO I 125', 0, 0, 0),
(602, '00602', 'L9 TORQUE DRIVE MIO SPORTY', 0, 0, 0),
(603, '00603', 'LAMBORG9 TORQUE DRIVE BEAT FI', 0, 0, 0),
(604, '00604', 'LAMBORG9 TORQUE DRIVE MIO I 125', 0, 0, 0),
(605, '00605', 'LAMBORG9 TORQUE DRIVE MIO SPORTY', 0, 0, 0),
(606, '00606', 'LAMBORG9 VALVE GUIDE GY6 125', 0, 0, 0),
(607, '00607', 'LAMBORG9 VALVE GUIDE MIO', 0, 0, 0),
(608, '00608', 'LAMBORG9 VALVE GUIDE R. 150', 0, 0, 0),
(609, '00609', 'LAMBORG9 VALVE GUIDE SHOGUN', 0, 0, 0),
(610, '00610', 'LAMBORG9 VALVE GUIDE SMASH', 0, 0, 0),
(611, '00611', 'LAMBORG9 VALVE GUIDE TMX', 0, 0, 0),
(612, '00612', 'LAMBORG9 VALVE GUIDE XRM', 0, 0, 0),
(613, '00613', 'LAMBORGE9 BRAKE PAD 3093', 0, 0, 0),
(614, '00614', 'LAMBORGE9 BRAKE PAD DORAEMON', 0, 0, 0),
(615, '00615', 'LAMBORGE9 BRAKE PAD TR 3099', 0, 0, 0),
(616, '00616', 'LAMBORGE9 BRAKE PAD TR 3136', 0, 0, 0),
(617, '00617', 'LAMBORGE9 BRAKE SHOE BEAT CLICK', 0, 0, 0),
(618, '00618', 'LAMBORGE9 ENGINE VALVES ASSORTED', 0, 0, 0),
(619, '00619', 'LAMBORGE9 HUB N MILE WAVE', 0, 0, 0),
(620, '00620', 'LAMBORGE9 IDLE GEAR MIO', 0, 0, 0),
(621, '00621', 'LAMBORGE9 IGNITION COIL', 0, 0, 0),
(622, '00622', 'LAMBORGE9 KICK GEAR PAMAYPAY', 0, 0, 0),
(623, '00623', 'LAMBORGE9 REAR SHOCK 270MM NMAX AEROX', 0, 0, 0),
(624, '00624', 'LAMBORGE9 REGULATOR MIO', 0, 0, 0),
(625, '00625', 'LAMBORGE9 STARTER VENDIX ASSY FURY', 0, 0, 0),
(626, '00626', 'LAMBORGE9 STARTER VENDIX ASSY MIO I 125 M3', 0, 0, 0),
(627, '00627', 'LAMBORGE9 STARTER VENDIX ASSY MIO SPORTY', 0, 0, 0),
(628, '00628', 'LAMBORGE9 STARTER VENDIX ASSY RAIDER 150', 0, 0, 0),
(629, '00629', 'LAMBORGE9 TORQUE DRIVE MIO', 0, 0, 0),
(630, '00630', 'LAMBORGE9 TORQUE DRIVE MIO I 125 M3', 0, 0, 0),
(631, '00631', 'LHK CAMSHAFT MIO SPORTY STAGE1', 0, 0, 0),
(632, '00632', 'LHK CAMSHAFT MIO SPORTY STAGE2', 0, 0, 0),
(633, '00633', 'LHK CAMSHAFT MIO SPORTY STAGE3', 0, 0, 0),
(634, '00634', 'LHK CAMSHAFT WAVE 125 STAGE1', 0, 0, 0),
(635, '00635', 'LHK CAMSHAFT WAVE 125 STAGE2', 0, 0, 0),
(636, '00636', 'LHK CAMSHAFT WAVE 125 STAGE3', 0, 0, 0),
(637, '00637', 'LIGHTEN DISC RAIDER CARB PAIR', 0, 0, 0),
(638, '00638', 'LIGHTEN DISC RAIDER FI PAIR', 0, 0, 0),
(639, '00639', 'LIGHTEN DISC WAVE125', 0, 0, 0),
(640, '00640', 'LIGHTEN FRONT SHOCK ASSY CLICK', 0, 0, 0),
(641, '00641', 'LIGHTEN FRONT SHOCK ASSY NMAX', 0, 0, 0),
(642, '00642', 'LIGHTEN FRONT SHOCK MIO', 0, 0, 0),
(643, '00643', 'LIGHTEN FRONT SHOCK WAVE 125', 0, 0, 0),
(644, '00644', 'HERB X ANGEL MAGIC GATAS', 0, 0, 0),
(645, '00645', 'MAGIC STAR MINI DRIVING LIGHT V6', 0, 0, 0),
(646, '00646', 'MAGIC WASHER ', 0, 0, 0),
(647, '00647', 'MAKOTO BRAKE CABLE MIO', 0, 0, 0),
(648, '00648', 'MAKOTO BRAKE SHOE NXT SKYDRIVE FI', 0, 0, 0),
(649, '00649', 'MAKOTO BRAKE SHOE SPORTY', 0, 0, 0),
(650, '00650', 'MANIFOLD XRM 24MM', 0, 0, 0),
(651, '00651', 'MANUAL TENSIONER AEROX', 0, 0, 0),
(652, '00652', 'MANUAL TENSIONER MIO', 0, 0, 0),
(653, '00653', 'MANUAL TENSIONER NMAX/AEROX', 0, 0, 0),
(654, '00654', 'MANUAL TENSIONER RAIDER 150', 0, 0, 0),
(655, '00655', 'MANUAL TENSIONER WAVE125', 0, 0, 0),
(656, '00656', 'MASTER REPAIR KIT MIO', 0, 0, 0),
(657, '00657', 'MASTER REPAIR KIT P19', 0, 0, 0),
(658, '00658', 'MASTER REPAIR KIT XRM', 0, 0, 0),
(659, '00659', 'MAX SPEED WAVE125 HEADNUT', 0, 0, 0),
(660, '00660', 'MDL BRACKET', 0, 0, 0),
(661, '00661', 'MHR CENTER SPRING BEAT FI', 0, 0, 0),
(662, '00662', 'MHR CENTER SPRING CLICK', 0, 0, 0),
(663, '00663', 'MHR CENTER SPRING MIO M3', 0, 0, 0),
(664, '00664', 'MHR CENTER SPRING NMAX', 0, 0, 0),
(665, '00665', 'MICHELIN 70*90-14', 0, 0, 0),
(666, '00666', 'MICHELIN 80*80-14', 0, 0, 0),
(667, '00667', 'MIO AMORE FAIRINGS ASSORTED', 0, 0, 0),
(668, '00668', 'MIO 1 FAIRINGS ASSORTED', 0, 0, 0),
(669, '00669', 'MIO I 125 REGULATOR YAMAHA ORIGINAL', 0, 0, 0),
(670, '00670', 'MIO SPORTY ROLLER ROCKER ARM', 0, 0, 0),
(671, '00671', 'MIO SPORTY ROLLER TYPE ROCKER ARM L9', 0, 0, 0),
(672, '00672', 'MOKOTO HEADLIGHT 6 LED', 0, 0, 0),
(673, '00673', 'MOKOTO TRI LED', 0, 0, 0),
(674, '00674', 'MORIN DISC SMASH110 FRONT 220MM', 0, 0, 0),
(675, '00675', 'MOTOR TECH SPEEDOMETER GEAR BOX', 0, 0, 0),
(676, '00676', 'MOTORTECH BRAKE PAD M3', 0, 0, 0),
(677, '00677', 'MOTUL OIL 3000PLUS 1L', 0, 0, 0),
(678, '00678', 'MOTUL OIL SCOOTER 800ML', 0, 0, 0),
(679, '00679', 'MOTUL OIL SCOOTER 1L', 0, 0, 0),
(680, '00680', 'MP BLOCK WAVE 125 57MM', 0, 0, 0),
(681, '00681', 'MRP TAPPET COVER WAVE125', 0, 0, 0),
(682, '00682', 'MRP TAPPET COVER YAMAHA', 0, 0, 0),
(683, '00683', 'MSD CABLE', 0, 0, 0),
(684, '00684', 'MSD IGNITION COIL SET', 0, 0, 0),
(685, '00685', 'MTK BLOCK XRM110 53MM', 0, 0, 0),
(686, '00686', 'MTR BRAKE SHOE BEAT', 0, 0, 0),
(687, '00687', 'MTR BRAKE SHOE CLICK', 0, 0, 0),
(688, '00688', 'MTR HANDLE SWITCH MIO', 0, 0, 0),
(689, '00689', 'MTR IGNITION SWITCH SET ANTI THEFT MIO', 0, 0, 0),
(690, '00690', 'MTRT FLYBALL MIO125', 0, 0, 0),
(691, '00691', 'MTRT FLYBALL BEATFI', 0, 0, 0),
(692, '00692', 'MTRT 5TURNS VALVESPRING MIO', 0, 0, 0),
(693, '00693', 'MTRT FLYBALL SPORTY', 0, 0, 0),
(694, '00694', 'MTRT ADV150/PCX FLYBALL', 0, 0, 0),
(695, '00695', 'MTRT ALLOY BLOCK DOME PISTON 59MM MIO SPORTY', 0, 0, 0),
(696, '00696', 'MTRT BELL AEROX155/NMAX155', 0, 0, 0),
(697, '00697', 'MTRT BELL GY6/CLICK125', 0, 0, 0),
(698, '00698', 'MTRT BIGVALVES HEAD  MIO SPORTY 24/28', 0, 0, 0),
(699, '00699', 'MTRT BLOCK ALLOY BORE 59MM DOME PISTON', 0, 0, 0),
(700, '00700', 'MTRT BLOCK MIO 115 59MM ALLOYBORE', 0, 0, 0),
(701, '00701', 'MTRT BLOCK MIO 115 63MM CHROMEBORE', 0, 0, 0),
(702, '00702', 'MTRT BLOCK NMAX 63.5 CHROMEBORE', 0, 0, 0),
(703, '00703', 'MTRT CAMSHAFT STG 2 NMAXV1', 0, 0, 0),
(704, '00704', 'MTRT CENTER SPRING NMAX/AEROX', 0, 0, 0),
(705, '00705', 'MTRT CENTER SPRING SPORTY', 0, 0, 0),
(706, '00706', 'MTRT CENTER SPRING CLICK', 0, 0, 0),
(707, '00707', 'MTRT CENTER SPRING MIO I 125', 0, 0, 0),
(708, '00708', 'MTRT CLUTCH SPRING BEAT FI', 0, 0, 0),
(709, '00709', 'MTRT CLUTCH SPRING GY6', 0, 0, 0),
(710, '00710', 'MTRT CLUTCH SPRING M3', 0, 0, 0),
(711, '00711', 'MTRT CLUTCH SPRING NMAX', 0, 0, 0),
(712, '00712', 'MTRT CLUTCH SPRING SPORTY', 0, 0, 0),
(713, '00713', 'MTRT CYLINDER BLOCK 63MM CHROMEBORE BLANK PISTON', 0, 0, 0),
(714, '00714', 'MTRT CYLINDER HEAD 29.5 25', 0, 0, 0),
(715, '00715', 'MTRT CYLINDER HEAD 31.5 26', 0, 0, 0),
(716, '00716', 'MTRT DIO/BEAT FLYBALL', 0, 0, 0),
(717, '00717', 'MTRT FLYBALL  MIO125', 0, 0, 0),
(718, '00718', 'MTRT FLYBALL BEAT FI', 0, 0, 0),
(719, '00719', 'MTRT FLYBALL CLICK', 0, 0, 0),
(720, '00720', 'MTRT GY6/BEAT FI FLYBALL', 0, 0, 0),
(721, '00721', 'MTRT HEAD 2428 SPORTY', 0, 0, 0),
(722, '00722', 'MTRT INJECTOR 130CC MIOI', 0, 0, 0),
(723, '00723', 'MTRT MIO/SOULI115/NOUVO FLYBALL', 0, 0, 0),
(724, '00724', 'MTRT NEEDLE BEARING LC150', 0, 0, 0),
(725, '00725', 'MTRT NEEDLE BEARING MIO', 0, 0, 0),
(726, '00726', 'MTRT FLYBALL NMAX/AEROX/MIO125', 0, 0, 0),
(727, '00727', 'MTRT PRIMARY GEAR 16T', 0, 0, 0),
(728, '00728', 'MTRT PULLEY SET NMAX 155', 0, 0, 0),
(729, '00729', 'MTRT PULLEY SET SPORTY V2', 0, 0, 0),
(730, '00730', 'MTRT ROCKER ARM MIO', 0, 0, 0),
(731, '00731', 'MTRT SILENT KILLER CLICK', 0, 0, 0),
(732, '00732', 'MTRT TPS LC150', 0, 0, 0),
(733, '00733', 'MTRT TPS M3/MIO 125', 0, 0, 0),
(734, '00734', 'MTRT VALVE SHIM MB0390 4.5MM', 0, 0, 0),
(735, '00735', 'MTRT VALVE SPRING 5 TURNS MIO', 0, 0, 0),
(736, '00736', 'MTRT XMAX300 FLYBALL', 0, 0, 0),
(737, '00737', 'MTV MIO FLAT SEAT BLUE', 0, 0, 0),
(738, '00738', 'MTV R150 FLAT SEAT RED', 0, 0, 0),
(739, '00739', 'MTV RAIDER FI FLAT SEAT BLACK', 0, 0, 0),
(740, '00740', 'MUSHROOM FILTER', 0, 0, 0),
(741, '00741', 'NATHONG FLAT SEAT BEAT FI BLACK', 0, 0, 0),
(742, '00742', 'NATHONG FLAT SEAT CLICK', 0, 0, 0),
(743, '00743', 'NATHONG FLAT SEAT CLICK WHITE LOGO', 0, 0, 0),
(744, '00744', 'NLK OPEN PIPE', 0, 0, 0),
(745, '00745', 'NMAX BUTA SET', 0, 0, 0),
(746, '00746', 'NMAX V2 CLEAR VISOR', 0, 0, 0),
(747, '00747', 'NMAX V2 SIDE POCKET', 0, 0, 0),
(748, '00748', 'NON VENTILATED DISC', 0, 0, 0),
(749, '00749', 'NTN BEARING 6200', 0, 0, 0),
(750, '00750', 'NUT M10', 0, 0, 0),
(751, '00751', 'NUT M12', 0, 0, 0),
(752, '00752', 'NUT M14', 0, 0, 0),
(753, '00753', 'NUT M17', 0, 0, 0),
(754, '00754', 'NUT M4', 0, 0, 0),
(755, '00755', 'NUT M5', 0, 0, 0),
(756, '00756', 'NUT M6', 0, 0, 0),
(757, '00757', 'NUT M8', 0, 0, 0),
(758, '00758', 'OD 12N5L BATTERY SMALL', 0, 0, 0),
(759, '00759', 'OD BATTERY 12N6.5L', 0, 0, 0),
(760, '00760', 'OD BATTERY 5L SPORTY', 0, 0, 0),
(761, '00761', 'OD BATTERY YTX 4L', 0, 0, 0),
(762, '00762', 'OEM YAMAHA QUICK THROTTLE', 0, 0, 0),
(763, '00763', 'OHLINS REAR SHOCK 300MM', 0, 0, 0),
(764, '00764', 'OHLINS STABILIZER', 0, 0, 0),
(765, '00765', 'OIL FILTER  BAJAJ', 0, 0, 0),
(766, '00766', 'OIL FILTER  KAWASAKI', 0, 0, 0),
(767, '00767', 'OIL FILTER  YAMAHA', 0, 0, 0),
(768, '00768', 'OIL FILTER RAIDER SUZUKI', 0, 0, 0),
(769, '00769', 'OIL FILTER SHOGUN SUZUKI', 0, 0, 0),
(770, '00770', 'ON OFF SWITCH W WIRE', 0, 0, 0),
(771, '00771', 'OSRAM LED HEADLIGHT', 0, 0, 0),
(772, '00772', 'P19 BRAKE MASTER ', 0, 0, 0),
(773, '00773', 'P19 BREMBO DUAL TANK', 0, 0, 0),
(774, '00774', 'P19 BREMBO SINGLE TANK', 0, 0, 0),
(775, '00775', 'PARK LIGHT SUPER SPEED', 0, 0, 0),
(776, '00776', 'PDD STARTER RELAY MIO I 125 ', 0, 0, 0),
(777, '00777', 'PDD STATOR WAVE 125', 0, 0, 0),
(778, '00778', 'PDD YAMAKOTO STAINLESS STEEL SPOKES', 0, 0, 0),
(779, '00779', 'PEANUT BULB', 0, 0, 0),
(780, '00780', 'PETRON OIL 0.8ML', 0, 0, 0),
(781, '00781', 'PETRON OIL/GEAL OIL', 0, 0, 0),
(782, '00782', 'PIAA HORN GOLD QR', 0, 0, 0),
(783, '00783', 'PIAA SILVER QR', 0, 0, 0),
(784, '00784', 'PIONEER GASKET MAKER RED', 0, 0, 0),
(785, '00785', 'PITO TUBELESS', 0, 0, 0),
(786, '00786', 'PITSBIKE 5 TURNS VALVESPRING MIO', 0, 0, 0),
(787, '00787', 'PITSBIKE BLOCK MIO I 125 STEELBORE 59MM', 0, 0, 0),
(788, '00788', 'PITSBIKE CDI MIO SPORTY', 0, 0, 0),
(789, '00789', 'PITSBIKE CDI RAIDER CARB REBORN', 0, 0, 0),
(790, '00790', 'PITSBIKE CLUTCH DUMPER SNIPER', 0, 0, 0),
(791, '00791', 'PITSBIKE CRANK PIN 2.0MM', 0, 0, 0),
(792, '00792', 'PITSBIKE CRANK PIN 3.0MM', 0, 0, 0),
(793, '00793', 'PITSBIKE CRANKSHAFT BEARING SIDE BEARING', 0, 0, 0),
(794, '00794', 'PITSBIKE OVAL VALVE SPRING AEROX', 0, 0, 0),
(795, '00795', 'PITSBIKE PROGRAMABLE CDI MIO', 0, 0, 0),
(796, '00796', 'PITSBIKE VALVE SPRING AEROX R1', 0, 0, 0),
(797, '00797', 'PITSBIKE VALVES 24 28 MIO', 0, 0, 0),
(798, '00798', 'PLATINUM 200ML 2T', 0, 0, 0),
(799, '00799', 'PLATINUM OIL 200ML PAMBANTO', 0, 0, 0),
(800, '00800', 'PLATINUM XRACE 800ML', 0, 0, 0),
(801, '00801', 'PODIUM CLICK 125/150', 0, 0, 0),
(802, '00802', 'PODIUM MIO I 125', 0, 0, 0),
(803, '00803', 'PODIUM NMAX V2/AEROX V2', 0, 0, 0),
(804, '00804', 'PODIUM PIPE BEAT', 0, 0, 0),
(805, '00805', 'PODIUM PIPE MIO I 125', 0, 0, 0),
(806, '00806', 'PODIUM PIPE MIO SPORTY', 0, 0, 0),
(807, '00807', 'PODIUM PIPE RAIDER150', 0, 0, 0),
(808, '00808', 'PODIUM PIPE SOULTY', 0, 0, 0),
(809, '00809', 'PODIUM WAVE 100', 0, 0, 0),
(810, '00810', 'PODIUM WAVE 125', 0, 0, 0),
(811, '00811', 'POWER COOLANT TOP1', 0, 0, 0),
(812, '00812', 'POWER MAX BRT CDI MIO', 0, 0, 0),
(813, '00813', 'POWER TIRE 60 80 17', 0, 0, 0),
(814, '00814', 'POWER TIRE 80  90 17', 0, 0, 0),
(815, '00815', 'POWER TIRE 80 90 14', 0, 0, 0),
(816, '00816', 'POWER TIRE 90 80 17', 0, 0, 0),
(817, '00817', 'POWER TIRE 90 90 14', 0, 0, 0),
(818, '00818', 'PRC COLORED CHAIN', 0, 0, 0),
(819, '00819', 'PS16 BRAKE MASTER', 0, 0, 0),
(820, '00820', 'PS16 REPAIR KIT', 0, 0, 0),
(821, '00821', 'PS16 RIGHT', 0, 0, 0),
(822, '00822', 'PULLEY OIL SEAL NMAX', 0, 0, 0),
(823, '00823', 'PULLEY OILSEAL YAMAHA MIO I 125', 0, 0, 0),
(824, '00824', 'PULLEY SET BEAT CARB M POWER', 0, 0, 0),
(825, '00825', 'PULLEY SET BEAT FI M POWER', 0, 0, 0),
(826, '00826', 'PULLEY SET CLICK M POWER', 0, 0, 0),
(827, '00827', 'PULLEY SET MIO L9', 0, 0, 0),
(828, '00828', 'PULLEY WASHER 1MM ', 0, 0, 0),
(829, '00829', 'QUICK RELEASE', 0, 0, 0),
(830, '00830', 'QUICK TIRE 45 90 17', 0, 0, 0),
(831, '00831', 'RACING FAN MIO MTRT', 0, 0, 0),
(832, '00832', 'RACING MONKEY BLOCK CHROMEBORE 59MM MIO', 0, 0, 0),
(833, '00833', 'RACING SEATCOVER MEDIUM', 0, 0, 0),
(834, '00834', 'RACING SEATCOVER SMALL', 0, 0, 0),
(835, '00835', 'RADIATOR COVER', 0, 0, 0),
(836, '00836', 'RAM AIR', 0, 0, 0),
(837, '00837', 'RCM 5 TURNS VALVESPRING', 0, 0, 0),
(838, '00838', 'RCM 8MM NUT UNIVERSAL', 0, 0, 0),
(839, '00839', 'RCM ADJUSTABLE CAMGEAR NMAX', 0, 0, 0),
(840, '00840', 'RCM ADJUSTABLE CAMGEAR SPORTY', 0, 0, 0),
(841, '00841', 'RCM ADJUSTABLE CAMGEAR WAVE 100', 0, 0, 0),
(842, '00842', 'RCM AEROX 155 ISC MANUAL', 0, 0, 0),
(843, '00843', 'RCM BALL RACE BEARING AEROX', 0, 0, 0),
(844, '00844', 'RCM BALL RACE BEARING LC150', 0, 0, 0),
(845, '00845', 'RCM BALL RACE BEARING R150', 0, 0, 0),
(846, '00846', 'RCM BELL NUT NMAX', 0, 0, 0),
(847, '00847', 'RCM BELL NUT SPORTY', 0, 0, 0),
(848, '00848', 'RCM BLOCK CHROMEBORE 59MM DOME PISTON', 0, 0, 0),
(849, '00849', 'RCM BUNG CENSOR RACING MONKEY', 0, 0, 0),
(850, '00850', 'RCM CENTER SPRING BEAT', 0, 0, 0),
(851, '00851', 'RCM CENTER SPRING NMAX/AEROX', 0, 0, 0),
(852, '00852', 'RCM CENTER SPRING SPORTY', 0, 0, 0),
(853, '00853', 'RCM CENTER SPRING CLICK', 0, 0, 0),
(854, '00854', 'RCM CENTER SPRING BEAT FI', 0, 0, 0),
(855, '00855', 'RCM CENTER SPRING MIO I 125 M3 ', 0, 0, 0),
(856, '00856', 'RCM CENTER SPRING MIO 125 BEAT CDI MXI', 0, 0, 0),
(857, '00857', 'RCM CENTER SPRING SKYDRIVE', 0, 0, 0),
(858, '00858', 'RCM CENTER SPRING XMAX300', 0, 0, 0),
(859, '00859', 'RCM CKP SENSOR BEAT CLICK', 0, 0, 0),
(860, '00860', 'RCM CLUTCH LINING ASSY. LIGHTEN COPPER MIO', 0, 0, 0),
(861, '00861', 'RCM CLUTCH LINING PAD ONLY MIO SPORTY', 0, 0, 0),
(862, '00862', 'RCM CLUTCH SPRING MIO', 0, 0, 0),
(863, '00863', 'RCM CLUTCH SPRING MIO I 125 M3', 0, 0, 0),
(864, '00864', 'RCM CRANK SHAFT OIL SEAL MIO', 0, 0, 0),
(865, '00865', 'RCM CTUCH SPRING 1500RPM MIO I', 0, 0, 0),
(866, '00866', 'RCM ENGINE VALVE MIO', 0, 0, 0),
(867, '00867', 'RCM ENGINE VALVE RAIDER150', 0, 0, 0),
(868, '00868', 'RCM ENGINE VALVE WAVE125', 0, 0, 0),
(869, '00869', 'RCM FLY BALL BEAT FI/GY6', 0, 0, 0),
(870, '00870', 'RCM FLYBALL BEAT', 0, 0, 0),
(871, '00871', 'RCM FLYBALL NMAX/AEROX/M3', 0, 0, 0),
(872, '00872', 'RCM FLYBALL  NEXT ADDRESS', 0, 0, 0),
(873, '00873', 'RCM FLYBALL SPORTY', 0, 0, 0),
(874, '00874', 'RCM FLYBALL CLICK/SKYDRIVE', 0, 0, 0),
(875, '00875', 'RCM FUEL FILTER BEAT FI', 0, 0, 0),
(876, '00876', 'RCM FUEL FILTER HONDA BEAT FI', 0, 0, 0),
(877, '00877', 'RCM GASKET ALLOY MIO', 0, 0, 0),
(878, '00878', 'RCM IDLE GEAR MIO', 0, 0, 0),
(879, '00879', 'RCM LIGHTEN GEAR MIO', 0, 0, 0),
(880, '00880', 'RCM LIGHTEN VALVE MIO SPORTY STD', 0, 0, 0),
(881, '00881', 'RCM MANUAL ISC NMAX AEROX', 0, 0, 0),
(882, '00882', 'RCM OIL PUMP GEAR MIO SPORTY', 0, 0, 0),
(883, '00883', 'RCM PIN DOWELL YAMAHA', 0, 0, 0),
(884, '00884', 'RCM PULLEY SET NMAX', 0, 0, 0),
(885, '00885', 'RCM PUMP GEAR MIO', 0, 0, 0),
(886, '00886', 'RCM RACING CDI RAIDER 150 REBORN', 0, 0, 0),
(887, '00887', 'RCM ROCKER ARM MIO SPORTY', 0, 0, 0),
(888, '00888', 'RCM ROCKER ARM NMAX', 0, 0, 0),
(889, '00889', 'RCM ROCKER ARM WAVE 125', 0, 0, 0),
(890, '00890', 'RCM SPORTY ROCKER ARM', 0, 0, 0),
(891, '00891', 'RCM STAR WASHER DRIVEFACE SPORTY', 0, 0, 0),
(892, '00892', 'RCM VALVE RETAINER MIO 4.5MM', 0, 0, 0),
(893, '00893', 'RCM VALVE RETAINER TITANIUM 4.5MM MIO', 0, 0, 0),
(894, '00894', 'RCM VALVE RETAINER TITANIUM 5.0MM MIO', 0, 0, 0),
(895, '00895', 'RCM VALVE SPRING 5 TURNS', 0, 0, 0),
(896, '00896', 'RCM WAVE 125 ROCKER ARM', 0, 0, 0),
(897, '00897', 'REDSPEED DEGREASER', 0, 0, 0),
(898, '00898', 'REPAIR KIT HONDA', 0, 0, 0),
(899, '00899', 'REPAIR KIT HONDA CLICK', 0, 0, 0),
(900, '00900', 'REPAIR KIT MASTER HONDA', 0, 0, 0),
(901, '00901', 'REPAIR KIT MASTER MIO', 0, 0, 0),
(902, '00902', 'REPAIR KIT YAMAHA', 0, 0, 0),
(903, '00903', 'RESHINGU CAMSHAFT 6.0MM MIO', 0, 0, 0),
(904, '00904', 'RETRO BRAKE LIGHT LED', 0, 0, 0),
(905, '00905', 'RIGHT BRAKE LIGHT SWITCH', 0, 0, 0),
(906, '00906', 'RKI MAGNETO KIT TMX', 0, 0, 0),
(907, '00907', 'RKI MAGNETO KIT XRM', 0, 0, 0),
(908, '00908', 'YAMAHA ROCKER ARM MIO GENUINE', 0, 0, 0),
(909, '00909', 'ROTATOR', 0, 0, 0),
(910, '00910', 'RS8 BELL PCX ', 0, 0, 0),
(911, '00911', 'RS8 CENTER SPRING BEAT FI', 0, 0, 0),
(912, '00912', 'RS8 CENTER SPRING MIO SPORTY', 0, 0, 0),
(913, '00913', 'RS8 CLUTCH ASSY PCX', 0, 0, 0),
(914, '00914', 'RS8 CLUTCH BELL CLICK', 0, 0, 0),
(915, '00915', 'RS8 CLUTCH BELL MIO SPORTY', 0, 0, 0),
(916, '00916', 'RS8 CLUTCH SPRING NMAX AEROX MIO I 125', 0, 0, 0),
(917, '00917', 'RS8 CLUTCH SPRING NMAX/AEROX', 0, 0, 0),
(918, '00918', 'RS8 CVT CLEANER', 0, 0, 0),
(919, '00919', 'RS8 GEAR OIL', 0, 0, 0),
(920, '00920', 'RS8 MAGIC ALL IN ONE', 0, 0, 0),
(921, '00921', 'RS8 OIL ECO SCOOTER 800ML', 0, 0, 0),
(922, '00922', 'RS8 OIL ECOLINE 1L', 0, 0, 0),
(923, '00923', 'RS8 OIL R9  0.8', 0, 0, 0),
(924, '00924', 'RS8 OIL R9 1L', 0, 0, 0),
(925, '00925', 'RS8 PULLEY SET BEAT FI', 0, 0, 0),
(926, '00926', 'RS8 PULLEY SET CLICK', 0, 0, 0),
(927, '00927', 'RS8 PULLEY SET MIO I 125 M3', 0, 0, 0),
(928, '00928', 'RS8 PULLEY SET MIO SPORTY', 0, 0, 0),
(929, '00929', 'RS8 PULLEY SET PCX 150 ADV', 0, 0, 0),
(930, '00930', 'RTD LED HEADLIGHT', 0, 0, 0),
(931, '00931', 'RUDDER TIRE 60*80-17', 0, 0, 0),
(932, '00932', 'RUDDER TIRE 60*90-17', 0, 0, 0),
(933, '00933', 'RUDDER TIRE 70*80-17', 0, 0, 0),
(934, '00934', 'RUDDER TIRE 80*80-17', 0, 0, 0),
(935, '00935', 'SC SIAM AXLE GEARS FRONT CNC AEROX', 0, 0, 0),
(936, '00936', 'SC SIAM AXLE GEARS FRONT CNC CLICK', 0, 0, 0),
(937, '00937', 'SC SIAM AXLE GEARS FRONT CNC NMAX', 0, 0, 0),
(938, '00938', 'SC SIAM AXLE GEARS FRONT CNC PCX', 0, 0, 0),
(939, '00939', 'SC SIAM AXLE GEARS FRONT CNC RAIDER/WAVE', 0, 0, 0),
(940, '00940', 'SC SIAM AXLE MIO', 0, 0, 0),
(941, '00941', 'SC SIAM FRONT AXLE NMAX', 0, 0, 0),
(942, '00942', 'SC SIAM OIL BOLT CNC YAMAHA', 0, 0, 0),
(943, '00943', 'SC SIAM OIL BOLT GEAR SPIRAL CNC', 0, 0, 0),
(944, '00944', 'SEAT COVER ORDINARY', 0, 0, 0),
(945, '00945', 'SEAT LOCK ASSY M3/SOUL i125', 0, 0, 0),
(946, '00946', 'SENSOR ASSY OIL TEMPERATURE BEAT FI HONDA', 0, 0, 0),
(947, '00947', 'SGP CLUTCH CABLE RAIDER 150', 0, 0, 0),
(948, '00948', 'SHARK KING FOOTREST', 0, 0, 0),
(949, '00949', 'SHELL ADVANCE 800ML WITH GEAR OIL', 0, 0, 0),
(950, '00950', 'SHELL ADVANCE AX3 20W 40 1L', 0, 0, 0),
(951, '00951', 'SHELL ADVANCE AX3 20W 40 800ML', 0, 0, 0),
(952, '00952', 'SHELL ADVANCE AX5 15W 40 1L', 0, 0, 0),
(953, '00953', 'SHELL ADVANCE AX5 15W 40 800ML', 0, 0, 0),
(954, '00954', 'SHELL ADVANCE AX7 10W 40 1L', 0, 0, 0),
(955, '00955', 'SHELL ADVANCE AX7 10W 40 800ML', 0, 0, 0),
(956, '00956', 'SHELL ADVANCE WITH GEAROIL', 0, 0, 0),
(957, '00957', 'SHOWAR CENTER SPRING', 0, 0, 0),
(958, '00958', 'SHOWAR STARTER RELAY BEAT FI', 0, 0, 0),
(959, '00959', 'SHOWAR STARTER RELAY CLICK125/150', 0, 0, 0),
(960, '00960', 'SHOWAR TAPPET SCREW', 0, 0, 0),
(961, '00961', 'SHOWAR TORQUE DRIVE PULLEY ONLY MIO', 0, 0, 0),
(962, '00962', 'SIGNAL LIGHT BULB ORDINARY', 0, 0, 0),
(963, '00963', 'SIGNAL LIGHT SOCKET', 0, 0, 0),
(964, '00964', 'SIGNAL LIGHT SWITCH WITH HAZZARD', 0, 0, 0),
(965, '00965', 'SLIDER HONDA BEAT', 0, 0, 0),
(966, '00966', 'SLIDER HONDA CLICK', 0, 0, 0),
(967, '00967', 'SLIDER PIECE  YAMAHA', 0, 0, 0),
(968, '00968', 'SNIPER 155R KAHA', 0, 0, 0),
(969, '00969', 'SPARK PLUG 6899 CPR6EA-9 ', 0, 0, 0),
(970, '00970', 'SPARK PLUG BP7HS', 0, 0, 0),
(971, '00971', 'SPARK PLUG C7HSA', 0, 0, 0),
(972, '00972', 'SPARK PLUG CAP L TYPE', 0, 0, 0),
(973, '00973', 'SPARK PLUG CAP MIO', 0, 0, 0),
(974, '00974', 'SPARK PLUG D6HA', 0, 0, 0),
(975, '00975', 'SPARK PLUG D6HS', 0, 0, 0),
(976, '00976', 'SPARK PLUG D8EA', 0, 0, 0),
(977, '00977', 'SPARKPLUG CAP MIO', 0, 0, 0),
(978, '00978', 'SPARKPLUG CAP R150', 0, 0, 0),
(979, '00979', 'SPEED TUNER OIL 10W40 800ML', 0, 0, 0),
(980, '00980', 'SPEED TUNER OIL 20W50 800ML', 0, 0, 0),
(981, '00981', 'SPEED TUNER SUPER OIL 10W 40 1L', 0, 0, 0),
(982, '00982', 'SPEED TUNER SUPER OIL 20W 50 1L', 0, 0, 0),
(983, '00983', 'SPEEDOMETER CABLE NOUVO MIO', 0, 0, 0),
(984, '00984', 'SPEEDOMETER CENSOR BEAT', 0, 0, 0),
(985, '00985', 'SPEEDOMETER GEAR BOX MIO', 0, 0, 0),
(986, '00986', 'SPEEDOMETER SENSOR BEAT FI', 0, 0, 0),
(987, '00987', 'SPORTY CENTER SPRING  ORIG', 0, 0, 0),
(988, '00988', 'SPORTY DOMINO LEVER BLACK', 0, 0, 0),
(989, '00989', 'SPORTY DOMINO LEVER CARBON', 0, 0, 0),
(990, '00990', 'SPORTY DOMINO LEVER GOLD', 0, 0, 0),
(991, '00991', 'SPYKER FOOTREST UNIVERSAL ', 0, 0, 0),
(992, '00992', 'STARTER BIDS REPAIR KIT XRM', 0, 0, 0),
(993, '00993', 'STARTER MOTOR M3', 0, 0, 0);
INSERT INTO `products` (`barcode`, `product_id`, `productname`, `dealer_price`, `supplier_price`, `srp`) VALUES
(994, '00994', 'STARTER RELAY M3', 0, 0, 0),
(995, '00995', 'SUN BELL MIO SOUL I 125', 0, 0, 0),
(996, '00996', 'SUN CLUTCH ASSY MIO I 125', 0, 0, 0),
(997, '00997', 'SUN CLUTCH ASSY NMAX AEROX', 0, 0, 0),
(998, '00998', 'SUN CLUTCH SHOE LINING W/ SPRING BEAT', 0, 0, 0),
(999, '00999', 'SUN CLUTCH SHOE LINING W/ SPRING SPORTY', 0, 0, 0),
(1000, '001000', 'SUN CLUTCH SHOE ONLY NMAX', 0, 0, 0),
(1001, '001001', 'SUN PULLEY SET CLICK 125', 0, 0, 0),
(1002, '001002', 'SUN PULLEY SET CLICK 150', 0, 0, 0),
(1003, '001003', 'SUN PULLEY SET MIO I 125', 0, 0, 0),
(1004, '001004', 'SUN PULLEY SET NMAX AEROX', 0, 0, 0),
(1005, '001005', 'SUN PULLEY SET PCX 160', 0, 0, 0),
(1006, '001006', 'SUN PULLEY SET SPORTY', 0, 0, 0),
(1007, '001007', 'SUPER DRAG RIM', 0, 0, 0),
(1008, '001008', 'SUPER SPEED BRAKE HOSE', 0, 0, 0),
(1009, '001009', 'SUPER SPEED ENGINE SUPPORT MIO +2.5', 0, 0, 0),
(1010, '001010', 'SUPER SPEED GEAR OIL', 0, 0, 0),
(1011, '001011', 'SUPER SPEED PIPE BEAT FI', 0, 0, 0),
(1012, '001012', 'SUPER SPEED PIPE CLICK', 0, 0, 0),
(1013, '001013', 'SUPER SPEED PIPE MIO I 125', 0, 0, 0),
(1014, '001014', 'SUPER SPEED PIPE MIO SOULTY', 0, 0, 0),
(1015, '001015', 'SUPER SPEED PIPE SPORTY', 0, 0, 0),
(1016, '001016', 'SUPER SPEED SHOCK CLICK 330MM', 0, 0, 0),
(1017, '001017', 'SUPER SPEED SHOCK MIO 300MM', 0, 0, 0),
(1018, '001018', 'SUPER SPEED SHOCK WAVE', 0, 0, 0),
(1019, '001019', 'SUPERSPEED SWING ARM R150', 0, 0, 0),
(1020, '001020', 'SUPERSPEED SWING ARM WAVE', 0, 0, 0),
(1021, '001021', 'SURE BRAKEFLUID', 0, 0, 0),
(1022, '001022', 'SUZUKI SKYDRIVE BELT GENUINE', 0, 0, 0),
(1023, '001023', 'SWING ARM BUSHING MIO', 0, 0, 0),
(1024, '001024', 'SWITCH CLICK PASSING / HAZZARD', 0, 0, 0),
(1025, '001025', 'SWITS BRAKE HOSE FRONT', 0, 0, 0),
(1026, '001026', 'SWITS HOSE REAR', 0, 0, 0),
(1027, '001027', 'T10 SIGNAL LIGHT BULB', 0, 0, 0),
(1028, '001028', 'T15 PARK LIGHT', 0, 0, 0),
(1029, '001029', 'TAIL LIGHT BULB', 0, 0, 0),
(1030, '001030', 'TDD HEADLIGHT', 0, 0, 0),
(1031, '001031', 'TDR BRAKE HOSE ABS NMAX', 0, 0, 0),
(1032, '001032', 'TDR BRAKE HOSE PCX CBS', 0, 0, 0),
(1033, '001033', 'THROTLE HOUSING MIO', 0, 0, 0),
(1034, '001034', 'THROTLLE CABLE MIO', 0, 0, 0),
(1035, '001035', 'THROTTLE BODY CLEANER BG.', 0, 0, 0),
(1036, '001036', 'THROTTLE BODY MTRT AEROX155', 0, 0, 0),
(1037, '001037', 'THROTTLE BODY MTRT MIO 125', 0, 0, 0),
(1038, '001038', 'THROTTLE BODY MTRT NMAX155', 0, 0, 0),
(1039, '001039', 'THROTTLE CABLE DIO RACING', 0, 0, 0),
(1040, '001040', 'THROTTLE CABLE MIO SPORTY STOCK', 0, 0, 0),
(1041, '001041', 'THROTTLE HOUSING MIO', 0, 0, 0),
(1042, '001042', 'THROTTLE TUBE', 0, 0, 0),
(1043, '001043', 'TIMING CHAIN SET W/ROLLER C100', 0, 0, 0),
(1044, '001044', 'TIMING CHAIN SET W/ROLLER XRM', 0, 0, 0),
(1045, '001045', 'TIRE SEALANT KOBY', 0, 0, 0),
(1046, '001046', 'TMX UNIVERSAL BIG BUSHING SHOCK', 0, 0, 0),
(1047, '001047', 'TOP 1 COOLANT 500ML', 0, 0, 0),
(1048, '001048', 'TPS HONDA PCX', 0, 0, 0),
(1049, '001049', 'TPS SENSOR BEAT CLICK', 0, 0, 0),
(1050, '001050', 'TPS YAMAHA ', 0, 0, 0),
(1051, '001051', 'TRANSFORMER MASTER X TWM', 0, 0, 0),
(1052, '001052', 'TRANSFORMER x KHOKEN BRAKE', 0, 0, 0),
(1053, '001053', 'TRC SWING ARM SNIPER', 0, 0, 0),
(1054, '001054', 'TRI SWITCH BEAT', 0, 0, 0),
(1055, '001055', 'TTGR ANTI THEFT IGNITION KEY BEAT FI', 0, 0, 0),
(1056, '001056', 'TTGR ANTI THEFT IGNITION KEY CLICK 125', 0, 0, 0),
(1057, '001057', 'TTGR ANTI THEFT IGNITION KEY MIO I 125 M3', 0, 0, 0),
(1058, '001058', 'TTGR ANTI THEFT IGNITION KEY MIO SOUL', 0, 0, 0),
(1059, '001059', 'TTGR ANTI THEFT IGNITION KEY MIO SPORTY', 0, 0, 0),
(1060, '001060', 'TTGR ANTI THEFT IGNITION KEY RAIDER 150 CARB', 0, 0, 0),
(1061, '001061', 'TTGR ANTI THEFT IGNITION KEY WAVE 125', 0, 0, 0),
(1062, '001062', 'TTGR HUB SET MIO SOUL I 125 M3', 0, 0, 0),
(1063, '001063', 'TTGR HUB SET XRM', 0, 0, 0),
(1064, '001064', 'TURTLE BACK CALIPER W125 W/ CNC BRACKET', 0, 0, 0),
(1065, '001065', 'TURTLEBACK CALIPER', 0, 0, 0),
(1066, '001066', 'TWM LEVER LEFT', 0, 0, 0),
(1067, '001067', 'UNIVERSAL SPARK PLUG CAP', 0, 0, 0),
(1068, '001068', 'UNIVERSAL SPRING CENTER STAND', 0, 0, 0),
(1069, '001069', 'UNIVERSAL THROTTLE CABLE ', 0, 0, 0),
(1070, '001070', 'VALVE RETAINER MTRT', 0, 0, 0),
(1071, '001071', 'VALVESEAL HONDA', 0, 0, 0),
(1072, '001072', 'VALVESEAL YAMAHA SPORTY', 0, 0, 0),
(1073, '001073', 'VANZ BOY', 0, 0, 0),
(1074, '001074', 'VENTILATED DISC', 0, 0, 0),
(1075, '001075', 'VOYAGER OIL 1L', 0, 0, 0),
(1076, '001076', 'VS1 120ML', 0, 0, 0),
(1077, '001077', 'WATERPROOF CASE FOR IPHONE HOLDER', 0, 0, 0),
(1078, '001078', 'WATERPUPM ASSY CLICK/PCX', 0, 0, 0),
(1079, '001079', 'WAVE 125 LIGHTEN DISC', 0, 0, 0),
(1080, '001080', 'WD40 ', 0, 0, 0),
(1081, '001081', 'WYD CYLINDER HEAD BIGVALVES 24 28 WAVE125', 0, 0, 0),
(1082, '001082', 'XRM UNIVERSAL SMALL BUSHING SHOCK', 0, 0, 0),
(1083, '001083', 'YAGUSO RIOS', 0, 0, 0),
(1084, '001084', 'YAMAHA 2DP HEAD GASKET NMAX', 0, 0, 0),
(1085, '001085', 'YAMAHA 2PH BELL NUT', 0, 0, 0),
(1086, '001086', 'YAMAHA 2PH TENSIONER', 0, 0, 0),
(1087, '001087', 'YAMAHA 2PV HEAD GASKET SNIPER', 0, 0, 0),
(1088, '001088', 'YAMAHA 44D BELT MX 125', 0, 0, 0),
(1089, '001089', 'YAMAHA 44D HEAD GASKET MXI 125', 0, 0, 0),
(1090, '001090', 'YAMAHA 5MX UPPER CHAIN GUIDE', 0, 0, 0),
(1091, '001091', 'YAMAHA 5TL F3108-00 EMBLEM', 0, 0, 0),
(1092, '001092', 'YAMAHA 5TL IDLE GEAR', 0, 0, 0),
(1093, '001093', 'YAMAHA 5TL TORQUE DRIVE NUT', 0, 0, 0),
(1094, '001094', 'YAMAHA BRAKE LEVER SET MIO I 125 BLACK', 0, 0, 0),
(1095, '001095', 'YAMAHA CKP SENSOR', 0, 0, 0),
(1096, '001096', 'YAMAHA CONNECTING ROD ORIGINAL STD MIO SPORTY 4D0', 0, 0, 0),
(1097, '001097', 'YAMAHA GEAR OIL', 0, 0, 0),
(1098, '001098', 'YAMAHA GENUINE FUEL FILTER', 0, 0, 0),
(1099, '001099', 'YAMAHA MIO BELL NUT', 0, 0, 0),
(1100, '001100', 'YAMAHA MIO1 DIBDIB', 0, 0, 0),
(1101, '001101', 'YAMAHA OEM QUICK THROTTLE', 0, 0, 0),
(1102, '001102', 'YAMAHA OILSEAL PULLEY SIDE MIO I 125 M3', 0, 0, 0),
(1103, '001103', 'YAMAHA OILSEAL PULLEY SIDE NMAX', 0, 0, 0),
(1104, '001104', 'YAMAHA PULLEY 2DP', 0, 0, 0),
(1105, '001105', 'YAMAHA PULLEY OIL SEAL MIO I 125', 0, 0, 0),
(1106, '001106', 'YAMAHA SLIDER PIECE', 0, 0, 0),
(1107, '001107', 'YAMAHA TIMING CHAIN SPORTY 90L', 0, 0, 0),
(1108, '001108', 'YAMAHA VALVESEAL', 0, 0, 0),
(1109, '001109', 'YAMAKOTO SIDE MIRROR', 0, 0, 0),
(1110, '001110', 'YAMAKOTO SPEEDOMETER CABLE MIO', 0, 0, 0),
(1111, '001111', 'YAMALUBE OIL 1L BLUE CORE AT', 0, 0, 0),
(1112, '001112', 'YAMALUBE OIL 800ML BLUE CORE AT', 0, 0, 0),
(1113, '001113', 'YMC GRASA', 0, 0, 0),
(1114, '001114', 'YTX4L-BS XTREME BATTERY', 0, 0, 0),
(1115, '001115', 'YTX7L-BS XTREME BATTERY', 0, 0, 0),
(1116, '001116', 'Z-5 CAMSHAFT STAGE 1', 0, 0, 0),
(1117, '001117', 'Z5 CAMSHAFT 5.5MM MIO', 0, 0, 0),
(1118, '001118', 'ZENO FLAT SEAT ADV 150', 0, 0, 0),
(1119, '001119', 'ZENO FLAT SEAT AEROX V1', 0, 0, 0),
(1120, '001120', 'ZENO FLAT SEAT AEROX V2', 0, 0, 0),
(1121, '001121', 'ZENO FLAT SEAT BEAT FI', 0, 0, 0),
(1122, '001122', 'ZENO FLAT SEAT CLICK 125', 0, 0, 0),
(1123, '001123', 'ZENO FLAT SEAT CLICK 160', 0, 0, 0),
(1124, '001124', 'ZENO FLAT SEAT MIO GT 125', 0, 0, 0),
(1125, '001125', 'ZENO FLAT SEAT MIO I 125', 0, 0, 0),
(1126, '001126', 'ZENO FLAT SEAT MIO SOULTY', 0, 0, 0),
(1127, '001127', 'ZENO FLAT SEAT MIO SPORTY', 0, 0, 0),
(1128, '001128', 'ZENO FLAT SEAT NMAX V2', 0, 0, 0),
(1129, '001129', 'ZENO FLAT SEAT PCX 160', 0, 0, 0),
(1130, '001130', 'ZENO FLAT SEAT RAIDER 150 CARB', 0, 0, 0),
(1131, '001131', 'ZENO FLAT SEAT RAIDER FI', 0, 0, 0),
(1132, '001132', 'ZENO FLAT SEAT WAVE 100', 0, 0, 0),
(1133, '001133', 'ZENO FLAT SEAT WAVE 125I', 0, 0, 0),
(1134, '001134', 'ZENO FLAT SEAT WAVE 125R', 0, 0, 0),
(1135, '001135', 'ZENO FLATSEAT MSI GT ', 0, 0, 0),
(1136, '001136', 'ZENO FLATSEAT WAVE100', 0, 0, 0),
(1137, '001137', 'ZENO PIPE AEROX V1', 0, 0, 0),
(1138, '001138', 'ZENO PIPE CLICK 125', 0, 0, 0),
(1139, '001139', 'ZENO PIPE MIO I 125', 0, 0, 0),
(1140, '001140', 'ZENO PIPE MIO SOULTY', 0, 0, 0),
(1141, '001141', 'ZENO PIPE MIO SPORTY', 0, 0, 0),
(1142, '001142', 'ZENO PIPE NMAX V1', 0, 0, 0),
(1143, '001143', 'ZENO PIPE NMAX V2', 0, 0, 0),
(1144, '001144', 'ZENO PIPE PCX 160', 0, 0, 0),
(1145, '001145', 'ZENO PIPE R150 CARB', 0, 0, 0),
(1146, '001146', 'ZENO PIPE WAVE 110', 0, 0, 0),
(1147, '001147', 'ZENO PIPE WAVE 125', 0, 0, 0),
(1148, '001148', 'ZENO PIPE WAVE100', 0, 0, 0),
(1149, '001149', 'ZENO SWING ARM WAVE +2', 0, 0, 0),
(1150, '001150', 'ZENO OIL 800ML CLUTCH EXCEL RED', 0, 0, 0),
(1151, '001151', 'ZENO OIL 1L CLUTCH EXCEL RED', 0, 0, 0),
(1152, '001152', 'ZENO OIL 800ML SCOOTER PRO BLUE', 0, 0, 0),
(1153, '001153', 'ZENO OIL 1L SCOOTER PRO BLUE', 0, 0, 0),
(1154, '001154', 'ZIC M9 FULLY SENTETIC', 0, 0, 0),
(1155, '001155', 'BEAST TIRE 130X70X13', 0, 0, 0),
(1156, '001156', 'BEAST TIRE 110X80X14', 0, 0, 0),
(1157, '001157', 'BEAST TIRE 110X70X13', 0, 0, 0),
(1158, '001158', 'BEAST TIRE 140X70X14', 0, 0, 0),
(1159, '001159', 'BEAST TIRE 60X80X14 4PR FLASH P6240 TL', 0, 0, 0),
(1160, '001160', 'BEAST TIRE 70X80X14 4PR FLASH P6240 TL', 0, 0, 0),
(1161, '001161', 'BEAST TIRE 80X80X14 4PR FLASH P6240 TL', 0, 0, 0),
(1162, '001162', 'BEAST TIRE 90X90X14 4PR FLASH P6240 TL', 0, 0, 0),
(1163, '001163', 'BEAST TIRE 90X90X17 4PR FLASH P6240 TL', 0, 0, 0),
(1164, '001164', 'BEAST TIRE 80X80X17 4PR FLASH P6240 TL', 0, 0, 0),
(1165, '001165', 'BEAST TIRE 80X90X14 4PR FLASH P6240 TL', 0, 0, 0),
(1166, '001166', 'BEAST TIRE 70X80X17 4PR FLASH P6240 TL', 0, 0, 0),
(1167, '001167', 'SAIYAN BRAKE SHOE XRM/WAVE REAR', 0, 0, 0),
(1168, '001168', 'SAIYAN BRAKE SHOE TMX125 ALPHA', 0, 0, 0),
(1169, '001169', 'SAIYAN BRAKE SHOE TMX155 REAR', 0, 0, 0),
(1170, '001170', 'SAIYAN BRAKE SHOE BARAKO REAR', 0, 0, 0),
(1171, '001171', 'SAIYAN BRAKE SHOE SKYDRIVE', 0, 0, 0),
(1172, '001172', 'SAIYAN BRAKE PAD RAIDER150 FRONT', 0, 0, 0),
(1173, '001173', 'SAIYAN BRAKE PAD RAIDER150 REAR', 0, 0, 0),
(1174, '001174', 'SAIYAN BRAKE PAD MIO', 0, 0, 0),
(1175, '001175', 'SAIYAN BRAKE PAD M3', 0, 0, 0),
(1176, '001176', 'SAIYAN BRAKE PAD RAIDER150 FI REAR', 0, 0, 0),
(1177, '001177', 'SAIYAN BRAKE PAD WAVE125', 0, 0, 0),
(1178, '001178', 'SAIYAN BRAKE PAD PCX160/ADV150 REAR', 0, 0, 0),
(1179, '001179', 'SAIYAN BRAKE PAD PCX160/ADV150 FRONT', 0, 0, 0),
(1180, '001180', 'SAIYAN BRAKE PAD CLICK125', 0, 0, 0),
(1181, '001181', 'SAIYAN BRAKE PAD WAVE110', 0, 0, 0),
(1182, '001182', 'SAIYAN BRAKE PAD TRINITY', 0, 0, 0),
(1183, '001183', 'SAIYAN BRAKE PAD RAIDER150 FI FRONT', 0, 0, 0),
(1184, '001184', 'SAIYAN BRAKE PAD BEAT NEW', 0, 0, 0),
(1185, '001185', 'SAIYAN MINI DRIVING LIGHT SS3', 0, 0, 0),
(1186, '001186', 'SAIYAN MINI DRIVING LIGHT SS4', 0, 0, 0),
(1187, '001187', 'SAIYAN CHANGE PEDAL CT100', 0, 0, 0),
(1188, '001188', 'SAIYAN CHANGE PEDAL C100/DREAM', 0, 0, 0),
(1189, '001189', 'SAIYAN CHANGE PEDAL WAVE110', 0, 0, 0),
(1190, '001190', 'SAIYAN CHANGE PEDAL TMX155', 0, 0, 0),
(1191, '001191', 'SAIYAN CHANGE PEDAL HD3', 0, 0, 0),
(1192, '001192', 'SAIYAN CHANGE PEDAL TC125', 0, 0, 0),
(1193, '001193', 'SAIYAN CHANGE PEDAL SUPREMO', 0, 0, 0),
(1194, '001194', 'SAIYAN CHANGE PEDAL WAVE125', 0, 0, 0),
(1195, '001195', 'SAIYAN CHANGE PEDAL RS100', 0, 0, 0),
(1196, '001196', 'SAIYAN CHANGE PEDAL SHOGUN', 0, 0, 0),
(1197, '001197', 'SAIYAN CHANGE PEDAL BARAKO', 0, 0, 0),
(1198, '001198', 'SAIYAN CHANGE PEDAL XRM110', 0, 0, 0),
(1199, '001199', 'SAIYAN CLUTCH LINING CB125', 0, 0, 0),
(1200, '001200', 'SAIYAN CLUTCH LINING CT100', 0, 0, 0),
(1201, '001201', 'SAIYAN CLUTCH LINING FZ16', 0, 0, 0),
(1202, '001202', 'SAIYAN CLUTCH LINING ROUSER135', 0, 0, 0),
(1203, '001203', 'SAIYAN CLUTCH LINING RUSI/TC150', 0, 0, 0),
(1204, '001204', 'SAIYAN CLUTCH LINING SMASH', 0, 0, 0),
(1205, '001205', 'SAIYAN CLUTCH LINING TMX', 0, 0, 0),
(1206, '001206', 'SAIYAN CLUTCH LINING WAVE125', 0, 0, 0),
(1207, '001207', 'SAIYAN CLUTCH LINING RAIDER150', 0, 0, 0),
(1208, '001208', 'SAIYAN CLUTCH LINING BARAKO', 0, 0, 0),
(1209, '001209', 'SAIYAN CLUTCH LINING HD3', 0, 0, 0),
(1210, '001210', 'SAIYAN CLUTCH LINING CT150', 0, 0, 0),
(1211, '001211', 'SAIYAN CLUTCH LINING XRM110', 0, 0, 0),
(1212, '001212', 'SAIYAN CDI LIFAN110', 0, 0, 0),
(1213, '001213', 'SAIYAN CDI TMX SUPREMO', 0, 0, 0),
(1214, '001214', 'SAIYAN STEEL RIM 1.4X17', 0, 0, 0),
(1215, '001215', 'SAIYAN STEEL RIM 1.6X17', 0, 0, 0),
(1216, '001216', 'SAIYAN THROTTLE CABLE BARAKO', 0, 0, 0),
(1217, '001217', 'SAIYAN THROTTLE CABLE CG125', 0, 0, 0),
(1218, '001218', 'SAIYAN THROTTLE CABLE CT125', 0, 0, 0),
(1219, '001219', 'SAIYAN THROTTLE CABLE XRM125', 0, 0, 0),
(1220, '001220', 'SAIYAN PULLEY SET ASSY W/ DRIVE FACE ZOOMER-X/BEAT FI', 0, 0, 0),
(1221, '001221', 'SAIYAN PULLEY SET ASSY W/ DRIVE FACE GY6', 0, 0, 0),
(1222, '001222', 'SAIYAN PULLEY SET ASSY W/ DRIVE FACE MIO I 125/MIO SOUL I 125', 0, 0, 0),
(1223, '001223', 'SAIYAN PULLEY SET ASSY W/ DRIVE FACE SKYDRIVE', 0, 0, 0),
(1224, '001224', 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING BEAT SCOOPY', 0, 0, 0),
(1225, '001225', 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING CLICK150', 0, 0, 0),
(1226, '001226', 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING MIO I 125/MIO SOUL I 125', 0, 0, 0),
(1227, '001227', 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING MIO SOUL I 115', 0, 0, 0),
(1228, '001228', 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING ZOOMER-X/BEAT FI', 0, 0, 0),
(1229, '001229', 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING SKYDRIVE', 0, 0, 0),
(1230, '001230', 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING GY6', 0, 0, 0),
(1231, '001231', 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING NMAX', 0, 0, 0),
(1232, '001232', 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING PCX2020 (K97)', 0, 0, 0),
(1233, '001233', 'SAIYAN CLUTCH SHOE ASSY BEAT SCOOPY', 0, 0, 0),
(1234, '001234', 'SAIYAN CLUTCH SHOE ASSY CLICK125', 0, 0, 0),
(1235, '001235', 'SAIYAN CLUTCH SHOE ASSY CLICK150', 0, 0, 0),
(1236, '001236', 'SAIYAN CLUTCH SHOE ASSY MIO I 125/MIO SOUL I 125', 0, 0, 0),
(1237, '001237', 'SAIYAN CLUTCH SHOE ASSY MIO SOUL I 115', 0, 0, 0),
(1238, '001238', 'SAIYAN CLUTCH SHOE ASSY ZOOMER-X/BEAT FI', 0, 0, 0),
(1239, '001239', 'SAIYAN CLUTCH SHOE ASSY GY6', 0, 0, 0),
(1240, '001240', 'SAIYAN CLUTCH SHOE ASSY NMAX', 0, 0, 0),
(1241, '001241', 'SAIYAN CLUTCH SHOE ASSY PCX2020 (K97)', 0, 0, 0),
(1242, '001242', 'SAIYAN PULLEY SLIDER PIECE MIO', 0, 0, 0),
(1243, '001243', 'SAIYAN PULLEY SLIDER PIECE BEAT SCOOPY', 0, 0, 0),
(1244, '001244', 'SAIYAN PULLEY SLIDER PIECE CLICK125', 0, 0, 0),
(1245, '001245', 'SAIYAN PULLEY SLIDER PIECE CLICK150', 0, 0, 0),
(1246, '001246', 'SAIYAN PULLEY SLIDER PIECE GY6', 0, 0, 0),
(1247, '001247', 'SAIYAN PULLEY SLIDER PIECE NMAX', 0, 0, 0),
(1248, '001248', 'SAIYAN PULLEY SLIDER PIECE SKYDRIVE', 0, 0, 0),
(1249, '001249', 'SAIYAN MAIN SWITCH WAVE125', 0, 0, 0),
(1250, '001250', 'SAIYAN MAIN SWITCH SMASH', 0, 0, 0),
(1251, '001251', 'SAIYAN MAIN SWITCH CT100', 0, 0, 0),
(1252, '001252', 'SAIYAN MAIN SWITCH HD3', 0, 0, 0),
(1253, '001253', 'SAIYAN MAIN SWITCH MIO SPORTY/MIO OLD', 0, 0, 0),
(1254, '001254', 'SAIYAN MAIN SWITCH TMX SUPREMO', 0, 0, 0),
(1255, '001255', 'SAIYAN MAIN SWITCH C100/DREAM', 0, 0, 0),
(1256, '001256', 'SAIYAN MAIN SWITCH SNIPER135', 0, 0, 0),
(1257, '001257', 'SAIYAN MAIN SWITCH CT150 BOXER', 0, 0, 0),
(1258, '001258', 'SAIYAN MAIN SWITCH TMX155', 0, 0, 0),
(1259, '001259', 'SAIYAN IGNITION SWITCH SET MIO SOUL (LOCK SET)', 0, 0, 0),
(1260, '001260', 'SAIYAN IGNITION SWITCH SET SKYDRIVE (LOCK SET)', 0, 0, 0),
(1261, '001261', 'SAIYAN IGNITION SWITCH SET WAVE100R (LOCK SET)', 0, 0, 0),
(1262, '001262', 'SAIYAN IGNITION SWITCH SET WAVE125 (LOCK SET)', 0, 0, 0),
(1263, '001263', 'SAIYAN IGNITION SWITCH SET XRM TRINITY (LOCK SET)', 0, 0, 0),
(1264, '001264', 'SAIYAN IGNITION SWITCH SET XRM110 (LOCK SET)', 0, 0, 0),
(1265, '001265', 'SAIYAN IGNITION SWITCH SET XRM125 (LOCK SET)', 0, 0, 0),
(1266, '001266', 'SAIYAN ANTI THEFT SWITCH SET MIO SPORTY', 0, 0, 0),
(1267, '001267', 'SAIYAN ANTI THEFT SWITCH SET RAIDER CARB', 0, 0, 0),
(1268, '001268', 'SAIYAN ANTI THEFT SWITCH SET SMASH110', 0, 0, 0),
(1269, '001269', 'SAIYAN ANTI THEFT SWITCH SET SMASH115 (DISC)', 0, 0, 0),
(1270, '001270', 'SAIYAN ANTI THEFT SWITCH SET BEAT', 0, 0, 0),
(1271, '001271', 'SAIYAN PULLEY SET W/ PIN (NO DRIVE FACE) MIO', 0, 0, 0),
(1272, '001272', 'SAIYAN PULLEY SET W/ PIN (NO DRIVE FACE) CLICK125', 0, 0, 0),
(1273, '001273', 'SAIYAN PULLEY SET W/ PIN (NO DRIVE FACE) CLICK150', 0, 0, 0),
(1274, '001274', 'SAIYAN PULLEY SET W/ PIN (NO DRIVE FACE) MIO I 125/MIO SOUL I 125', 0, 0, 0),
(1275, '001275', 'SAIYAN PULLEY SET W/ PIN (NO DRIVE FACE) GY6', 0, 0, 0),
(1276, '001276', 'SAIYAN DRIVE FACE MIO', 0, 0, 0),
(1277, '001277', 'SAIYAN DRIVE FACE CLICK125', 0, 0, 0),
(1278, '001278', 'SAIYAN DRIVE FACE CLICK150', 0, 0, 0),
(1279, '001279', 'SAIYAN TORQUE DRIVE BEAT SCOOPY', 0, 0, 0),
(1280, '001280', 'SAIYAN TORQUE DRIVE MIO I 125/MIO SOUL I 125', 0, 0, 0),
(1281, '001281', 'SAIYAN TORQUE DRIVE MIO SOUL I 115', 0, 0, 0),
(1282, '001282', 'SAIYAN TORQUE DRIVE GY6', 0, 0, 0),
(1283, '001283', 'SAIYAN TORQUE DRIVE NMAX', 0, 0, 0),
(1284, '001284', 'SAIYAN TORQUE DRIVE PCX2020 (K97)', 0, 0, 0),
(1285, '001285', 'SAIYAN CLUTCH LINING LIFAN110', 0, 0, 0),
(1286, '001286', 'SAIYAN DISC NMAX FRONT', 0, 0, 0),
(1287, '001287', 'SAIYAN DISC NMAX REAR', 0, 0, 0),
(1288, '001288', 'SAIYAN DISC M3', 0, 0, 0),
(1289, '001289', 'SAIYAN DISC CLICK150', 0, 0, 0),
(1290, '001290', 'SAIYAN DISC MIO 190MM', 0, 0, 0),
(1291, '001291', 'SAIYAN DISC MIO 200MM', 0, 0, 0),
(1292, '001292', 'SAIYAN DISC BEAT', 0, 0, 0),
(1293, '001293', 'SAIYAN AIR FILTER BEAT', 0, 0, 0),
(1294, '001294', 'SAIYAN AIR FILTER BURGMAN', 0, 0, 0),
(1295, '001295', 'SAIYAN AIR FILTER TMX125 ALPHA', 0, 0, 0),
(1296, '001296', 'SAIYAN AIR FILTER TMX155 FOAM', 0, 0, 0),
(1297, '001297', 'SAIYAN AIR FILTER CLICK125/150', 0, 0, 0),
(1298, '001298', 'SAIYAN FLASHER RELAY W/OUT SOUND', 0, 0, 0),
(1299, '001299', 'SAIYAN FLASHER RELAY W/ ADJUSTER', 0, 0, 0),
(1300, '001300', 'SAIYAN HORN RELAY 4PIN', 0, 0, 0),
(1301, '001301', 'SAIYAN HORN RELAY 5PIN', 0, 0, 0),
(1302, '001302', 'SAIYAN ROCKER ARM M3', 0, 0, 0),
(1303, '001303', 'SAIYAN ROCKER ARM GY6125', 0, 0, 0),
(1304, '001304', 'SAIYAN ROCKER ARM MIO', 0, 0, 0),
(1305, '001305', 'SAIYAN ROCKER ARM CT100', 0, 0, 0),
(1306, '001306', 'SAIYAN ROCKER ARM TMX155 CAM FOLLOWER', 0, 0, 0),
(1307, '001307', 'SAIYAN ROCKER ARM TMX125 ALPHA', 0, 0, 0),
(1308, '001308', 'SAIYAN ROCKER ARM BARAKO', 0, 0, 0),
(1309, '001309', 'SAIYAN ROCKER ARM TMX SUPREMO', 0, 0, 0),
(1310, '001310', 'SAIYAN ROCKER ARM TMX155 UPPER', 0, 0, 0),
(1311, '001311', 'SAIYAN STATOR COIL TMX ALPHA', 0, 0, 0),
(1312, '001312', 'SAIYAN STARTER RELAY RAIDER150', 0, 0, 0),
(1313, '001313', 'SAIYAN STARTER RELAY TMX', 0, 0, 0),
(1314, '001314', 'SAIYAN STARTER RELAY CLICK 4PIN', 0, 0, 0),
(1315, '001315', 'SAIYAN KICK STARTER BARAKO', 0, 0, 0),
(1316, '001316', 'SAIYAN KICK STARTER TMX155', 0, 0, 0),
(1317, '001317', 'SAIYAN KICK STARTER WAVE125', 0, 0, 0),
(1318, '001318', 'SAIYAN KICK STARTER XRM110', 0, 0, 0),
(1319, '001319', 'SAIYAN KICK STARTER TMX ALPHA', 0, 0, 0),
(1320, '001320', 'SAIYAN HANDLE SWITCH ASSY MIO', 0, 0, 0),
(1321, '001321', 'SAIYAN HANDLE SWITCH ASSY TMX155', 0, 0, 0),
(1322, '001322', 'SAIYAN HANDLE SWITCH ASSY TMX ALPHA', 0, 0, 0),
(1323, '001323', 'SAIYAN HANDLE SWITCH ASSY BARAKO', 0, 0, 0),
(1324, '001324', 'SAIYAN SPOKES 10GX150', 0, 0, 0),
(1325, '001325', 'SAIYAN SPOKES 10GX155', 0, 0, 0),
(1326, '001326', 'SAIYAN SPOKES 10GX161', 0, 0, 0),
(1327, '001327', 'SAIYAN SPOKES 10GX162', 0, 0, 0),
(1328, '001328', 'SAIYAN SPOKES 8GX150', 0, 0, 0),
(1329, '001329', 'SAIYAN SPOKES 8GX155', 0, 0, 0),
(1330, '001330', 'SAIYAN SPOKES 8GX161', 0, 0, 0),
(1331, '001331', 'SAIYAN SPOKES 8GX162', 0, 0, 0),
(1332, '001332', 'SAIYAN FUEL FILTER CLICK150', 0, 0, 0),
(1333, '001333', 'SAIYAN FUEL FILTER AEROX', 0, 0, 0),
(1334, '001334', 'SAIYAN FUEL FILTER RAIDER150 FI', 0, 0, 0),
(1335, '001335', 'SAIYAN KICK PINION GEAR MIO', 0, 0, 0),
(1336, '001336', 'SAIYAN CHAIN GUIDE MIO', 0, 0, 0),
(1337, '001337', 'SAIYAN CHAIN GUIDE RAIDER150', 0, 0, 0),
(1338, '001338', 'SAIYAN CHAIN GUIDE WAVE125', 0, 0, 0),
(1339, '001339', 'SAIYAN CHAIN GUIDE BARAKO', 0, 0, 0),
(1340, '001340', 'SAIYAN CHAIN GUIDE M3', 0, 0, 0),
(1341, '001341', 'SAIYAN CDI BARAKO', 0, 0, 0),
(1342, '001342', 'SAIYAN CDI WAVE125 OLD', 0, 0, 0),
(1343, '001343', 'SAIYAN CDI MIO SOULTY', 0, 0, 0),
(1344, '001344', 'SAIYAN CHAIN 428H-110L', 0, 0, 0),
(1345, '001345', 'SAIYAN CHAIN 428H-120L', 0, 0, 0),
(1346, '001346', 'SAIYAN CHAIN 428H-130L', 0, 0, 0),
(1347, '001347', 'SAIYAN CDI TMX ALPHA', 0, 0, 0),
(1348, '001348', 'SAIYAN CDI TMX155', 0, 0, 0),
(1349, '001349', 'SAIYAN CDI C100/DREAM', 0, 0, 0),
(1350, '001350', 'SAIYAN CDI WAVE125 ALPHA', 0, 0, 0),
(1351, '001351', 'SAIYAN BEARING 6000', 0, 0, 0),
(1352, '001352', 'SAIYAN BEARING 6001', 0, 0, 0),
(1353, '001353', 'SAIYAN BEARING 6002', 0, 0, 0),
(1354, '001354', 'SAIYAN BEARING 6200', 0, 0, 0),
(1355, '001355', 'SAIYAN BEARING 6201', 0, 0, 0),
(1356, '001356', 'SAIYAN BEARING 6202', 0, 0, 0),
(1357, '001357', 'SAIYAN BEARING 6203', 0, 0, 0),
(1358, '001358', 'SAIYAN BEARING 6204', 0, 0, 0),
(1359, '001359', 'SAIYAN BEARING 6205', 0, 0, 0),
(1360, '001360', 'SAIYAN BEARING 6300', 0, 0, 0),
(1361, '001361', 'SAIYAN BEARING 6301', 0, 0, 0),
(1362, '001362', 'SAIYAN BEARING 6302', 0, 0, 0),
(1363, '001363', 'SAIYAN BEARING 6303', 0, 0, 0),
(1364, '001364', 'SAIYAN BEARING 6304', 0, 0, 0),
(1365, '001365', 'SAIYAN BEARING 6003', 0, 0, 0),
(1366, '001366', 'SAIYAN BEARING 6004', 0, 0, 0),
(1367, '001367', 'SAIYAN BEARING 6005', 0, 0, 0),
(1368, '001368', 'SAIYAN KNUCKLE BEARING C100/WAVE', 0, 0, 0),
(1369, '001369', 'SAIYAN KNUCKLE BEARING CT100/HD3/BARAKO', 0, 0, 0),
(1370, '001370', 'SAIYAN KNUCKLE BEARING MIO', 0, 0, 0),
(1371, '001371', 'SAIYAN KNUCKLE BEARING TMX', 0, 0, 0),
(1372, '001372', 'SAIYAN KNUCKLE BEARING XRM', 0, 0, 0),
(1373, '001373', 'SAIYAN KNUCKLE BEARING FURY125', 0, 0, 0),
(1374, '001374', 'SAIYAN KNUCKLE BEARING RAIDER150', 0, 0, 0),
(1375, '001375', 'SAIYAN KNUCKLE BEARING CB110', 0, 0, 0),
(1376, '001376', 'SAIYAN KNUCKLE BEARING ROUSER135', 0, 0, 0),
(1377, '001377', 'SAIYAN KNUCKLE BEARING STX', 0, 0, 0),
(1378, '001378', 'SAIYAN KNUCKLE BEARING RAIDER J', 0, 0, 0),
(1379, '001379', 'SAIYAN KNUCKLE BEARING SHOGUN', 0, 0, 0),
(1380, '001380', 'SAIYAN KNUCKLE BEARING WAVE100', 0, 0, 0),
(1381, '001381', 'SAIYAN KNUCKLE BEARING WAVE125', 0, 0, 0),
(1382, '001382', 'SAIYAN STATOR COIL TMX155', 0, 0, 0),
(1383, '001383', 'SAIYAN KICK STARTER MIO', 0, 0, 0),
(1384, '001384', 'SAIYAN KICK STARTER RAIDER150', 0, 0, 0),
(1385, '001385', 'SAIYAN KICK STARTER TC125', 0, 0, 0),
(1386, '001386', 'SAIYAN BRAKE PEDAL TMX', 0, 0, 0),
(1387, '001387', 'SAIYAN REGULATOR CG125', 0, 0, 0),
(1388, '001388', 'SAIYAN REGULATOR GY6 5WIRES', 0, 0, 0),
(1389, '001389', 'SAIYAN REGULATOR M3', 0, 0, 0),
(1390, '001390', 'SAIYAN REGULATOR TMX SUPREMO', 0, 0, 0),
(1391, '001391', 'SAIYAN REGULATOR TMX ALPHA', 0, 0, 0),
(1392, '001392', 'SAIYAN REGULATOR WAVE125', 0, 0, 0),
(1393, '001393', 'SAIYAN CHAINSET TMX 14 36 110L', 0, 0, 0),
(1394, '001394', 'SAIYAN CHAINSET TMX 14 38 120L', 0, 0, 0),
(1395, '001395', 'SAIYAN CHAINSET TMX 14 40 120L', 0, 0, 0),
(1396, '001396', 'SAIYAN CHAINSET TMX 14 42 120L', 0, 0, 0),
(1397, '001397', 'SAIYAN CHAINSET XRM 14 34 110L', 0, 0, 0),
(1398, '001398', 'SAIYAN CHAINSET XRM 14 36 110L', 0, 0, 0),
(1399, '001399', 'SAIYAN CHAINSET SMASH 14 34 110L', 0, 0, 0),
(1400, '001400', 'SAIYAN CHAINSET SMASH 14 36 110L', 0, 0, 0),
(1401, '001401', 'SAIYAN CHAINSET SMASH 14 38 110L', 0, 0, 0),
(1402, '001402', 'SAIYAN CHAINSET BARAKO 14 42 120L', 0, 0, 0),
(1403, '001403', 'SAIYAN CHAINSET BARAKO 14 45 120L', 0, 0, 0),
(1404, '001404', 'SAIYAN CHAINSET C100 14 34 110L', 0, 0, 0),
(1405, '001405', 'SAIYAN CHAINSET C100 14 36 110L', 0, 0, 0),
(1406, '001406', 'SAIYAN SPOKES 8G 185', 0, 0, 0),
(1407, '001407', 'SAIYAN FRONT SHOCK ASSY XRM 110', 0, 0, 0),
(1408, '001408', 'SAIYAN FRONT SHOCK ASSY XRM 125', 0, 0, 0),
(1409, '001409', 'SAIYAN FRONT SHOCK ASSY TMX 125', 0, 0, 0),
(1410, '001410', 'SAIYAN FRONT SHOCK ASSY TC125', 0, 0, 0),
(1411, '001411', 'SAIYAN FRONT SHOCK ASSY WAVE 100', 0, 0, 0),
(1412, '001412', 'SAIYAN FRONT SHOCK ASSY WAVE 125', 0, 0, 0),
(1413, '001413', 'SAIYAN FRONT SHOCK ASSY M3', 0, 0, 0),
(1414, '001414', 'SAIYAN FRONT SHOCK ASSY SMASH', 0, 0, 0),
(1415, '001415', 'SAIYAN FRONT SHOCK ASSY BARAKO', 0, 0, 0),
(1416, '001416', 'SAIYAN FRONT SHOCK ASSY CT 100', 0, 0, 0),
(1417, '001417', 'SAIYAN COLORED CHAIN 428 130', 0, 0, 0),
(1418, '001418', 'SAIYAN RIM LIME GREEN', 0, 0, 0),
(1419, '001419', 'SAIYAN RIM GREEN', 0, 0, 0),
(1420, '001420', 'SAIYAN RIM BROWN', 0, 0, 0),
(1421, '001421', 'SAIYAN RIM GOLD', 0, 0, 0),
(1422, '001422', 'SAIYAN RIM ORANGE', 0, 0, 0),
(1423, '001423', 'SAIYAN STARTER MOTOR MIO', 0, 0, 0),
(1424, '001424', 'SAIYAN STARTER MOTOR LIFAN 150', 0, 0, 0),
(1425, '001425', 'SAIYAN CONNECTING ROD MIO', 0, 0, 0),
(1426, '001426', 'SAIYAN CONNECTING ROD LIFAN 110', 0, 0, 0),
(1427, '001427', 'SAIYAN CONNECTING ROD LIFAN125', 0, 0, 0),
(1428, '001428', 'SAIYAN CONNECTING ROD TMX 155', 0, 0, 0),
(1429, '001429', 'SAIYAN CONNECTING ROD CG 125', 0, 0, 0),
(1430, '001430', 'SAIYAN CONNECTING ROD CG 150', 0, 0, 0),
(1431, '001431', 'SAIYAN CONNECTING ROD WAVE 110 XRM 110', 0, 0, 0),
(1432, '001432', 'SAIYAN CONNECTING ROD XRM', 0, 0, 0),
(1433, '001433', 'SAIYAN CONNECTING ROD CT100', 0, 0, 0),
(1434, '001434', 'SAIYAN STATOR COIL CG 125', 0, 0, 0),
(1435, '001435', 'SAIYAN STATOR COIL WAVE 125', 0, 0, 0),
(1436, '001436', 'SAIYAN STATOR COIL MIO SPORTY', 0, 0, 0),
(1437, '001437', 'SAIYAN STATOR COIL M3', 0, 0, 0),
(1438, '001438', 'SAIYAN AIR FILTER MUSHROOM', 0, 0, 0),
(1439, '001439', 'SAIYAN STARTER RELAY XRM MIO', 0, 0, 0),
(1440, '001440', 'SAIYAN FRONT SHOCK ASSY MIO', 0, 0, 0),
(1441, '001441', 'SAIYAN FRONT SHOCK ASSY CLICK', 0, 0, 0),
(1442, '001442', 'SAIYAN BLOCK MIO 59MM', 0, 0, 0),
(1443, '001443', 'SAIYAN BLOCK BEAT 50MM', 0, 0, 0),
(1444, '001444', 'SAIYAN FRONT SHOCK ASSY RAIDER 150', 0, 0, 0),
(1445, '001445', 'SAIYAN THROTTLE CABLE WAVE125', 0, 0, 0),
(1446, '001446', 'SAIYAN THROTTLE CABLE CT150', 0, 0, 0),
(1447, '001447', 'SAIYAN THROTTLE CABLE DREAM', 0, 0, 0),
(1448, '001448', 'SAIYAN THROTTLE CABLE GD110', 0, 0, 0),
(1449, '001449', 'SAIYAN THROTTLE CABLE RAIDER150', 0, 0, 0),
(1450, '001450', 'SAIYAN THROTTLE CABLE RAIDER J', 0, 0, 0),
(1451, '001451', 'SAIYAN THROTTLE CABLE ROUSER135', 0, 0, 0),
(1452, '001452', 'SAIYAN THROTTLE CABLE RUSI TC125', 0, 0, 0),
(1453, '001453', 'SAIYAN THROTTLE CABLE RUSI150', 0, 0, 0),
(1454, '001454', 'SAIYAN THROTTLE CABLE SMASH DRUM BRAKE', 0, 0, 0),
(1455, '001455', 'SAIYAN THROTTLE CABLE SUPREMO', 0, 0, 0),
(1456, '001456', 'SAIYAN THROTTLE CABLE TMX ALPHA', 0, 0, 0),
(1457, '001457', 'SAIYAN THROTTLE CABLE TMX155', 0, 0, 0),
(1458, '001458', 'SAIYAN THROTTLE CABLE WAVE100', 0, 0, 0),
(1459, '001459', 'SAIYAN THROTTLE CABLE XRM TRINITY', 0, 0, 0),
(1460, '001460', 'SAIYAN THROTTLE CABLE XRM110', 0, 0, 0),
(1461, '001461', 'SAIYAN THROTTLE CABLE MIO', 0, 0, 0),
(1462, '001462', 'SAIYAN THROTTLE CABLE MIO RACING', 0, 0, 0),
(1463, '001463', 'SAIYAN THROTTLE CABLE SKYDRIVE', 0, 0, 0),
(1464, '001464', 'SAIYAN THROTTLE CABLE BEAT FI', 0, 0, 0),
(1465, '001465', 'SAIYAN THROTTLE CABLE CLICK', 0, 0, 0),
(1466, '001466', 'SAIYAN THROTTLE CABLE M3', 0, 0, 0),
(1467, '001467', 'SAIYAN THROTTLE CABLE GY6125', 0, 0, 0),
(1468, '001468', 'SAIYAN THROTTLE CABLE BEAT FI V2', 0, 0, 0),
(1469, '001469', 'SAIYAN THROTTLE CABLE CB125 CL', 0, 0, 0),
(1470, '001470', 'SAIYAN THROTTLE CABLE HD3', 0, 0, 0),
(1471, '001471', 'SAIYAN CLUTCH CABLE SNIPER155/150', 0, 0, 0),
(1472, '001472', 'SAIYAN CLUTCH CABLE TC125', 0, 0, 0),
(1473, '001473', 'SAIYAN CLUTCH CABLE STX', 0, 0, 0),
(1474, '001474', 'SAIYAN CLUTCH CABLE XRM125', 0, 0, 0),
(1475, '001475', 'SAIYAN CLUTCH CABLE ROUSER200', 0, 0, 0),
(1476, '001476', 'SAIYAN CLUTCH CABLE CB125', 0, 0, 0),
(1477, '001477', 'SAIYAN CLUTCH CABLE CT100', 0, 0, 0),
(1478, '001478', 'SAIYAN CLUTCH CABLE RAIDER150', 0, 0, 0),
(1479, '001479', 'SAIYAN CLUTCH CABLE CG125', 0, 0, 0),
(1480, '001480', 'SAIYAN CLUTCH CABLE SHOGUN', 0, 0, 0),
(1481, '001481', 'SAIYAN CLUTCH CABLE HD3', 0, 0, 0),
(1482, '001482', 'SAIYAN CLUTCH CABLE XRM TRINITY', 0, 0, 0),
(1483, '001483', 'SAIYAN CLUTCH CABLE GD110', 0, 0, 0),
(1484, '001484', 'SAIYAN CLUTCH CABLE ROUSER135', 0, 0, 0),
(1485, '001485', 'SAIYAN CLUTCH CABLE FZ16', 0, 0, 0),
(1486, '001486', 'SAIYAN CLUTCH CABLE TMX SUPREMO', 0, 0, 0),
(1487, '001487', 'SAIYAN CLUTCH CABLE RAIDER J', 0, 0, 0),
(1488, '001488', 'SAIYAN CLUTCH CABLE BARAKO', 0, 0, 0),
(1489, '001489', 'SAIYAN AIR FILTER AEROX155', 0, 0, 0),
(1490, '001490', 'SAIYAN AIR FILTER M3', 0, 0, 0),
(1491, '001491', 'SAIYAN AIR FILTER MIO SPORTY', 0, 0, 0),
(1492, '001492', 'SAIYAN AIR FILTER NMAX V1', 0, 0, 0),
(1493, '001493', 'SAIYAN AIR FILTER NMAX 2020 V2', 0, 0, 0),
(1494, '001494', 'SAIYAN AIR FILTER GEAR/GRAVIS', 0, 0, 0),
(1495, '001495', 'SAIYAN BLOCK MIO 50MM', 0, 0, 0),
(1496, '001496', 'SAIYAN BLOCK CLICK150', 0, 0, 0),
(1497, '001497', 'SAIYAN BLOCK CLICK125 STD', 0, 0, 0),
(1498, '001498', 'SAIYAN BLOCK NMAX STD', 0, 0, 0),
(1499, '001499', 'SAIYAN BLOCK RAIDER150 STD', 0, 0, 0),
(1500, '001500', 'SAIYAN BLOCK M3 59MM', 0, 0, 0),
(1501, '001501', 'SAIYAN BLOCK BARAKO 65MM', 0, 0, 0),
(1502, '001502', 'SAIYAN IGNITION SWITCH SET MIO OLD (LOCK SET)', 0, 0, 0),
(1503, '001503', 'SAIYAN REAR SHOCK Z SERIES WITHOUT TANK NMAX 305MM', 0, 0, 0),
(1504, '001504', 'SAIYAN REAR SHOCK Z SERIES MIO WITH TANK 300MM', 0, 0, 0),
(1505, '001505', 'SAIYAN REAR SHOCK Z SERIES CLICK WITH TANK 330MM', 0, 0, 0),
(1506, '001506', 'SAIYAN REAR SHOCK Z SERIES WITH TANK NMAX 305MM', 0, 0, 0),
(1507, '001507', 'SAIYAN REAR SHOCK Z SERIES MIO WITHOUT TANK 300MM', 0, 0, 0),
(1508, '001508', 'SAIYAN REAR SHOCK Z SERIES CLICK WITHOUT TANK 330MM', 0, 0, 0),
(1509, '001513', 'OUTSIDE PROFIT', 0, 0, 0),
(1510, '001513', 'FI CLEANING PROFIT', 0, 0, 0),
(1511, '001513', 'USED OIL PROFIT', 0, 0, 0),
(1512, '001513', 'DIAGNOSTIC PROFIT', 0, 0, 0),
(1513, '001513', 'TEST PRODUCT', 20, 30, 40);

--
-- Triggers `products`
--
DELIMITER $$
CREATE TRIGGER `set_productID` BEFORE INSERT ON `products` FOR EACH ROW BEGIN
    SET @new_id = (SELECT AUTO_INCREMENT FROM information_schema.TABLES
                   WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'products');
    SET NEW.product_id = CONCAT('00', @new_id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `returned_product`
--

CREATE TABLE `returned_product` (
  `id` int(11) NOT NULL,
  `barcode` int(11) NOT NULL,
  `returned_units` int(11) NOT NULL,
  `price` float NOT NULL,
  `date_bought` date NOT NULL,
  `date_returned` date NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service`) VALUES
(1, 'CVT Cleaning'),
(2, 'CVT Refresh'),
(3, 'Diagnostic'),
(4, 'Change Oil'),
(5, 'Fi Cleaning'),
(6, 'Reset ECU'),
(7, 'Tune Up'),
(8, 'Shock Tuning'),
(9, 'Carb Cleaning'),
(10, 'Engine Upgrade'),
(11, 'Major Overhaul'),
(12, 'Top Overhaul');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_record`
--

CREATE TABLE `supplier_record` (
  `code` varchar(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(255) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `code` int(11) NOT NULL,
  `userID` varchar(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `usertype` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`code`, `userID`, `firstname`, `email`, `password`, `contact`, `usertype`) VALUES
(1, 'A1', 'Renelina Mañabo', 'renju.manabo@yahoo.com', 'Admin@2024', '', 'Super Admin'),
(2, 'A2', 'Herb Andrei Doblada', 'dobladaherb09@gmail.com', 'GodIsGreat23', '09455517380', 'Super Admin');

--
-- Triggers `users`
--
DELIMITER $$
CREATE TRIGGER `setadmin_userID` BEFORE INSERT ON `users` FOR EACH ROW BEGIN
    DECLARE new_id INT;
    SELECT AUTO_INCREMENT INTO new_id
    FROM information_schema.TABLES
    WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'users';
    
    SET NEW.userID = CONCAT('A', new_id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users_branch`
--

CREATE TABLE `users_branch` (
  `code` int(11) NOT NULL,
  `userID` varchar(255) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `usertype` varchar(50) NOT NULL,
  `branch_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users_branch`
--

INSERT INTO `users_branch` (`code`, `userID`, `firstname`, `email`, `password`, `contact`, `usertype`, `branch_code`) VALUES
(2, 'B1', 'John Llyod Camero', 'johnlloydcamero123@gmail.com', '$Atis23', '09602377565', 'Branch Manager', '2'),
(3, 'B3', 'Mark Doblada', 'marknino175@gmail.com', 'Mark@2024', '09271174533	', 'Branch Manager', '1'),
(4, 'B4', 'Aulie Van August G. Raymundo', 'nget1703@gmail.com	', 'Aulie@2024', '09530344672', 'Branch Manager', '3'),
(5, 'B5', 'Terence Gabriel I. Madera', 'tgim.gabs@gmail.com', '$2y$10$MpQQAXz9KEkCUvUclxW1QebEskM5y4G5UByCUEaDg75', '09515819708', 'Branch Manager', '4'),
(6, 'B6', 'Fernando Jr. Pontillas	', 'pontillasfern@gmail.com', '$2y$10$XYcwAB8arvd0ZudP2k.dyeXDtD1Jql5OV81PYPpWCcj', '09487414785', 'Branch Manager', '5'),
(7, 'B7', 'Ken Ross T. Ladica', 'koopalkagar@gmail.com', '$2y$10$vHC8nnvHxfOIkLxi5e6eJeKMZ/A.EHukLVfPcttcy.G', '09547506485', 'Branch Manager', '6'),
(9, 'B8', 'Jefferson Villanueva Lopez', 'jhoeffgeisha08@gmail.com', '$2y$10$MmC8WIvVsGOrvTEZsNxuI.7DcXJ5O915w0vcwDG6rsD', '09957107070', 'Branch Manager', '8'),
(10, 'B10', 'Ricky Mariñas', 'marinasricky218@gmail.com', '$2y$10$MI6zwYO2llyhPiyeKnFStOk/kWs4OGXP5AVNZMsVIRH', '09536888389', 'Branch Manager', '9'),
(11, 'B11', 'Dan Carlo Alvarado', 'dobladaherb09@gmail.com', '$2y$10$wk6a/6SGt6Ce8hlyqJNv4O6GgQyFI3B.peVmwrsiuIp', '09455517380', 'Branch Manager', '10'),
(12, 'B12', 'Jude V. Fresnido', 'judefresnido7@gmail.com', '$2y$10$6qY0Avzs9d0tR4athwNWi.dDksoMPiVkdFCnEIG8/FS', '09389844433', 'Branch Manager', '11');

--
-- Triggers `users_branch`
--
DELIMITER $$
CREATE TRIGGER `set_userID` BEFORE INSERT ON `users_branch` FOR EACH ROW BEGIN
    SET @new_id = (SELECT AUTO_INCREMENT FROM information_schema.TABLES
                   WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'users_branch');
    SET NEW.userID = CONCAT('B', @new_id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users_dealer`
--

CREATE TABLE `users_dealer` (
  `code` int(11) NOT NULL,
  `userID` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `usertype` varchar(255) NOT NULL,
  `dealer_code` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users_dealer`
--

INSERT INTO `users_dealer` (`code`, `userID`, `firstname`, `email`, `password`, `contact`, `usertype`, `dealer_code`) VALUES
(1, 'D1', 'Juan Tamad', 'juantamad@gmail.com', 'juantamad@2024', '09090852555', 'Dealer', '1');

--
-- Triggers `users_dealer`
--
DELIMITER $$
CREATE TRIGGER `setdealer_userID` BEFORE INSERT ON `users_dealer` FOR EACH ROW BEGIN
    SET @new_id = (SELECT AUTO_INCREMENT FROM information_schema.TABLES
                   WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'users_dealer');
    SET NEW.userID = CONCAT('D', @new_id);
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch_record`
--
ALTER TABLE `branch_record`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `dealer_record`
--
ALTER TABLE `dealer_record`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `expenses_admin`
--
ALTER TABLE `expenses_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_expenses_code` (`code`);

--
-- Indexes for table `expenses_branch`
--
ALTER TABLE `expenses_branch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_expenses_branch_code` (`code`);

--
-- Indexes for table `inflow_admin`
--
ALTER TABLE `inflow_admin`
  ADD KEY `fk_barcode` (`barcode`),
  ADD KEY `fk_codeINFLOW` (`code`);

--
-- Indexes for table `inflow_branch`
--
ALTER TABLE `inflow_branch`
  ADD KEY `fk_barcode_branch` (`barcode`),
  ADD KEY `fk_inflow_branch` (`code`),
  ADD KEY `fk_admin_code_inflow` (`admin_code`),
  ADD KEY `fk_dealer_code_inflow` (`dealer_code`);

--
-- Indexes for table `logistics_admin`
--
ALTER TABLE `logistics_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_logistics_code_branch` (`code`);

--
-- Indexes for table `logistics_branch`
--
ALTER TABLE `logistics_branch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_logistics_branch_code` (`code`);

--
-- Indexes for table `mechanic_record`
--
ALTER TABLE `mechanic_record`
  ADD PRIMARY KEY (`mechanic_id`),
  ADD KEY `fk_users_branch_code` (`user_branch_code`);

--
-- Indexes for table `mechanic_services`
--
ALTER TABLE `mechanic_services`
  ADD PRIMARY KEY (`mechanic_service_id`),
  ADD KEY `fk_mechanic_id` (`mechanic_id`),
  ADD KEY `fk_services_id` (`services_id`),
  ADD KEY `fk_mechanic_code` (`code`);

--
-- Indexes for table `outflow_selling`
--
ALTER TABLE `outflow_selling`
  ADD KEY `fk_barcode_outflow` (`barcode`),
  ADD KEY `fk_codeOUTFLOW` (`code`);

--
-- Indexes for table `outflow_selling_branch`
--
ALTER TABLE `outflow_selling_branch`
  ADD KEY `fk_barcode_outflow_branch` (`barcode`),
  ADD KEY `fk_codeOUTFLOW_branch` (`code`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`barcode`),
  ADD UNIQUE KEY `unique_productnane` (`productname`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `supplier_record`
--
ALTER TABLE `supplier_record`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`code`),
  ADD UNIQUE KEY `userID` (`userID`);

--
-- Indexes for table `users_branch`
--
ALTER TABLE `users_branch`
  ADD PRIMARY KEY (`code`),
  ADD UNIQUE KEY `userID` (`userID`),
  ADD KEY `fk_branch` (`branch_code`);

--
-- Indexes for table `users_dealer`
--
ALTER TABLE `users_dealer`
  ADD PRIMARY KEY (`code`),
  ADD UNIQUE KEY `userID` (`userID`),
  ADD KEY `fk_users_dealer_code` (`dealer_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `expenses_admin`
--
ALTER TABLE `expenses_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenses_branch`
--
ALTER TABLE `expenses_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logistics_admin`
--
ALTER TABLE `logistics_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logistics_branch`
--
ALTER TABLE `logistics_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mechanic_record`
--
ALTER TABLE `mechanic_record`
  MODIFY `mechanic_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mechanic_services`
--
ALTER TABLE `mechanic_services`
  MODIFY `mechanic_service_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `barcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1514;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users_branch`
--
ALTER TABLE `users_branch`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users_dealer`
--
ALTER TABLE `users_dealer`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `expenses_admin`
--
ALTER TABLE `expenses_admin`
  ADD CONSTRAINT `fk_expenses_code` FOREIGN KEY (`code`) REFERENCES `users` (`code`);

--
-- Constraints for table `expenses_branch`
--
ALTER TABLE `expenses_branch`
  ADD CONSTRAINT `fk_expenses_branch_code` FOREIGN KEY (`code`) REFERENCES `branch_record` (`code`);

--
-- Constraints for table `inflow_admin`
--
ALTER TABLE `inflow_admin`
  ADD CONSTRAINT `fk_barcode` FOREIGN KEY (`barcode`) REFERENCES `products` (`barcode`),
  ADD CONSTRAINT `fk_codeINFLOW` FOREIGN KEY (`code`) REFERENCES `users` (`code`);

--
-- Constraints for table `inflow_branch`
--
ALTER TABLE `inflow_branch`
  ADD CONSTRAINT `fk_admin_code_inflow` FOREIGN KEY (`admin_code`) REFERENCES `users` (`code`),
  ADD CONSTRAINT `fk_barcode_branch` FOREIGN KEY (`barcode`) REFERENCES `products` (`barcode`),
  ADD CONSTRAINT `fk_dealer_code_inflow` FOREIGN KEY (`dealer_code`) REFERENCES `users_dealer` (`code`),
  ADD CONSTRAINT `fk_inflow_branch` FOREIGN KEY (`code`) REFERENCES `branch_record` (`code`);

--
-- Constraints for table `logistics_admin`
--
ALTER TABLE `logistics_admin`
  ADD CONSTRAINT `fk_logistics_code_branch` FOREIGN KEY (`code`) REFERENCES `users` (`code`);

--
-- Constraints for table `logistics_branch`
--
ALTER TABLE `logistics_branch`
  ADD CONSTRAINT `fk_logistics_branch_code` FOREIGN KEY (`code`) REFERENCES `branch_record` (`code`);

--
-- Constraints for table `mechanic_record`
--
ALTER TABLE `mechanic_record`
  ADD CONSTRAINT `fk_users_branch_code` FOREIGN KEY (`user_branch_code`) REFERENCES `users_branch` (`code`);

--
-- Constraints for table `mechanic_services`
--
ALTER TABLE `mechanic_services`
  ADD CONSTRAINT `fk_mechanic_code` FOREIGN KEY (`code`) REFERENCES `users_branch` (`code`),
  ADD CONSTRAINT `fk_mechanic_id` FOREIGN KEY (`mechanic_id`) REFERENCES `mechanic_record` (`mechanic_id`),
  ADD CONSTRAINT `fk_services_id` FOREIGN KEY (`services_id`) REFERENCES `services` (`service_id`);

--
-- Constraints for table `outflow_selling`
--
ALTER TABLE `outflow_selling`
  ADD CONSTRAINT `fk_barcode_outflow` FOREIGN KEY (`barcode`) REFERENCES `products` (`barcode`),
  ADD CONSTRAINT `fk_codeOUTFLOW` FOREIGN KEY (`code`) REFERENCES `users` (`code`);

--
-- Constraints for table `outflow_selling_branch`
--
ALTER TABLE `outflow_selling_branch`
  ADD CONSTRAINT `fk_barcode_outflow_branch` FOREIGN KEY (`barcode`) REFERENCES `products` (`barcode`),
  ADD CONSTRAINT `fk_codeOUTFLOW_branch` FOREIGN KEY (`code`) REFERENCES `users_branch` (`code`);

--
-- Constraints for table `users_branch`
--
ALTER TABLE `users_branch`
  ADD CONSTRAINT `fk_branch` FOREIGN KEY (`branch_code`) REFERENCES `branch_record` (`code`);

--
-- Constraints for table `users_dealer`
--
ALTER TABLE `users_dealer`
  ADD CONSTRAINT `fk_users_dealer_code` FOREIGN KEY (`dealer_code`) REFERENCES `dealer_record` (`code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
